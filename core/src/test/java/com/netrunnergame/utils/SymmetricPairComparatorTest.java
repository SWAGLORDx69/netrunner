package com.netrunnergame.utils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * SymmetricPairComparator Tester.
 *
 * @author Kirill Smirnov
 * @version 1.0
 */
public class SymmetricPairComparatorTest {
    private final SymmetricPairComparator comp = new SymmetricPairComparator();

    @Test
    public void testCompareEq() throws Exception {
        assertEquals(comp.compare(new PairInt(1, 2), new PairInt(1, 2)), 0);
    }

    @Test
    public void testCompareEqRev() throws Exception {
        assertEquals(comp.compare(new PairInt(1, 2), new PairInt(2, 1)), 0);
    }

    @Test
    public void testCompareMore1() throws Exception {
        assertEquals(comp.compare(new PairInt(3, 2), new PairInt(2, 1)), 1);
    }

    @Test
    public void testCompareMore2() throws Exception {
        assertEquals(comp.compare(new PairInt(3, 0), new PairInt(2, 1)), 1);
    }

    @Test
    public void testCompareMore3() throws Exception {
        assertEquals(comp.compare(new PairInt(2, 2), new PairInt(2, 1)), 1);
    }

    @Test
    public void testCompareMore4() throws Exception {
        assertEquals(comp.compare(new PairInt(0, 6), new PairInt(2, 1)), 1);
    }

    @Test
    public void testCompareMore5() throws Exception {
        assertEquals(comp.compare(new PairInt(3, 1), new PairInt(0, 3)), 1);
    }

    @Test
    public void testCompareLess1() throws Exception {
        assertEquals(comp.compare(new PairInt(2, 1), new PairInt(3, 2)), -1);
    }

    @Test
    public void testCompareLess2() throws Exception {
        assertEquals(comp.compare(new PairInt(2, 1), new PairInt(3, 0)), -1);
    }

    @Test
    public void testCompareLess3() throws Exception {
        assertEquals(comp.compare(new PairInt(2, 1), new PairInt(2, 2)), -1);
    }

    @Test
    public void testCompareLess4() throws Exception {
        assertEquals(comp.compare(new PairInt(2, 1), new PairInt(0, 6)), -1);
    }

    @Test
    public void testCompareLess5() throws Exception {
        assertEquals(comp.compare(new PairInt(3, 0), new PairInt(1, 3)), -1);
    }
} 
