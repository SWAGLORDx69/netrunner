package com.netrunnergame.core.programs;

import com.badlogic.gdx.math.Vector3;
import com.netrunnergame.core.Level;
import com.netrunnergame.core.LevelController;
import com.netrunnergame.core.files.DataFile;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Test class for defensive program
 *
 * @author Peto
 * @version 1.0
 */
public class DefensiveProgramTest extends GameProgramWithArgTest {

    @Before
    public void before() {
        program = new DefensiveProgram();
    }

    @Test
    public void simpleDefensiveProgramTest() {
        Level level = Level.builder()
                .newPlayer("player")
                .newNode(0, "type", "player", new Vector3(0, 0, 0), 1, 1, 1, 1000)
                .newNode(1, "type", "player", new Vector3(0, 0, 3), 1, 1, 1, 1000)
                .build();
        LevelController player = level.player();

        DataFile dataFile = new DataFile(1000);
        dataFile.setHost(level.player().getNodeView(0));
        dataFile.discover(level.getRepresentative());

        GameProgramWithArg prg = new DefensiveProgram();
        prg.setHost(player.getNodeView(0));
        prg.setArg(player.getNodeView(0));
        assertTrue(player.runProgram(prg.clone(), 0));
        player.stopProgram(0, 0);
    }

    @Test
    public void advancedDefensiveProgramTest() {
        Level level = Level.builder()
                .newPlayer("player")
                .newNode(0, "type", "player", new Vector3(0, 0, 0), 1, 1, 1, 1000)
                .newNode(1, "type", "player", new Vector3(0, 0, 3), 1, 1, 1, 1000)
                .build();
        LevelController player = level.player();

        DataFile dataFile = new DataFile(1000);
        dataFile.setHost(level.player().getNodeView(0));
        dataFile.discover(level.getRepresentative());

        GameProgramWithArg prg1 = new DefensiveProgram();
        prg1.setHost(player.getNodeView(0));
        prg1.setArg(player.getNodeView(1));
        assertTrue(player.runProgram(prg1, 0));
        GameProgramWithArg prg2 = new DefensiveProgram();
        prg2.setHost(player.getNodeView(1));
        prg2.setArg(player.getNodeView(0));
        assertTrue(player.runProgram(prg2, 1));
        player.stopProgram(0, 0);
        player.stopProgram(1, 0);
    }

    @Test
    public void duoDefensiveProgramTest() {
        Level level = Level.builder()
                .newPlayer("player")
                .newNode(0, "type", "player", new Vector3(0, 0, 0), 2, 2, 2, 2000)
                .newNode(1, "type", "player", new Vector3(0, 0, 3), 1, 1, 1, 1000)
                .build();
        LevelController player = level.player();

        DataFile dataFile = new DataFile(1000);
        dataFile.setHost(level.player().getNodeView(0));
        dataFile.discover(level.getRepresentative());

        GameProgramWithArg prg1 = new DefensiveProgram();
        prg1.setHost(player.getNodeView(0));
        prg1.setArg(player.getNodeView(1));
        assertTrue(player.runProgram(prg1, 0));
        GameProgramWithArg prg2 = new DefensiveProgram();
        prg2.setHost(player.getNodeView(0));
        prg2.setArg(player.getNodeView(1));
        assertTrue(player.runProgram(prg2, 0));
        player.stopProgram(0, 0);
        player.stopProgram(0, 1);
    }
}
