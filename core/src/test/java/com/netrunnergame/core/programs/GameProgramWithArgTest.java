package com.netrunnergame.core.programs;

import com.netrunnergame.core.files.DataFile;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by kirill on 21.04.17.
 */
public abstract class GameProgramWithArgTest extends GameProgramTest {
    @Test
    @Override
    public void cloneTest() throws Exception {

        DataFile dataFile = new DataFile(1000);
        dataFile.setHost(level.player().getNodeView(0));
        dataFile.discover(level.getRepresentative());

        GameProgramWithArg program = (GameProgramWithArg) this.program;
        program.setHost(nodes.get(0));
        program.setArg(nodes.get(1));
        GameProgramWithArg cloneOfProgram = (GameProgramWithArg) program.clone();
        assertEquals(cloneOfProgram.getHost(), nodes.get(0));
        assertEquals(cloneOfProgram.getArg(), nodes.get(1));
        assertEquals(cloneOfProgram.getDataCost(), program.getDataCost(), 1e-6);
        assertEquals(cloneOfProgram.getRAM(), program.getRAM(), 1e-6);
        assertEquals(cloneOfProgram.getName(), program.getName());
        assertEquals(cloneOfProgram.getInfo(), program.getInfo());
        cloneOfProgram.setHost(null);
        assertEquals(program.getHost(), nodes.get(0));
        assertEquals(cloneOfProgram.getHost(), null);
    }

}