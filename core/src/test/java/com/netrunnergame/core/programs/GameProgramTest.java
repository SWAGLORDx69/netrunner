package com.netrunnergame.core.programs;

import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.IntMap;
import com.netrunnergame.core.Level;
import com.netrunnergame.core.Node;
import com.netrunnergame.core.Program;
import com.netrunnergame.core.files.DataFile;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * Created by kirill on 21.04.17.
 */
public abstract class GameProgramTest {
    protected GameProgram program;
    protected Level level;
    protected Map<Integer,Node> nodes;

    @Before
    public void setUp() throws Exception {
        level = Level.builder()
                .setTimeOfGame(1000)
                .newPlayer("player")
                .newNode(0, "type", "player", new Vector3(0, 0, 0), 1, 1, 1, 100)
                .newNode(1, "type", "player", new Vector3(0, 0, 1), 1, 1, 1, 100)
                .newNode(2, "type", "player", new Vector3(0, 0, 2), 1, 1, 1, 100)
                .newNode(3, "type", "player", new Vector3(0, 0, 3), 1, 1, 1, 100)
                .linkFromTo(0, 1)
                .linkFromTo(0, 2)
                .linkFromTo(0, 3)
                .build();
        nodes = level.getRepresentative().getNodes();

    }

    @Test
    public void cloneTest() throws Exception {

        DataFile dataFile = new DataFile(1000);
        dataFile.setHost(level.player().getNodeView(0));
        dataFile.discover(level.getRepresentative());

        program.setHost(nodes.get(0));
        Program cloneOfProgram = (Program) program.clone();
        assertEquals(cloneOfProgram.getHost(), nodes.get(0));
        assertEquals(cloneOfProgram.getDataCost(), program.getDataCost(), 1e-6);
        assertEquals(cloneOfProgram.getRAM(), program.getRAM(), 1e-6);
        assertEquals(cloneOfProgram.getName(), program.getName());
        assertEquals(cloneOfProgram.getInfo(), program.getInfo());
        cloneOfProgram.setHost(null);
        assertEquals(program.getHost(), nodes.get(0));
        assertEquals(cloneOfProgram.getHost(), null);
    }

}