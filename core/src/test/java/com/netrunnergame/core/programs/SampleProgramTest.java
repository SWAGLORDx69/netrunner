package com.netrunnergame.core.programs;

import com.badlogic.gdx.math.Vector3;
import com.netrunnergame.core.Level;
import com.netrunnergame.core.LevelController;
import com.netrunnergame.core.files.DataFile;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Test class for sample program
 *
 * @author Kots Michael
 * @version 1.0
 */
public class SampleProgramTest extends GameProgramTest {

    @Before
    public void before() {
        program = new DistrCompProgram();
    }

    @Test
    public void simpleSampleProgramTest() {
        Level level = Level.builder()
                .newPlayer("player")
                .newNode(0, "type", "player", new Vector3(0, 0, 0), 1, 1, 1, 1000)
                .build();
        LevelController player = level.player();

        DataFile dataFile = new DataFile(1000);
        dataFile.setHost(level.player().getNodeView(0));
        dataFile.discover(level.getRepresentative());

        SampleProgram prg = new SampleProgram();
        prg.setHost(player.getNodeView(0));
        assertTrue(player.runProgram(prg, 0));
        player.stopProgram(0, 0);
    }

    @Test
    public void advancedSampleProgramTest() {
        Level level = Level.builder()
                .newPlayer("player")
                .newNode(0, "type", "player", new Vector3(0, 0, 0), 1, 1, 1, 1000)
                .newNode(3, "type", "player", new Vector3(0, 0, 3), 1, 1, 1, 1000)
                .build();
        LevelController player = level.player();

        DataFile dataFile = new DataFile(1000);
        dataFile.setHost(level.player().getNodeView(0));
        dataFile.discover(level.getRepresentative());

        SampleProgram prg1 = new SampleProgram();
        prg1.setHost(player.getNodeView(0));
        assertTrue(player.runProgram(prg1, 0));
        SampleProgram prg2 = new SampleProgram();
        prg2.setHost(player.getNodeView(3));
        assertTrue(player.runProgram(prg2, 3));
        player.stopProgram(0, 0);
        player.stopProgram(3, 0);
    }

    @Test
    public void duoSampleProgramTest() {
        Level level = Level.builder()
                .newPlayer("player")
                .newNode(0, "type", "player", new Vector3(0, 0, 0), 2, 2, 2, 2000)
                .build();
        LevelController player = level.player();

        DataFile dataFile = new DataFile(1000);
        dataFile.setHost(level.player().getNodeView(0));
        dataFile.discover(level.getRepresentative());

        SampleProgram prg1 = new SampleProgram();
        prg1.setHost(player.getNodeView(0));
        assertTrue(player.runProgram(prg1, 0));
        SampleProgram prg2 = new SampleProgram();
        prg2.setHost(player.getNodeView(0));
        assertTrue(player.runProgram(prg2, 0));
        player.stopProgram(0, 0);
        player.stopProgram(0, 1);
    }
}
