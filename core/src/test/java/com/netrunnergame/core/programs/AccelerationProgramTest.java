package com.netrunnergame.core.programs;

import com.netrunnergame.core.Program;
import org.junit.Before;

/**
 * Test class for defensive program
 *
 * @author Kirill Smirnov
 * @version 1.0
 */
public class AccelerationProgramTest extends GameProgramWithArgTest {
    @Before
    public void before() {
        program = new AccelerationProgram();
    }
}