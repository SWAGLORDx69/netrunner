package com.netrunnergame.core.programs;

import com.badlogic.gdx.math.Vector3;
import com.netrunnergame.core.Level;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for mask neutral program
 *
 * @author Peto
 * @version 1.0
 */
public class MaskProgramTest extends GameProgramTest {

    @Before
    public void before() {
        program = new DistrCompProgram();
    }

    @Test
    public void simpleMaskProgramTest() {
        Level level = Level.builder()
                .newPlayer("player")
                .newNode(0, "type", null, new Vector3(0, 0, 0), 1, 1, 1, 1)
                .runProgram(new MaskProgram(), 0)
                .build();
    }
}
