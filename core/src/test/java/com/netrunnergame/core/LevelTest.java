package com.netrunnergame.core;

import com.badlogic.gdx.math.Vector3;
import com.netrunnergame.NetRunnerGame;
import com.netrunnergame.utils.NonExistentNodeIdException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Test class for main game level
 *
 * @author Kots Michael
 * @version 1.1
 * @see Level
 */
public class LevelTest {

    Level lvl;

    /*@Before
    public void after() {
        lvl = new LevelLoader(new NetRunnerGame(),"data/levels/test.json").load().buildLevel();
    }*/

    @Test
    public void simpleLevelTest() {
        Level level = Level.builder()
                .newPlayer("player")
                .newNode(0, "type", "player", new Vector3(0, 0, 0), 1, 1, 1, 100)
                .build();
        LevelController player = level.player();
        NodeView node = player.getNodeView(0);
        assertTrue(node.getId() == 0);
        assertTrue(node.getType().equals("type"));
        assertTrue(node.getPosition().equals(new Vector3(0, 0, 0)));
    }

    @Test
    public void iteratorLevelTest() {
        Level level = Level.builder()
                .newPlayer("player")
                .newNode(0, "type", "player", new Vector3(0, 0, 0), 1, 1, 1, 100)
                .newNode(1, "type", "player", new Vector3(0, 0, 1), 1, 1, 1, 100)
                .newNode(2, "type", "player", new Vector3(0, 0, 2), 1, 1, 1, 100)
                .newNode(3, "type", "player", new Vector3(0, 0, 3), 1, 1, 1, 100)
                .build();
        LevelController player = level.player();
        Iterator<NodeView> nodes = player.iterator();
        int k = 0;
        while (nodes.hasNext()) {
            assertTrue(nodes.next().getId() == k);
            k++;
        }
    }

    @Test
    public void advancedLevelTest() {
        Level level = Level.builder()
                .newPlayer("player")
                .newNode(0, "type", "player", new Vector3(0, 0, 0), 1, 1, 1, 100)
                .newNode(1, "type", "player", new Vector3(0, 0, 1), 1, 1, 1, 100)
                .newNode(2, "type", "player", new Vector3(0, 0, 2), 1, 1, 1, 100)
                .newNode(3, "type", "player", new Vector3(0, 0, 3), 1, 1, 1, 100)
                .linkFromTo(0, 1)
                .linkFromTo(0, 2)
                .linkFromTo(0, 3)
                .build();
        LevelController player = level.player();
        NodeView node = player.getNodeView(0);
        assertTrue(node.numOfLinks() == 3);
        Iterator<NodeView> it = node.getLinkedNodes();
        int k = 1;
        while (it.hasNext()) {
            assertTrue(it.next().getId() == k);
            k++;
        }
        Iterator<String> types = player.getNodeTypes();
        assertTrue(types.next().equals("type"));
    }

    /*@Test
    public void testNodeCalculateWayTo() throws NonExistentNodeIdException {
        NodeView node = lvl.player().getNodeView(1);
        assertEquals(node.calculateWayTo(1), 0);
        assertEquals(node.calculateWayTo(2), 1);
        assertEquals(node.calculateWayTo(9), 2);
        assertEquals(node.calculateWayTo(13), 2);
        try {
            node.calculateWayTo(-1);
            Assert.fail(); //wrong way
        } catch (NonExistentNodeIdException e) {
            //right way
        }
    }*/
}
