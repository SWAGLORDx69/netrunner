package com.netrunnergame.core.files;

import com.badlogic.gdx.math.Vector3;
import com.netrunnergame.core.*;
import com.netrunnergame.core.files.DataFile;
import com.netrunnergame.core.files.GameFile;
import com.netrunnergame.core.programs.SampleProgram;
import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.assertTrue;

/**
 * Test class for data file entity
 *
 * @author Pozigun Mikhail
 * @version 0.1
 */
public class DataFileTest {

    @Test
    public void simpleDataFileTest() {
        Level level = Level.builder()
                .newPlayer("player")
                .newNode(0, "type", "player", new Vector3(0, 0, 0), 1, 1, 1, 10)
                .addFile(new DataFile(9), 0)
                .build();
        LevelController player = level.player();

        DataFile dataFile = new DataFile(1000);
        dataFile.setHost(player.getNodeView(0));
        dataFile.discover(level.getRepresentative());

        Program prg = new SampleProgram();
        prg.setHost(player.getNodeView(0));
        assertTrue(player.runProgram(prg, 0));

        NodeView node = player.getNodeView(0);
        ((Node) node).discover();
        assertTrue(player.getDataAmount() == 9);
    }

    @Test
    public void advancedDataFileTest() {
        Level level = Level.builder()
                .newPlayer("player")
                .newNode(0, "type", "player", new Vector3(0, 0, 0), 1, 1, 1, 10)
                .addFile(new DataFile(2), 0)
                .addFile(new DataFile(3), 0)
                .addFile(new DataFile(4), 0)
                .build();
        LevelController player = level.player();

        DataFile dataFile = new DataFile(1000);
        dataFile.setHost(player.getNodeView(0));
        dataFile.discover(level.getRepresentative());

        Program prg = new SampleProgram();
        prg.setHost(player.getNodeView(0));
        assertTrue(player.runProgram(prg, 0));

        NodeView node = player.getNodeView(0);
        ((Node) node).discover();
        assertTrue(player.getDataAmount() == 9);
    }
}
