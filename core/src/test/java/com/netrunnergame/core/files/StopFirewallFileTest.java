package com.netrunnergame.core.files;

import com.badlogic.gdx.math.Vector3;
import com.netrunnergame.core.*;
import com.netrunnergame.core.files.GameFile;
import com.netrunnergame.core.files.StopFirewallFile;
import com.netrunnergame.core.programs.FirewallProgram;
import com.netrunnergame.core.programs.SampleProgram;
import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.assertTrue;

/**
 * Test class for stop firewall file entity
 *
 * @author Pozigun Mikhail
 * @version 0.1
 */
public class StopFirewallFileTest {

    @Test
    public void simpleStopFirewallFileTest() {
        Level level = Level.builder()
                .newPlayer("player")
                .newNode(0, "type", "player", new Vector3(0, 0, 0), 1, 1, 1, 1)
                .newNode(1, "type", "player", new Vector3(0, 0, 0), 1, 1, 1, 1)
                .addFile(new StopFirewallFile(0), 1)
                .build();
        LevelController player = level.player();

        DataFile dataFile = new DataFile(1000);
        dataFile.setHost(player.getNodeView(0));
        dataFile.discover(level.getRepresentative());

        Program prg1 = new FirewallProgram();
        prg1.setHost(player.getNodeView(0));
        assertTrue(player.runProgram(prg1, 0));

        assertTrue(player.getNodeView(0).getNumberOfBusyCores() == 1);

        NodeView node = player.getNodeView(1);
        ((Node) node).discover();
//        Iterator<File> it = node.getFiles();
//        GameFile file = (GameFile) it.next();
//        assertTrue(file.getOwner().getId() == 1);
//        assertTrue(!it.hasNext());

        assertTrue(player.getNodeView(0).getNumberOfBusyCores() == 0);

    }

    @Test
    public void anotherSimpleStopFirewallFileTest() {
        Level level = Level.builder()
                .newPlayer("player")
                .newNode(0, "type", "player", new Vector3(0, 0, 0), 10, 1, 100, 100)
                .newNode(1, "type", "player", new Vector3(0, 0, 0), 1, 1, 1, 1)
                .addFile(new StopFirewallFile(0), 1)
                .build();
        LevelController player = level.player();

        DataFile dataFile = new DataFile(1000);
        dataFile.setHost(player.getNodeView(0));
        dataFile.discover(level.getRepresentative());

        Program prg1 = new FirewallProgram();
        Program prg2 = new SampleProgram();
        prg1.setHost(player.getNodeView(0));
        prg2.setHost(player.getNodeView(0));
        assertTrue(player.runProgram(prg1, 0));
        assertTrue(player.runProgram(prg2, 0));

        assertTrue(player.getNodeView(0).getNumberOfBusyCores() == 2);

        NodeView node = player.getNodeView(1);
        ((Node) node).discover();
//        Iterator<File> it = node.getFiles();
//        GameFile file = (GameFile) it.next();
//        assertTrue(file.getOwner().getId() == 1);
//        assertTrue(!it.hasNext());

        assertTrue(player.getNodeView(0).getNumberOfBusyCores() == 1);

        assertTrue(player.getNodeView(0).getProgram(0) == null);

        assertTrue(player.getNodeView(0).getProgram(1) != null &&
                player.getNodeView(0).getProgram(1) instanceof SampleProgram);

    }
}
