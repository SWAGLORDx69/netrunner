package com.netrunnergame.core.files;

import com.badlogic.gdx.math.Vector3;
import com.netrunnergame.core.*;
import com.netrunnergame.core.files.GameFile;
import com.netrunnergame.core.files.PasswordFile;
import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.assertTrue;

/**
 * Test class for password file entity
 *
 * @author Pozigun Mikhail
 * @version 0.1
 */
public class PasswordFileTest {

    @Test
    public void simplePasswordFileTest() {
        Level level = Level.builder()
                .newPlayer("player")
                .newNode(0, "type", "player", new Vector3(0, 0, 0), 1, 1, 1, 1)
                .newNode(1, "type", null, new Vector3(0, 0, 0), 1, 1, 1, 1)
                .linkFromTo(0, 1)
                .addFile(new PasswordFile(1), 0)
                .build();
        LevelController player = level.player();
        NodeView node = player.getNodeView(0);

        assertTrue(player.getNodeView(1).getOwner().getName().equals(""));

        ((Node) node).discover();

        assertTrue(player.getNodeView(1).getOwner() == player);
    }

    @Test
    public void advancedPasswordFileTest1() {
        Level level = Level.builder()
                .newPlayer("player")
                .newNode(0, "type", "player", new Vector3(0, 0, 0), 1, 1, 1, 1)
                .newNode(1, "type", null, new Vector3(0, 0, 0), 1, 1, 1, 1)
                .newNode(2, "type", null, new Vector3(0, 0, 0), 1, 1, 1, 1)
                .linkFromTo(0, 1)
                .linkFromTo(0, 2)
                .addFile(new PasswordFile(1), 0)
                .addFile(new PasswordFile(2), 0)
                .build();
        LevelController player = level.player();
        NodeView node = player.getNodeView(0);

        assertTrue(player.getNodeView(1).getOwner().getName().equals(""));
        assertTrue(player.getNodeView(2).getOwner().getName().equals(""));

       ((Node) node).discover();

        assertTrue(player.getNodeView(1).getOwner() == player);
        assertTrue(player.getNodeView(2).getOwner() == player);
    }

    @Test
    public void advancedPasswordFileTest2() {
        Level level = Level.builder()
                .newPlayer("player")
                .newNode(0, "type", "player", new Vector3(0, 0, 0), 1, 1, 1, 1)
                .newNode(1, "type", null, new Vector3(0, 0, 0), 1, 1, 1, 1)
                .newNode(2, "type", null, new Vector3(0, 0, 0), 1, 1, 1, 1)
                .newNode(3, "type", null, new Vector3(0, 0, 0), 1, 1, 1, 1)
                .newNode(4, "type", null, new Vector3(0, 0, 0), 1, 1, 1, 1)
                .linkFromTo(0, 1)
                .linkFromTo(0, 1)
                .linkFromTo(0, 2)
                .linkFromTo(0, 3)
                .linkFromTo(0, 4)
                .addFile(new PasswordFile(1), 0)
                .addFile(new PasswordFile(2), 1)
                .addFile(new PasswordFile(3), 2)
                .addFile(new PasswordFile(4), 3)
                .build();
        LevelController player = level.player();

        NodeView node = player.getNodeView(0);

        assertTrue(player.getNodeView(1).getOwner().getName().equals(""));
        assertTrue(player.getNodeView(2).getOwner().getName().equals(""));
        assertTrue(player.getNodeView(3).getOwner().getName().equals(""));
        assertTrue(player.getNodeView(4).getOwner().getName().equals(""));

        ((Node) node).discover();

        assertTrue(player.getNodeView(1).getOwner() == player);
        assertTrue(player.getNodeView(2).getOwner() == player);
        assertTrue(player.getNodeView(3).getOwner() == player);
        assertTrue(player.getNodeView(4).getOwner() == player);
    }
}
