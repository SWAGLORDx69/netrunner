package com.netrunnergame.core.files;

import com.badlogic.gdx.math.Vector3;
import com.netrunnergame.core.*;
import com.netrunnergame.core.files.GameFile;
import com.netrunnergame.core.files.NetMapFile;
import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.assertTrue;

/**
 * Test class for net map file entity
 *
 * @author Pozigun Mikhail
 * @version 0.1
 */
public class NetMapFileTest {

    @Test
    public void NetMapFileTest1() {
        Level level = Level.builder()
                .newPlayer("player")
                .newNode(0, "type", "player", new Vector3(0, 0, 0), 1, 1, 1, 1)
                .newNode(1, "type", null, new Vector3(0, 0, 0), 1, 1, 1, 1)
                .newNode(2, "type", null, new Vector3(0, 0, 0), 1, 1, 1, 1)
                .newNode(3, "type", null, new Vector3(0, 0, 0), 1, 1, 1, 1)
                .newNode(4, "type", null, new Vector3(0, 0, 0), 1, 1, 1, 1)
                .linkFromTo(0, 1)
                .linkFromTo(1, 2)
                .linkFromTo(1, 2)
                .linkFromTo(2, 3)
                .linkFromTo(2, 4)
                .addFile(new NetMapFile(), 0)
                .build();
        LevelController player = level.player();
        NodeView node = player.getNodeView(0);

        ((Node) node).discover();

//        Iterator<File> itFiles = node.getFiles();
//        GameFile file = (GameFile) itFiles.next();
//        assertTrue(file.getOwner().getId() == 0);
//        assertTrue(!itFiles.hasNext());

        Iterator<NodeView> itNodes = player.iterator();
        assertTrue(itNodes.next().getId() == 0);
        assertTrue(itNodes.next().getId() == 1);
        assertTrue(itNodes.next().getId() == 2);
        assertTrue(itNodes.next().getId() == 3);
        assertTrue(itNodes.next().getId() == 4);
        assertTrue(!itNodes.hasNext());
    }

    @Test
    public void NetMapFileTest2() {
        Level level = Level.builder()
                .newPlayer("player")
                .newNode(1, "type", "player", new Vector3(0, 0, 0.1f), 2, 1, 3, 1)
                .linkFromTo(1, 2)
                .linkFromTo(1, 3)
                .linkFromTo(1, 5)
                .linkFromTo(1, 6)
                .newNode(2, "type", null, new Vector3(-0.5f * 4.1f, 0.866f * 4.0f, 2.1f), 10, 3, 10, 1)
                .linkFromTo(2, 1)
                .linkFromTo(2, 7)
                .linkFromTo(2, 8)
                .linkFromTo(2, 9)
                .linkFromTo(2, 3)
                .newNode(3, "type", null, new Vector3(0.5f * 4.1f, 0.866f * 4.1f, 2.2f), 6, 1, 6, 1)
                .linkFromTo(3, 1)
                .linkFromTo(3, 2)
                .linkFromTo(3, 4)
                .linkFromTo(3, 9)
                .linkFromTo(3, 10)
                .newNode(4, "type", null, new Vector3(4.0f, -0.1f, 2.3f), 2, 1, 2, 1)
                .linkFromTo(4, 3)
                .linkFromTo(4, 5)
                .linkFromTo(4, 10)
                .linkFromTo(4, 11)
                .newNode(5, "type", null, new Vector3(0.6f * 4.1f, -0.866f * 4.0f, 2.4f), 8, 15, 18, 1)
                .linkFromTo(5, 4)
                .linkFromTo(5, 1)
                .linkFromTo(5, 6)
                .linkFromTo(5, 11)
                .linkFromTo(5, 12)
                .newNode(6, "type", null, new Vector3(-0.6f * 4.1f, -0.866f * 4.1f, 2.5f), 2, 1, 2, 1)
                .linkFromTo(6, 1)
                .linkFromTo(6, 7)
                .linkFromTo(6, 5)
                .linkFromTo(6, 12)
                .linkFromTo(6, 13)
                .newNode(7, "type", null, new Vector3(-4.0f, 0.1f, 2.6f), 2, 1, 2, 1)
                .linkFromTo(7, 2)
                .linkFromTo(7, 6)
                .linkFromTo(7, 8)
                .linkFromTo(7, 13)
                .newNode(8, "type", null, new Vector3(-1.5f * 4.0f, 0.866f * 4.2f, -0.1f), 4, 1, 4, 1)
                .linkFromTo(8, 2)
                .linkFromTo(8, 7)
                .newNode(9, "type", null, new Vector3(0.1f, 1.7f * 4.0f, -0.2f), 2, 1, 2, 1)
                .linkFromTo(9, 3)
                .linkFromTo(9, 2)
                .newNode(10, "type", null, new Vector3(1.5f * 4.0f, 0.866f * 4.3f, -0.2f), 2, 1, 2, 1)
                .linkFromTo(10, 3)
                .linkFromTo(10, 4)
                .newNode(11, "type", null, new Vector3(1.5f * 4.1f, -0.866f * 4.2f, -0.3f), 6, 1, 7, 1)
                .linkFromTo(11, 4)
                .linkFromTo(11, 5)
                .newNode(12, "type", null, new Vector3(0.2f, -1.7f * 4.0f, -0.4f), 2, 1, 2, 1)
                .linkFromTo(12, 5)
                .linkFromTo(12, 6)
                .newNode(13, "type", null, new Vector3(-1.5f * 4.1f, -0.866f * 4.3f, -0.5f), 2, 1, 2, 1)
                .linkFromTo(13, 6)
                .linkFromTo(13, 7)
                .addFile(new NetMapFile(), 1)
                .build();
        LevelController player = level.player();
        NodeView node = player.getNodeView(1);

        ((Node) node).discover();

//        Iterator<File> itFiles = node.getFiles();
//        GameFile file = (GameFile) itFiles.next();
//        assertTrue(file.getOwner().getId() == 1);
//        assertTrue(!itFiles.hasNext());

        Iterator<NodeView> itNodes = player.iterator();
        assertTrue(itNodes.next().getId() == 1);
        assertTrue(itNodes.next().getId() == 2);
        assertTrue(itNodes.next().getId() == 3);
        assertTrue(itNodes.next().getId() == 4);
        assertTrue(itNodes.next().getId() == 5);
        assertTrue(itNodes.next().getId() == 6);
        assertTrue(itNodes.next().getId() == 7);
        assertTrue(itNodes.next().getId() == 8);
        assertTrue(itNodes.next().getId() == 9);
        assertTrue(itNodes.next().getId() == 10);
        assertTrue(itNodes.next().getId() == 11);
        assertTrue(itNodes.next().getId() == 12);
        assertTrue(itNodes.next().getId() == 13);
        assertTrue(!itNodes.hasNext());
    }

    @Test
    public void NetMapFileTest3() {
        Level level = Level.builder()
                .newPlayer("player")
                .newNode(2, "type", "player", new Vector3(0, 0, 0), 1, 1, 1, 1)
                .newNode(5, "type", null, new Vector3(0, 0, 0), 1, 1, 1, 1)
                .newNode(9, "type", null, new Vector3(0, 0, 0), 1, 1, 1, 1)
                .newNode(1, "type", null, new Vector3(0, 0, 0), 1, 1, 1, 1)
                .newNode(3, "type", null, new Vector3(0, 0, 0), 1, 1, 1, 1)
                .linkFromTo(2, 1)
                .linkFromTo(2, 9)
                .linkFromTo(9, 5)
                .linkFromTo(5, 3)
                .addFile(new NetMapFile(), 2)
                .build();
        LevelController player = level.player();
        NodeView node = player.getNodeView(2);

        ((Node) node).discover();

//        Iterator<File> itFiles = node.getFiles();
//        GameFile file = (GameFile) itFiles.next();
//        assertTrue(file.getOwner().getId() == 2);
//        assertTrue(!itFiles.hasNext());

        Iterator<NodeView> itNodes = player.iterator();
        assertTrue(itNodes.next().getId() == 1);
        assertTrue(itNodes.next().getId() == 2);
        assertTrue(itNodes.next().getId() == 3);
        assertTrue(itNodes.next().getId() == 5);
        assertTrue(itNodes.next().getId() == 9);
        assertTrue(!itNodes.hasNext());
    }
}
