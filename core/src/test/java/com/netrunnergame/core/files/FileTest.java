package com.netrunnergame.core.files;

import com.badlogic.gdx.math.Vector3;
import com.netrunnergame.core.File;
import com.netrunnergame.core.Level;
import com.netrunnergame.core.LevelController;
import com.netrunnergame.core.NodeView;
import com.netrunnergame.core.files.GameFile;
import com.netrunnergame.core.files.SampleFile;
import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.assertTrue;

/**
 * Test class for file entity
 *
 * @author Kots Michael
 * @version 1.0
 */
public class FileTest {

    @Test
    public void simpleFileTest() {
        Level level = Level.builder()
                .newPlayer("player")
                .newNode(0, "type", "player", new Vector3(0, 0, 0), 1, 1, 1, 1)
                .addFile(new SampleFile(), 0)
                .build();
        LevelController player = level.player();
        NodeView node = player.getNodeView(0);
    }

    @Test
    public void advancedFileTest() {
        Level level = Level.builder()
                .newPlayer("player")
                .newNode(0, "type", "player", new Vector3(0, 0, 0), 1, 1, 1, 1)
                .addFile(new SampleFile(), 0)
                .addFile(new SampleFile(), 0)
                .addFile(new SampleFile(), 0)
                .build();
        LevelController player = level.player();
        NodeView node = player.getNodeView(0);
    }


}
