package com.netrunnergame.utils;

import java.util.Comparator;

/**
 * I can't explain how it work
 *
 * @author Kirill Smirnov
 * @version 1.0
 */
public class SymmetricPairComparator implements Comparator<PairInt> {
    @Override
    public int compare(PairInt o1, PairInt o2) {
        int max1 = Math.max(o1.getA(), o1.getB());
        int min1 = Math.min(o1.getA(), o1.getB());
        int max2 = Math.max(o2.getA(), o2.getB());
        int min2 = Math.min(o2.getA(), o2.getB());

        if (max1 > max2) {
            return 1;
        } else if (max1 < max2) {
            return -1;
        } else if (min1 > min2) {
            return 1;
        } else if (min1 < min2) {
            return -1;
        } else {
            return 0;
        }
    }
}
