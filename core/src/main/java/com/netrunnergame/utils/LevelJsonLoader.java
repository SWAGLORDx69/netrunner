package com.netrunnergame.utils;

/**
 * Json Level asset loader
 *
 * @author Andrew Konstantinov
 * @version 1.0
 * @see LevelJson
 */

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.AsynchronousAssetLoader;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;

public class LevelJsonLoader extends AsynchronousAssetLoader<LevelJson, LevelJsonLoader.TextParameter> {

    LevelJson text;

    public LevelJsonLoader(FileHandleResolver resolver) {
        super(resolver);
    }

    @Override
    public void loadAsync(AssetManager manager, String fileName, FileHandle file, TextParameter parameter) {
        this.text = null;
        this.text = new LevelJson(file);
    }

    @Override
    public LevelJson loadSync(AssetManager manager, String fileName, FileHandle file, TextParameter parameter) {
        LevelJson text = this.text;
        this.text = null;
        return text;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public Array<AssetDescriptor> getDependencies(String fileName, FileHandle file, TextParameter parameter) {
        return null;
    }

    public static class TextParameter extends AssetLoaderParameters<LevelJson> {

    }

}
