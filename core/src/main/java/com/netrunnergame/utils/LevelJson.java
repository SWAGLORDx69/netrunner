package com.netrunnergame.utils;

/**
 * Json file asset with level representation
 *
 * @author Andrew Konstantinov
 * @version 1.0
 */

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;

public class LevelJson {

    private String string;

    public LevelJson() {
        this.string = new String("".getBytes());
    }

    public LevelJson(byte[] data) {
        this.string = new String(data);
    }

    public LevelJson(String string) {
        this.string = new String(string.getBytes());
    }

    public LevelJson(FileHandle file) {
        this.string = new String(file.readBytes());
    }

    public LevelJson(LevelJson text) {
        this.string = new String(text.getString().getBytes());
    }

    public String getString() {
        return this.string;
    }

    public void setString(String string) {
        this.string = string;
    }

    public void clear() {
        this.string = new String("".getBytes());
    }

    public JsonValue getRoot() {
        JsonReader jsonReader = new JsonReader();
        return jsonReader.parse(getString());
    }

}