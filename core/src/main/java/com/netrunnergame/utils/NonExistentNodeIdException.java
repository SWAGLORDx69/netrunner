package com.netrunnergame.utils;

/**
 * The type of exception that is thrown when the system did not find a node with the requested ID.
 *
 * @author Kirill Smirnov
 * @version 1.0
 */
public class NonExistentNodeIdException extends Exception {
    /**
     * Instantiates a new exception from the non-existent node ID
     *
     * @param id is the ID of the non-existent node
     */
    public NonExistentNodeIdException(int id) {
        super("the node with the id == " + id + "  does not exist");
    }
}
