package com.netrunnergame.utils;

/**
 * the tuple of two integers
 *
 * @author Kirill Smirnov
 * @version 1.0
 */
public class PairInt{
    private final int a;
    private final int b;

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    public PairInt(int a, int b) {
        this.a = a;
        this.b = b;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PairInt pairInt = (PairInt) o;
        return (a == pairInt.a && b == pairInt.b) ;
    }

    @Override
    public int hashCode() {
        return a*31 + b;
    }
}