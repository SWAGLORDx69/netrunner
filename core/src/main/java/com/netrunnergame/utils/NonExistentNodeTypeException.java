package com.netrunnergame.utils;

/**
 * The type of exception that throws when the system did not found a requested type.
 *
 * @author Kirill Smirnov
 * @version 1.0
 */
public class NonExistentNodeTypeException extends Exception {
    /**
     * Instantiates a new exception from the name of the wrong type.
     *
     * @param type is a string contained the wrong type
     */
    public NonExistentNodeTypeException(String type) {
        super("this type of node doesn't exist: \"" + type + "\"");
    }
}
