package com.netrunnergame.core;

import java.util.*;

/**
 * Interface for controlling the level. Contains methods that allow to obtain
 * level's state and send commands for it
 * <p>
 * TODO write more detailed description
 *
 * @author Kirill Smirnov, Kots Michael
 * @version 1.2
 * @see Level
 */
public abstract class LevelController extends Observable implements Iterable<NodeView> {
    /**
     * Gets name.
     *
     * @return the name
     */
    public abstract String getName();

    /**
     * Get cur level help string.
     *
     * @return the string
     */
    public abstract String getCurLevelHelp();

    /**
     * Gets node view.
     *
     * @param id the id
     * @return the node view
     */
    public abstract NodeView getNodeView(int id);

    /**
     * Gets node types.
     *
     * @return the node types
     */
    public abstract Iterator<String> getNodeTypes();

    /**
     * Check is it able to run program.
     *
     * @param program the program
     * @param nodeID  the node id
     * @return success of probable attempt to run the program
     */
    public abstract boolean isAbleToRunProgram(Program program, int nodeID);

    /**
     * Run program.
     *
     * @param program the program
     * @param nodeID  the node id
     * @return success of attempt to run the program
     */
    public abstract boolean runProgram(Program program, int nodeID);

    /**
     * Stop program.
     *
     * @param nodeID     id of the node  where program execute
     * @param coreNumber the number of the core which program use
     * @return success of attempt to stop the program
     */
    public abstract boolean stopProgram(int nodeID, int coreNumber);

    /**
     * Starts level updating process.
     */
    public abstract void start();

    /**
     * Stops level updating process.
     */
    public abstract void pause();

    /**
     * Gets program prototypes that are available for this player.
     *
     * @return the available programs
     */
    public abstract List<Program> getAvailablePrograms();

    /**
     * Sets program prototypes that are available for this player .
     *
     * @param programs list of prototypes
     * @see #getProgramPrototypes()
     */
    public abstract void setAvailablePrograms(Collection<Program> programs);

    /**
     * @return the name of winner or null if there is no winner
     */
    public abstract String getWinnerName();

    /**
     * Gets time remaining.
     *
     * @return the time remaining
     */
    public abstract double getTimeRemaining();

    /**
     * Gets data amount.
     *
     * @return the data amount
     */
    public abstract int getDataAmount();

    /**
     * Gets max data amount.
     *
     * @return the max data amount
     */
    public abstract int getMaxDataAmount();

    /**
     * Signalises about visibility node.
     * TODO decide whether to delete it or not
     *
     * @param id id of the required node
     * @return false if node invisible
     */
    public abstract boolean isVisibleNode(int id);

    /**
     * Signalises that node is hidden.
     * TODO decide whether to delete it or not
     *
     * @param id id of the required node
     * @return false if node marked as "Hidden"
     */
    public abstract boolean isMaskedNode(int id);

    /**
     * get all programs that are available for this level
     *
     * @return list of prototypes
     */
    public abstract List<Program> getProgramPrototypes();

    public abstract Map<Integer, Node> getNodes();
}
