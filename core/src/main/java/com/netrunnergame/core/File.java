package com.netrunnergame.core;

/**
 * Interface for game files
 *
 * @author Kots Michael
 * @author Kirill Smirnov
 * @version 1.1
 */
public interface File {
    /**
     * Gets name of the file.
     * Example: "Acceleration Program"
     *
     * @return the name
     */
    String getName();

    /**
     * Gets a string with the information about the file.
     * Example: "File with map of net, makes all computers visible"
     *
     * @return help information about program for user
     */
    String getInfo();

    /**
     * Sets a node which will execute this program.
     *
     * @param host the host
     */
    void setHost(NodeView host);

    /**
     * Action upon capturing node with this file
     *
     * @param level
     */
    void discover(Representative level);
}
