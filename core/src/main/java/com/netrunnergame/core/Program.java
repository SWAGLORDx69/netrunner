package com.netrunnergame.core;

import java.util.List;

/**
 * Defines interface of program entity
 *
 * @author Kots Michael
 * @author Kirill Smirnov
 * @version 1.1
 * @see com.netrunnergame.core.programs.GameProgram
 */
public interface Program extends Cloneable {
    /**
     * Gets name.
     *
     * @return the name
     */
    String getName();

    /**
     * Gets ram.
     *
     * @return amount of RAM required to run this program
     */
    double getRAM();

    /**
     * Gets data cost.
     *
     * @return data cost of the program
     */
    double getDataCost();

    /**
     * Gets info.
     *
     * @return help information about program for user
     */
    String getInfo();

    /**
     * Gets core.
     *
     * @return the core on which the program was ran or -1 if this program doesn't exist on this core
     */
    int getCore();

    /**
     * actions that happen on program start
     *
     * @param level
     */
    void run(Representative level);

    /**
     * actions that happen every tick while program is in play
     *
     * @return false if program finished
     */
    boolean update();

    /**
     * action that happen when program is finished
     */
    void dismiss();

    /**
     * this is very bad TODO fix this somehow
     *
     * @return the host of node
     */
    Node getHost();

    /**
     * this is very bad TODO fix this somehow
     *
     * @param host the host of node
     */
    void setHost(NodeView host);

    /**
     * TODO make it less awful
     *
     * @return
     */
    List<Integer> getIdOfPossibleArg();

    /**
     * clones this program
     *
     * @return full copy of this program
     */
    Program clone();
}
