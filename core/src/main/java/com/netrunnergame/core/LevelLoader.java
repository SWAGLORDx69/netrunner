package com.netrunnergame.core;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.JsonValue;
import com.netrunnergame.NetRunnerGame;
import com.netrunnergame.core.programs.AttackProgram;
import com.netrunnergame.utils.LevelJson;

import java.lang.reflect.Constructor;
import java.util.Arrays;


/**
 * TODO comments this shit
 */
public class LevelLoader {
    private String levelHelp = null;
    private static final String FILECLASSPATH = "com.netrunnergame.core.files";
    private static final String PROGRAMCLASSPATH = "com.netrunnergame.core.programs";
    private final String path;
    private NetRunnerGame game;
    private JsonValue root = null;

    public LevelLoader(NetRunnerGame gm, String sourcePath) {
        game = gm;
        path = sourcePath;
    }

    /**
     * Load json level content from assets, parse and unload
     *
     * @return LevelLoader
     */
    public LevelLoader load() {

        if (!game.assets.isLoaded(path)) {
            game.assets.load(path, LevelJson.class);
            game.assets.finishLoadingAsset(path);
        }
        LevelJson json = game.assets.get(path);

        game.assets.unload(path); // assume a rare same level loading

        root = json.getRoot();

        return this;
    }

    public Level buildLevel() {

        Level.LevelBuilder builder = Level.builder();
        // builder.newPlayer("player", 8945000, 100);

        load();

        builder.setTimeOfGame(root.get("time").asDouble());

        JsonValue hint = root.get("hint");
        if (hint != null) {
            levelHelp = hint.asString();
        } else {
            Gdx.app.log("loader", "There is no hint in level");
        }
        builder.setLevelHelp(levelHelp);

        JsonValue player = root.get("player");
        builder.newPlayer(player.get("name").asString());

        {
            JsonValue programsJson = root.get("programs");
            if (null != programsJson && !programsJson.isNull()) {

                String[] programs = null;
                try {
                    programs = programsJson.asStringArray();
                } catch (Exception ignoredException) {
                    programs = null;
                }

                if (programs != null) {
                    for (String program : programs) {
                        try {
                            Constructor programConstructor = Class.forName(
                                    PROGRAMCLASSPATH + "." + program
                            ).getConstructor();
                            builder.addLevelProgram((Program) programConstructor.newInstance());
                            Gdx.app.log("loader", "Program class loaded: " + program);
                        } catch (Exception exception) {
                            Gdx.app.log("loader", "Can't load Program class: " + program);
                        }
                    }
                }
            }
        }

        JsonValue nodes = root.get("nodes");

        for (JsonValue node = nodes.child; node != null; node = node.next) {
            try {
                int id = node.get("id").asInt();
                JsonValue pos = node.get("position");
                float x = pos.get("x").asFloat();
                float y = pos.get("y").asFloat();
                float z = pos.get("z").asFloat();
                String type = node.get("type").asString();

                String owner = node.get("owner").asString();
                if (owner.equals("")) //host.isEmpty())
                    owner = null;
                int coreNumber = node.get("coreNumber").asInt();
                int clockSpeed = node.get("clockSpeed").asInt();
                int RAM = node.get("RAM").asInt();
                int HDD = node.get("HDD").asInt();

                int[] links = node.get("links").asIntArray();
                builder.newNode(id, type, owner, new Vector3(x, y, z), coreNumber, clockSpeed, RAM, HDD);
                for (int i = 0; i < links.length; i++) {
                    builder.linkFromTo(id, links[i]);
                }

                JsonValue filesJson = node.get("files");
                if (null != filesJson && !filesJson.isNull()) {

                    String[] files = null;
                    try {
                        files = filesJson.asStringArray();
                    } catch (Exception ignoredException) {
                        files = null;
                    }

                    if (files != null) {// backward compatibility for json file format to load files without args
                        for (String file : files) {
                            try {
                                Constructor fileConstructor = Class.forName(
                                        FILECLASSPATH + "." + file
                                ).getConstructor();
                                builder.addFile((File) fileConstructor.newInstance(), id);
                                Gdx.app.log("loader", "File class loaded: " + file);
                            } catch (Exception exception) {
                                Gdx.app.log("loader", "Can't load File class: " + file);
                            }
                        }
                    } else {
                        for (JsonValue fileObj = filesJson.child; fileObj != null; fileObj = fileObj.next) {
                            String file = fileObj.get("name").asString();
                            int[] args = fileObj.get("args").asIntArray();
                            Gdx.app.log("loader",
                                    "Loading complex file: " + file + "; with args: " + Arrays.toString(args)
                            );
                            try {
                                Class fileClass = Class.forName(
                                        FILECLASSPATH + "." + file
                                );
                                if (args.length != 0) {
                                    Constructor fileConstructor = fileClass.getConstructor(int.class);
                                    builder.addFile((File) fileConstructor.newInstance((Object[])boxInts(args)), id);
                                } else {
                                    Constructor fileConstructor = fileClass.getConstructor();
                                    builder.addFile((File) fileConstructor.newInstance(), id);
                                }
                                Gdx.app.log("loader", "File class loaded: " + file);
                            } catch (Exception exception) {
                                Gdx.app.log("loader",
                                        "Can't load File class: " + file + "; caused by " + exception.toString()
                                );
                            }
                        }
                    }

                }

                JsonValue programsJson = node.get("programs");
                if (null != programsJson && !programsJson.isNull()) {

                    String[] programs = null;
                    try {
                        programs = programsJson.asStringArray();
                    } catch (Exception ignoredException) {
                        programs = null;
                    }

                    if (programs != null) {
                        for (String program : programs) {
                            try {
                                Constructor programConstructor = Class.forName(
                                        PROGRAMCLASSPATH + "." + program
                                ).getConstructor();
                                builder.runProgram((Program) programConstructor.newInstance(), id);
                                Gdx.app.log("loader", "Program class loaded: " + program);
                            } catch (Exception exception) {
                                Gdx.app.log("loader", "Can't load Program class: " + program);
                            }
                        }
                    } else {
                        for (JsonValue programObj = programsJson.child; programObj != null; programObj = programObj.next) {
                            String program = programObj.get("name").asString();
                            double[] args = programObj.get("args").asDoubleArray();
                            Gdx.app.log("loader",
                                    "Loading complex program: " + program + "; with args: " + Arrays.toString(args)
                            );
                            try {
                                Class programClass = Class.forName(
                                        PROGRAMCLASSPATH + "." + program
                                );
                                if (args.length != 0) {
                                    Constructor programConstructor = programClass.getConstructor(double.class, double.class);
                                    builder.runProgram((Program) programConstructor.newInstance((Object[])boxDoubles(args)), id);
                                } else {
                                    Constructor programConstructor = programClass.getConstructor();
                                    builder.runProgram((Program) programConstructor.newInstance(), id);
                                }
                                Gdx.app.log("loader", "Program class loaded: " + program);
                            } catch (Exception exception) {
                                Gdx.app.log("loader",
                                        "Can't load Program class: " + program + "; caused by " + exception.toString()
                                );
                            }
                        }
                    }
                }
            } catch (NullPointerException exception) { // caused by JsonValue.get(<...>).as<...> method call
                Gdx.app.log("loader", "There is a kind of invalid node");
            }
        }

        return builder.build();
    }


    protected static Integer[] boxInts(int[] arr) {
        Integer[] boxed = new Integer[arr.length];
        for (int i = 0; i < arr.length; i++) {
            boxed[i] = Integer.valueOf(arr[i]);
        }
        return boxed;
    }

    protected static Double[] boxDoubles(double[] arr) {
        Double[] boxed = new Double[arr.length];
        for (int i = 0; i < arr.length; i++) {
            boxed[i] = Double.valueOf(arr[i]);
        }
        return boxed;
    }

    /*
    private class FakeBuilder {

        StringBuilder json = new StringBuilder();

        FakeBuilder newNode(int id, String type, String host, Vector3 pos, int coreNumber, int clockSpeed, int RAM, int HDD) {
            json.append("{");
            json.append("\"id\": \"");
            json.append(id);
            json.append("\",");

            json.append("\"position\": {");
            json.append("\"x\": \"");
            json.append(pos.x);
            json.append("\",");

            json.append("\"y\": \"");
            json.append(pos.y);
            json.append("\",");

            json.append("\"z\": \"");
            json.append(pos.z);
            json.append("\"},");

            json.append("\"type\": \"");
            json.append(type);
            json.append("\",");

            json.append("\"host\": \"");
            json.append(host == null ? "" : host);
            json.append("\",");

            json.append("\"coreNumber\": \"");
            json.append(coreNumber);
            json.append("\",");

            json.append("\"clockSpeed\": \"");
            json.append(clockSpeed);
            json.append("\",");

            json.append("\"RAM\": \"");
            json.append(RAM);
            json.append("\",");

            json.append("\"HDD\": \"");
            json.append(HDD);
            json.append("\",");

            json.append("\"links\": []},");

            return this;
        }

        FakeBuilder linkFromTo(int a, int b) {
            return this;
        }

        void print() {
            Gdx.app.log("core", json.toString());
        }

    } */

}
