package com.netrunnergame.core.files;

import com.netrunnergame.core.Node;
import com.netrunnergame.core.NodeView;
import com.netrunnergame.core.Representative;

/**
 * File that captures certain node.
 *
 * @author Pozigun Mikhail
 * @author Kirill Smirnov
 * @version 1.1
 * @see GameFile
 */
public class PasswordFile extends GameFile {

    private final int nodeId;
    private boolean isDiscovered = false;

    public PasswordFile(int nodeId) {
        super("Password File",
                "Password from this file gives you access to node " + nodeId);
        this.nodeId = nodeId;
    }

    @Override
    public void discover(Representative level) {
        if (!isDiscovered) {
            NodeView target = host.getOwner().getNodeView(nodeId);
            if (target != null &&
                    target.getOwner().equals(level.getDefaultPlayer())) {
                ((Node) target).setOwner(host.getOwner());
            }
            super.discover(level);
            isDiscovered = true;
        }
    }
}
