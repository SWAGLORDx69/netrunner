package com.netrunnergame.core.files;


import com.netrunnergame.core.Node;
import com.netrunnergame.core.Program;
import com.netrunnergame.core.Representative;
import com.netrunnergame.core.programs.FirewallProgram;

/**
 * File for stopping firewall program on certain node.
 *
 * @author Pozigun Mikhail
 * @author Kirill Smirnov
 * @version 1.1
 * @see GameFile
 */
public class StopFirewallFile extends GameFile {

    private boolean isDiscovered = false;
    private int nodeId;

    public StopFirewallFile(int nodeId) {
        super("Stop Firewall",
                "This file will help you to hack firewall on node " + nodeId + " and stop it");
        this.nodeId = nodeId;
    }

    public StopFirewallFile() {
        this(16);
    }

    @Override
    public void discover(Representative level) {
        if (!isDiscovered) {
            Node targetNode = level.getNodes().get(nodeId);
            if (targetNode != null) {
                for (Program program : targetNode.getCores()) {
                    if (program != null && program instanceof FirewallProgram) {
                        level.stopProgram(program);
                        break;
                    }
                }
            }
            host.logMessage("Stop firewall program file was discovered " + host.getId());
            isDiscovered = true;
        }
    }
}
