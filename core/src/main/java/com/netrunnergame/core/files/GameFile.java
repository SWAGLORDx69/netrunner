package com.netrunnergame.core.files;

import com.netrunnergame.core.File;
import com.netrunnergame.core.Node;
import com.netrunnergame.core.NodeView;
import com.netrunnergame.core.Representative;

/**
 * Defines common fields for game files
 *
 * @author Kots Michael
 * @author Kirill Smirnov
 * @version 2.0
 * @see File
 */
public abstract class GameFile implements File {

    /**
     * pointer on node that owns this file
     */
    protected Node host;

    private final String name;

    private final String info;

    public GameFile(String name, String info) {
        this.name = name;
        this.info = info;
    }

    @Override
    public final String getName() {
        return name;
    }

    @Override
    public final String getInfo() {
        return info;
    }


    public final NodeView getHost() {
        return host;
    }

    @Override
    public void discover(Representative level) {
        host.logMessage(name + " was discovered");
    }

    @Override
    public void setHost(NodeView host) {
        this.host = (Node) host;
    }

}
