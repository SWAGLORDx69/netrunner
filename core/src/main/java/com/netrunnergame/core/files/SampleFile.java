package com.netrunnergame.core.files;

/**
 * Sample file that does nothing
 *
 * @author Kots Michael
 * @author Kirill Smirnov
 * @version 1.1
 * @see GameFile
 */
public class SampleFile extends GameFile {

    public SampleFile() {
        super("SampleFile",
                "Sample file that does nothing");
    }
}
