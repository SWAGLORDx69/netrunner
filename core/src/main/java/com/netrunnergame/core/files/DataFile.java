package com.netrunnergame.core.files;

import com.netrunnergame.core.Player;
import com.netrunnergame.core.Representative;

/**
 * File that gives some data to player
 *
 * @author Pozigun Mikhail
 * @author Kirill Smirnov
 * @version 1.1
 * @see GameFile
 */
public class DataFile extends GameFile {

    private final int data;
    private boolean isDiscovered = false;

    public DataFile() {
        this(10);
    }

    public DataFile(int data) {
        super("Data File",
                "File with " + data + " units of data.");
        this.data = data;
    }

    @Override
    public void discover(Representative level) {
        if (!isDiscovered) {
            super.discover(level);
            ((Player) host.getOwner()).setDataAmount(host.getOwner().getDataAmount() + data);
            isDiscovered = true;
        }
    }
}
