package com.netrunnergame.core.files;

import com.netrunnergame.core.Node;
import com.netrunnergame.core.Player;
import com.netrunnergame.core.Representative;

/**
 * File that makes visible all computers
 *
 * @author Pozigun Mikhail
 * @author Kirill Smirnov
 * @version 1.1
 * @see GameFile
 */
public class NetMapFile extends GameFile {

    private boolean isDiscovered = false;

    public NetMapFile() {
        super("Net Map",
                "File with map of net, makes all computers visible");
    }

    @Override
    public void discover(Representative level) {
        if (!isDiscovered) {
            Player player = (Player) host.getOwner();
            for (Node node : level.getNodes().values()) {
                player.setNodeVisible(node.getId());
            }
            super.discover(level);
            isDiscovered = true;
        }
    }
}
