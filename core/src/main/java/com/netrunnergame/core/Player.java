package com.netrunnergame.core;

import com.netrunnergame.core.programs.*;

import java.util.*;

/**
 * Implementation of an entity controlling the level
 * <p>
 * TODO write more detailed description
 *
 * @author Kots Michael
 * @author Kirill Smirnov
 * @version 2.0
 * @see LevelController
 */
public class Player extends LevelController implements Observer {
    private final Set<Integer> visibleNodes = new HashSet<>();
    private final Representative level;
    private final List<Program> availablePrograms = new ArrayList<Program>(5) {{
        add(new DataMiningProgram());
        add(new SpyProgram());
        add(new AttackProgram());
        add(new AdvancedAttackProgram());
        add(new DistrCompProgram());
        add(new AccelerationProgram());
        add(new DefensiveProgram());
    }};
    private String name;
    private int dataAmount = 0;
    private int maxDataAmount;

    /**
     * TODO write a description
     *
     * @param level
     * @param name
     */
    public Player(final Representative level, String name) {
        this.name = name;
        this.level = level;
        level.addLevelObserver(this);
    }

    @Override
    public Map<Integer, Node> getNodes() {
        return level.getNodes();
    }

    /**
     * method for forced updating a player data
     */
    void updatePlayer() {
        maxDataAmount = 0;
        for (Node n : level.getNodes().values()) {
            if (n.getOwner() != null && n.getOwner().equals(this)) {
                visibleNodes.add(n.getId());
                maxDataAmount += n.getHDD();
                Iterator<NodeView> it = n.getLinkedNodes();
                while (it.hasNext()) {
                    visibleNodes.add(it.next().getId());
                }
            }
        }
    }

    public void setNodeVisible(int id) {
        this.visibleNodes.add(id);
        setChanged();
        notifyObservers(getNodeView(id));
    }

    public void setNodeInvisible(int id) {
        //visibleNodes.remove(id);
        //не поддерживается на даном этапе
    }

    @Override
    public String getWinnerName() {
        if (level.getWinner() != null) {
            return level.getWinner().getName();
        }
        return null;
    }

    @Override
    public boolean isVisibleNode(int id) {
        return this.visibleNodes.contains(id);
    }

    @Override
    public boolean isMaskedNode(int id) {
        return level.getNodes().get(id).getFlag("Hidden");
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getCurLevelHelp() {
        return level.getLevelHelp();
    }

    @Override
    public NodeView getNodeView(int id) {
        if (visibleNodes.contains(id))
            return level.getNodes().get(id);
        else
            return null;
    }

    /**
     * return object for iterating through the visible nodes of graph
     */
    @Override
    public Iterator<NodeView> iterator() {
        return new Iterator<NodeView>() {
            private final Iterator<Integer> itNode = visibleNodes.iterator();

            @Override
            public boolean hasNext() {
                return itNode.hasNext();
            }

            @Override
            public NodeView next() {
                return level.getNodes().get(itNode.next());
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("remove");
            }
        };
    }

    @Override
    public Iterator<String> getNodeTypes() {
        return level.getTypesOfNode().iterator();
    }

    @Override
    public boolean isAbleToRunProgram(Program program, int nodeID) {
        if (visibleNodes.contains(nodeID) && dataAmount >= program.getDataCost()) {
            return level.isAbleToRunProgram(program, nodeID);
        }
        return false;
    }

    @Override
    public boolean runProgram(Program program, int nodeID) {
        if (visibleNodes.contains(nodeID) && dataAmount >= program.getDataCost()) {
            boolean res = level.runProgram(program, nodeID);
            if (res) {
                dataAmount -= program.getDataCost();
            }
            return res;
        }
        return false;
    }

    @Override
    public boolean stopProgram(int nodeID, int coreNumber) {
        if (visibleNodes.contains(nodeID)) {
            level.stopProgram(nodeID, coreNumber);
            return true;
        } else
            return false;
    }

    @Override
    public void start() {
        level.start();
    }

    @Override
    public void pause() {
        level.pause();
    }

    public List<Program> getAvailablePrograms() {
        return Collections.unmodifiableList(availablePrograms);
    }

    @Override
    public void setAvailablePrograms(Collection<Program> programs) {
        availablePrograms.clear();
        availablePrograms.addAll(programs);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg == null) {
            setChanged();
            notifyObservers();
        }

        if (arg instanceof Log) {
            setChanged();
            notifyObservers(arg);
        }

        if (arg instanceof NodeView) {
            NodeView node = (NodeView) arg;
            if (equals(node.getOwner()) || visibleNodes.contains(node.getId())) {
                visibleNodes.add(node.getId());
                setChanged();
                notifyObservers(node);
                if (getName().equals(node.getOwnerName())) {
                    Iterator<NodeView> it = node.getLinkedNodes();
                    while (it.hasNext()) {
                        int lid = it.next().getId();
                        if (!visibleNodes.contains(lid)) {
                            visibleNodes.add(lid);
                            setChanged();
                            notifyObservers(getNodeView(lid));
                        }
                    }
                }
            }
            if (visibleNodes.contains(node.getId())) {
                setChanged();
                notifyObservers(arg);
            }
            recalculateMaxDataAmount();
        }
    }

    private void recalculateMaxDataAmount() {
        maxDataAmount = 0;
        for (Integer id : visibleNodes) {
            Node node = level.getNodes().get(id);
            if (node.getOwner().equals(this)) {
                maxDataAmount += node.getHDD();
            }
        }
    }

    @Override
    public double getTimeRemaining() {
        return level.getTimeRemaining();
    }

    @Override
    public int getDataAmount() {
        return dataAmount;
    }

    public void setDataAmount(int dataAmount) {
        if (dataAmount > maxDataAmount) {
            this.dataAmount = maxDataAmount;
        } else if (dataAmount < 0) {
            this.dataAmount = 0;
        } else {
            this.dataAmount = dataAmount;
        }
    }

    @Override
    public int getMaxDataAmount() {
        return maxDataAmount;
    }

    @Override
    public List<Program> getProgramPrototypes() {
        return level.getProgramPrototypes();
    }
}
