package com.netrunnergame.core;

import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.Timer;
import com.google.common.base.Function;
import com.netrunnergame.core.programs.*;
import com.netrunnergame.utils.PairInt;

import java.util.*;

/**
 * Main game level class
 * <p>
 * TODO write how use with example
 *
 * @author Kirill Smirnov
 * @author Kots Michael
 * @author Pozigun Mikhail
 * @version 2.0
 * @see LevelController
 */
public class Level extends Observable {
    /**
     * Delay between updatings of level.
     */
    public static final float UPDATE_RATE = (float) (1.0 / 1000.0);
    /**
     * The Name of the default(neutral) player.
     */
    public static final String defaultPlayerName = "";

    public static String levelHelp = "";
    /**
     * The object which provide the access to the inner structure of the level.
     */
    private Representative representative = new RepresentativeLevel();
    /**
     * Set of all node types existed in this level.
     */
    private Map<Integer,Node> nodes = new TreeMap<>();

    private Set<String> types = new HashSet<>();
    /**
     * Object that contain all string messages from the level.
     */
    private Log log = new Log();
    /**
     * Contains the objects of player. For every players key is name this player
     */
    private Map<String, Player> players = new TreeMap<String, Player>() {{
        put(defaultPlayerName, new Player(representative, defaultPlayerName));
    }};
    /**
     * The object that evaluate the method update in separate thread
     * with rate equal UPDATE_RATE.
     */
    private Timer timer;
    /**
     * TODO remove it
     * just a kostyl
     */
    private String mainPlayer;
    /**
     * The time that remaining to end of level
     */
    private double timeRemaining = 0;
    /**
     * The field contain the object of player that win.
     * If there is no winner it's null
     */
    private Player winner = null;
    /**
     * The list of all available program. It contains their prototypes
     */
    private List<Program> programPrototypes;/* = Collections.unmodifiableList(new ArrayList<Program>() {{
        add(new AccelerationProgram());
        add(new AdvancedAttackProgram());
        add(new AttackProgram());
        add(new DataMiningProgram());
        add(new DefensiveProgram());
        add(new DistrCompProgram());
        add(new FirewallProgram());
        add(new MaskProgram());
        add(new SpyProgram());
    }});*/
    /**
     * It is function that check the condition of victory.
     * If condition matches, this function return winner else it return {@code null}
     */
    private Function<Representative, LevelController> gameOver =
            new Function<Representative, LevelController>() {
                @Override
                public LevelController apply(Representative lvl) {
                    if (lvl.getTimeRemaining() == 0) {
                        return lvl.getDefaultPlayer();
                    }
                    LevelController player = null;
                    for (Node node : lvl.getNodes().values()) {
                        if (node.getType().equals("green")) {
                            if (player == null) {
                                player = node.getOwner();
                            } else {
                                if (player != node.getOwner()) {
                                    return null;
                                }
                            }
                        }
                    }
                    if (!lvl.getDefaultPlayer().equals(player)) {
                        return player;
                    } else {
                        return null;
                    }
                }
            };


    /**
     * return builder.
     *
     * @return the Builder of Level
     * @see LevelBuilder
     */
    public static LevelBuilder builder() {
        return (new Level()).new LevelBuilder();
    }

    /**
     * get the object which provide the access to the inner structure of the level.
     * <p>
     * !only for tests!
     *
     * @return the object which provide the access to the inner structure of the level
     * @see Representative
     */
    public Representative getRepresentative() {
        return representative;
    }

    /**
     * @return the Object that represent player data and abilities
     */
    public LevelController player() {
        return this.players.get(mainPlayer);
    }


    /**
     * Main method of the Level.
     * <p>
     * It updates state of level by running all programs and select the winner.
     * This method is executed by timer with the rate equals UPDATE_RATE.
     */
    private void update() {
        if (winner == null) {
            timeRemaining = timeRemaining > 0 ? timeRemaining - UPDATE_RATE : 0.0f;
            for (Node n : nodes.values()) {
                for (Program p : n.getCores()) {
                    if (p != null) {
                        if (!p.update()) {
                            p.getHost().stopProgram(p.getCore());
                        }
                    }
                }
            }
            winner = (Player) gameOver.apply(representative);
            if (winner != null) {
                timer.stop();
                timer.clear();
                setChanged();
                notifyObservers();
            }
        }
    }

    public void run() {
        timer = new Timer();
        timer.scheduleTask(new Timer.Task() {
                               @Override
                               public void run() {
                                   Level.this.update();

                               }
                           }
                , 0
                , UPDATE_RATE
        );
        timer.stop();
    }

    /**
     * Proxy class for constructing instances of Level class.
     * <p>
     * Example:
     * {@code
     * Level lvl = Level.builder()
     * .setTimeRemaining(8945.000f)    //02:29:05
     * .newPlayer("player")
     * .newNode(1, "blue", "player", new Vector3(0, 0, 0.1f), 2, 1, 3, 100)
     * .linkFromTo(1, 2)
     * .linkFromTo(1, 3)
     * .newNode(2, "blue", null, new Vector3(-0.5f * 4.1f, 0.866f * 4.0f, 2.1f), 10, 3, 10, 1)
     * .linkFromTo(2, 1)
     * .newNode(3, "blue", null, new Vector3(0.5f * 4.1f, 0.866f * 4.1f, 2.2f), 6, 1, 6, 1)
     * .linkFromTo(3, 1)
     * .linkFromTo(3, 2)
     * .runProgram(new FirewallProgram(), 2)
     * .addFile(new DataFile(10), 2)
     * .addFile(new NetMapFile(), 3)
     * .build()
     * }
     *
     * @author Kirill Smirnov
     * @author Kots Michael
     * @version 1.1
     * @see Level
     */
    public class LevelBuilder {
        private final List<PairInt> links = new LinkedList<>();
        private final List<Program> programs = new ArrayList<>();

        private LevelBuilder() {
        }

        public void setLevelHelp(String lvlHelp) {
            levelHelp = lvlHelp;
        }

        /**
         * Creates a new node with this parameters
         *
         * @param type       any string
         * @param owner      name of player whom is a owner this node
         * @param pos        is position this node in a space
         * @param coreNumber is max number of programs those can execute in one time
         * @param clockSpeed is coefficient that affects the power of programs
         * @param RAM        is resource which is required by programs
         * @param HDD        is resource which is required by players to store the data
         */
        public LevelBuilder newNode(int id, String type, String owner, Vector3 pos, int coreNumber, int clockSpeed, int RAM, int HDD) {
            nodes.put(id, new Node(Level.this.representative, id, type, owner, pos, coreNumber, clockSpeed, RAM, HDD));
            types.add(type);
            return this;
        }

        /**
         * Create link from the node with id "idFrom" to the node with id "idTo"
         * Both nodes can be created after adding this link
         */
        public LevelBuilder linkFromTo(int idFrom, int idTo) {
            links.add(new PairInt(idFrom, idTo));
            return this;
        }

        /**
         * Set the duration of the level
         */
        public LevelBuilder setTimeOfGame(double time) {
            if (time >= 0.0f) {
                timeRemaining = time;
            }
            return this;
        }

        /**
         * Adds a new player to the level
         *
         * @param name name of a new player
         */
        public LevelBuilder newPlayer(String name) {
            players.put(name, new Player(representative, name));
            //TODO remove this shit
            mainPlayer = name;
            return this;
        }

        /**
         * Puts the file to the node
         *
         * @param nodeID id of the node
         * @param file   is the file that will be added
         * @see File
         */
        public LevelBuilder addFile(File file, int nodeID) {
            file.setHost(nodes.get(nodeID));
            nodes.get(nodeID).addFile(file);
            return this;
        }

        /**
         * Run the program to the node
         *
         * @param nodeID  id of the node
         * @param program is the program that will be ran
         * @see Program
         */
        public LevelBuilder runProgram(Program program, int nodeID) {
//            if (program instanceof MaskProgram) {
//                ((Player) Level.this.player()).addMaskedNode(nodeID);
//            }
            program.setHost(nodes.get(nodeID));
            nodes.get(nodeID).runProgram(program);
            return this;
        }

        public LevelBuilder addLevelProgram(Program program) {
            programs.add(program);
            return this;
        }

        /**
         * Builds the level
         *
         * @return the level
         */
        public Level build() {
            for (PairInt link : links) {
                Node from = nodes.get(link.getA());
                Node to = nodes.get(link.getB());
                from.addLinkWith(to);
            }
            for (Player player : players.values()) {
                player.updatePlayer();
            }
            Level.this.programPrototypes = Collections.unmodifiableList(programs);
            Level.this.player().setAvailablePrograms(Level.this.programPrototypes);

            return Level.this;
        }
    }

    /**
     * This class provide the access to the inner structure of the level.
     * Only Level can create the instance of this class
     *
     * @author Kirill Smirnov
     * @version 1.1
     * @see Level
     * @see Node
     * @see Player
     */
    private class RepresentativeLevel implements Representative {
        private RepresentativeLevel() {
        }

        @Override
        public String getLevelHelp() {
            return levelHelp;
        }

        @Override
        public Player getDefaultPlayer() {
            return players.get(defaultPlayerName);
        }

        @Override
        public double getTimeRemaining() {
            return timeRemaining;
        }

        @Override
        public void setTimeRemaining(double time) {
            timeRemaining = time;
        }

        @Override
        public Map<Integer,Node> getNodes() {
            return nodes;
        }

        @Override
        public Set<String> getTypesOfNode() {
            return Collections.unmodifiableSet(types);
        }

        @Override
        public Map<String, Player> getPlayers() {
            return Collections.unmodifiableMap(players);
        }

        @Override
        public Player getWinner() {
            return winner;
        }

        @Override
        public void start() {
            if (winner == null) {
                timer.start();
            }
        }

        @Override
        public void pause() {
            timer.stop();
        }

        @Override
        public boolean isAbleToRunProgram(Program program, int hostID) {
            program.setHost(nodes.get(hostID));
            return isAbleToRunProgram(program);
        }

        @Override
        public boolean isAbleToRunProgram(Program program) {
            if (program.getHost() != null) {
                return program.getHost().isAbleToRunProgram(program);
            }
            return false;
        }

        @Override
        public boolean runProgram(Program program, int hostID) {
            program.setHost(nodes.get(hostID));
            return runProgram(program);
        }

        @Override
        public boolean runProgram(Program program) {
            if (program.getHost() != null) {
                return program.getHost().runProgram(program);
            }
            return false;
        }

        @Override
        public void stopProgram(int nodeID, int coreNumber) {
            if (nodes.get(nodeID) != null) {
                nodes.get(nodeID).stopProgram(coreNumber);
            }
        }

        @Override
        public void stopProgram(Program p) {
            stopProgram(p.getHost().getId(), p.getCore());
        }

        @Override
        public void addLevelObserver(Observer o) {
            addObserver(o);
        }

        @Override
        public void notifyObservers(Object arg) {
            setChanged();
            Level.this.notifyObservers(arg);
        }

        @Override
        public void log(String text) {
            log.logMessage(text);
            notifyObservers(log);
        }

        @Override
        public void log(String obj, String text) {
            log(obj + "::" + text);
        }

        @Override
        public List<Program> getProgramPrototypes() {
            return Collections.unmodifiableList(programPrototypes);
        }

        public List<Program> getLevelAvailablePrograms() {
            return Collections.unmodifiableList(programPrototypes);
        }

    }
}
