package com.netrunnergame.core;

import com.badlogic.gdx.math.Vector3;
import com.netrunnergame.utils.NonExistentNodeIdException;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Interface for obtaining graphical properties of graph's nodes
 * <p>
 * TODO write more detailed description
 *
 * @author Kirill Smirnov, Kots Michael, Pozigun Mikhail
 * @version 1.2
 * @see Node
 */
public interface NodeView {
    /**
     * Gets position.
     *
     * @return node position
     */
    Vector3 getPosition();

    /**
     * Gets type.
     *
     * @return node type
     */
    String getType();

    /**
     * Gets id.
     *
     * @return node id
     */
    int getId();

    /**
     * Gets linked nodes.
     * <p>
     * Node can be linked with other nodes by a directed links.
     *
     * @return the iterator thought linked nodes
     */
    Iterator<NodeView> getLinkedNodes();

    /**
     * Number of links.
     * <p>
     * Node can be linked with other nodes by a directed links.
     *
     * @return the number of outgoing links.
     */
    int numOfLinks();

    /**
     * Gets program.
     *
     * @param coreNumber the core number
     * @return the program ran on this core
     */
    Program getProgram(int coreNumber);

    /**
     * Gets owner.
     *
     * @return the player which owns this node
     */
    LevelController getOwner();

    /**
     * Gets owner name.
     *
     * @return the name of the player which owns this node
     */
    String getOwnerName();

    /**
     * compare name of its owner and parameter
     *
     * @param name name with which it compares
     * @return true if names is equal
     */
    boolean isSameOwner(String name);

    /**
     * Gets core number.
     * <p>
     * One core can perform one program so
     * number of cores equals a max number of running programs
     *
     * @return the core number
     */
    int getCoreNumber();

    /**
     * Gets clock speed.
     * <p>
     * Clock speed is the coefficient on which depends the power of the program
     * The higher its value, the more powerful the program
     *
     * @return the clock speed
     */
    double getClockSpeed();

    /**
     * Sets clock speed.
     * <p>
     * Clock speed is the coefficient on which depends the power of the program
     * The higher its value, the more powerful the program
     *
     * @param clockSpeed the clock speed
     */
    void setClockSpeed(double clockSpeed);

    /**
     * Gets ram.
     * <p>
     * RAM is resource that program take when it run.
     * When program stop it return RAM back
     *
     * @return the ram
     */
    double getRAM();

    /**
     * Sets RAM.
     * <p>
     * RAM is resource that program take when it run.
     * When program stop it return RAM back
     *
     * @param ram the ram
     */
    void setRAM(double ram);

    /**
     * Gets hdd.
     * <p>
     * TODO write what is hdd
     *
     * @return the hdd
     */
    double getHDD();

    /**
     * Sets hdd.
     * <p>
     * TODO write what is hdd
     *
     * @param hdd the hdd
     */
    void setHDD(double hdd);

    /**
     * Gets number of busy cores.
     * <p>
     * Get number of cores which are perform the programs now
     *
     * @return the number of busy cores
     */
    int getNumberOfBusyCores();

    /**
     * Gets files that store in the node.
     *
     * @return the files
     */
    List<File> getFiles();

    /**
     * Calculate a length of way from the current node.
     *
     * @param nodeId is end of way
     * @return the length of way
     * @throws NonExistentNodeIdException if the node with this id wasn't found
     */
    int calculateWayTo(int nodeId) throws NonExistentNodeIdException;

    /**
     * Gets status.
     *
     * @return the status
     */
    double getStatus();
}