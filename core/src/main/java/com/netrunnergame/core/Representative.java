package com.netrunnergame.core;

import com.badlogic.gdx.utils.IntMap;

import java.util.List;
import java.util.Map;
import java.util.Observer;
import java.util.Set;

/**
 * there is can be your advertising
 * TODO write a description
 *
 * @author Kirill Smirnov
 * @version 1.0
 * @see Level
 * @see Node
 * @see Player
 */
public interface Representative {

    String getLevelHelp();

    /**
     * Gets the default(neutral) player.
     *
     * @return the object of the neutral player
     */
    Player getDefaultPlayer();

    /**
     * Gets a time to the end of the level
     *
     * @return time in seconds
     */
    double getTimeRemaining();

    /**
     * Sets a time to the end of the level.
     *
     * @param time time in seconds
     */
    void setTimeRemaining(double time);

    /**
     * Gets all nodes that level contain
     *
     * @return Map of the nodes. Keys are node ids
     */
    Map<Integer,Node> getNodes();

    /**
     * Gets all types of node that level contain
     *
     * @return Set of types
     */
    Set<String> getTypesOfNode();

    /**
     * Gets all players that level contain
     *
     * @return Map of players. Keys are player names
     */
    Map<String, Player> getPlayers();

    /**
     * Gets a player who won
     *
     * @return object of player who won if level finished else null
     */
    Player getWinner();

    /**
     * Resumes level updating in its thread
     */
    void start();

    /**
     * Suspends level updating in its thread
     */
    void pause();

    boolean isAbleToRunProgram(Program program, int hostID);

    boolean isAbleToRunProgram(Program program);

    /**
     * Runs the program on the target node
     *
     * @param program program that will be launched
     * @param hostID  id of the target node
     * @return true if the program is successfully launched
     */
    boolean runProgram(Program program, int hostID);

    /**
     * Runs the program on the target node that set in the program
     *
     * @param program program that will be launched
     * @return true if the program is successfully launched
     */
    boolean runProgram(Program program);

    /**
     * Stops the program
     *
     * @param nodeID     id of node which processes the program
     * @param coreNumber number of a core of the node which processes the program
     */
    void stopProgram(int nodeID, int coreNumber);

    /**
     * Stops the program
     *
     * @param p the program that will be stopped
     */
    void stopProgram(Program p);

    void addLevelObserver(Observer o);

    void notifyObservers(Object arg);

    /**
     * Writes in the log
     *
     * @param text the text which will be wrote in the log
     */
    void log(String text);

    /**
     * Writes in the log
     * <p>
     * Example:
     * log("Node_3", "start program")
     * Output:
     * Node_3::start program
     *
     * @param obj  the name of object which write in the log
     * @param text the text which will be wrote in the log
     */
    void log(String obj, String text);

    /**
     * Gets program prototypes
     *
     * @return list of the prototypes
     */
    List<Program> getProgramPrototypes();
}
