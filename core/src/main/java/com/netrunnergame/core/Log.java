package com.netrunnergame.core;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * TODO refactoring is needed
 */
public class Log {

    private List<String> log = new CopyOnWriteArrayList<>();

    public Iterator<String> getLog() {
        return log.iterator();
    }

    public void logMessage(String message) {
        log.add(message);
    }

}