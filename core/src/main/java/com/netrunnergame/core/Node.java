package com.netrunnergame.core;

import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.IntIntMap;
import com.badlogic.gdx.utils.Queue;
import com.netrunnergame.utils.NonExistentNodeIdException;

import java.util.*;

/**
 * Class for containing information about graph vertexes
 * TODO u mast comment it!
 *
 * @author Kirill Smirnov
 * @author Kots Michael
 * @version 2.1
 * @see NodeView
 */
public class Node implements NodeView {
    private int id;
    private List<Node> linkedNodes = new LinkedList<>();
    private String type;
    private LevelController owner;
    private Vector3 position;
    private int coreNumber;
    private double clockSpeed;
    private double RAM;
    private double HDD;
    private double status;
    private List<Program> cores = new ArrayList<>();
    private List<File> files = new LinkedList<>();
    private Set<String> flags = new TreeSet<>();
    private Representative level;

    /**
     * Instantiates a new Node.
     *
     * @param level      the level
     * @param id         the id
     * @param type       the type
     * @param owner      the owner
     * @param position   the position
     * @param coreNumber the core number
     * @param clockSpeed the clock speed
     * @param RAM        the ram
     * @param HDD        the hdd
     */
    Node(Representative level, int id, String type, String owner, Vector3 position, int coreNumber, int clockSpeed, int RAM, int HDD) {
        this.position = position;
        this.type = type;
        this.owner = null;
        for (Player player : level.getPlayers().values()) {
            if (player.getName().equals(owner))
                this.owner = player;
        }
        if (this.owner == null) {
            this.owner = level.getDefaultPlayer();
        }
        this.id = id;
        this.coreNumber = coreNumber;
        this.clockSpeed = clockSpeed;
        this.RAM = RAM;
        this.HDD = HDD;
        for (int i = 0; i < coreNumber; i++)
            cores.add(null);
        status = 0;
        this.level = level;
    }

    /**
     * Sets flag.
     *
     * @param f the f
     */
    public void setFlag(String f) {
        flags.add(f);
    }

    /**
     * Drop flag.
     *
     * @param f the f
     */
    public void dropFlag(String f) {
        flags.remove(f);
    }

    /**
     * Gets flag.
     *
     * @param f the f
     * @return the flag
     */
    public boolean getFlag(String f) {
        return flags.contains(f);
    }


    /**
     * Discover.
     */
    public void discover() {
        for (File f : files) {
            f.discover(level);
        }
        files.clear();
    }


    @Override
    public List<File> getFiles() {
        return Collections.unmodifiableList(files);
    }

    /**
     * Add file.
     *
     * @param file the file
     */
    void addFile(File file) {
        files.add(file);
    }

    /**
     * Add link with.
     *
     * @param node the node
     */
    void addLinkWith(Node node) {
        linkedNodes.add(node);
        notifyLevel();
    }

    @Override
    public int calculateWayTo(int nodeId) throws NonExistentNodeIdException {
        //implementation of the Lee algorithm
        if (nodeId == getId()) {
            return 0;
        }
        IntIntMap dist = new IntIntMap();
        Queue<NodeView> queue = new Queue<>();
        queue.addFirst(this);
        dist.put(this.getId(), 0);
        while (queue.size > 0) {
            NodeView current = queue.removeLast();
            int curentDist = dist.get(current.getId(), -1) + 1;
            Iterator<NodeView> it = current.getLinkedNodes();
            while (it.hasNext()) {
                NodeView linkedNode = it.next();
                if (linkedNode.getId() == nodeId) {
                    return curentDist;
                } else {
                    if (dist.get(linkedNode.getId(), -1) == -1) {
                        dist.put(linkedNode.getId(), curentDist);
                        queue.addFirst(linkedNode);
                    }
                }
            }
        }
        throw new NonExistentNodeIdException(nodeId);
    }

    private void notifyLevel() {
        level.notifyObservers(this);
    }

    /**
     * Gets status.
     *
     * @return the status
     */
    @Override
    public double getStatus() {
        return status;
    }

    /**
     * Sets status.
     *
     * @param status the status
     */
    public void setStatus(double status) {
        if (status < 0.0) {
            status = 0.0;
        } else {
            this.status = status;
        }
        notifyLevel();

    }

    boolean isAbleToRunProgram(Program program) {
        assert (program.getHost().equals(this));
        for (int i = 0; i < coreNumber; i++) {
            if (cores.get(i) == null && program.getRAM() <= RAM) {
                return true;
            }
        }
        return false;
    }

    /**
     * Run program boolean.
     *
     * @param program the program
     * @return the boolean
     */
    boolean runProgram(Program program) {
        assert (program.getHost().equals(this));
        for (int i = 0; i < coreNumber; i++) {
            if (cores.get(i) == null && program.getRAM() <= RAM) {
                RAM -= program.getRAM();
                cores.set(i, program);
                program.run(level);
                notifyLevel();
                return true;
            }
        }
        return false;
    }

    /**
     * Stop program.
     *
     * @param coreNumber the core number
     */
    void stopProgram(int coreNumber) {
        if (cores.get(coreNumber) != null) {
            RAM += cores.get(coreNumber).getRAM();
            cores.get(coreNumber).dismiss();
            cores.get(coreNumber).setHost(null);
            cores.set(coreNumber, null);
            notifyLevel();
        }
    }

    @Override
    public Program getProgram(int coreNumber) {
        return cores.get(coreNumber);
    }

    /**
     * Gets cores.
     *
     * @return the cores
     */
    public List<Program> getCores() {
        return Collections.unmodifiableList(cores);
    }

    @Override
    public int numOfLinks() {
        return linkedNodes.size();
    }

    @Override
    public String getType() {
        return type;
    }

    /**
     * Sets type.
     *
     * @param type the type
     */
    public void setType(String type) {
        assert (level.getTypesOfNode().contains(type));
        this.type = type;
        notifyLevel();
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public LevelController getOwner() {
        return owner;
    }

    /**
     * Sets owner.
     *
     * @param owner the owner
     */
    public void setOwner(LevelController owner) {
        for (int i = 0; i < coreNumber; i++) {
            stopProgram(i);
        }
        this.owner = owner;
        setStatus(0);
        discover();
        notifyLevel();
    }

    @Override
    public String getOwnerName() {
        return (owner != null ? owner.getName() : null);
    }

    @Override
    public boolean isSameOwner(String name) {
        if (getOwner() == null) {
            return name == null;
        } else {
            return getOwnerName().equals(name);
        }
    }

    @Override
    public int getCoreNumber() {
        return coreNumber;
    }

    /**
     * Sets core number.
     *
     * @param coreNumber the core number
     */
    public void setCoreNumber(int coreNumber) {
        assert (coreNumber > 0);
        this.coreNumber = coreNumber;
        notifyLevel();
    }

    @Override
    public double getClockSpeed() {
        return clockSpeed;
    }

    @Override
    public void setClockSpeed(double clockSpeed) {
        assert (clockSpeed > 0);
        this.clockSpeed = clockSpeed;
        notifyLevel();
    }

    @Override
    public double getRAM() {
        return RAM;
    }

    public void setRAM(double RAM) {
        assert (RAM >= 0);
        this.RAM = RAM;
        notifyLevel();
    }

    @Override
    public double getHDD() {
        return HDD;
    }

    public void setHDD(double HDD) {
        assert (HDD >= 0);
        this.HDD = HDD;
        notifyLevel();
    }

    @Override
    public int getNumberOfBusyCores() {
        int busyCoresCount = 0;
        for (int i = 0; i < coreNumber; i++) {
            if (cores.get(i) != null) {
                busyCoresCount++;
            }
        }
        return busyCoresCount;
    }

    @Override
    public Iterator<NodeView> getLinkedNodes() {
        return new Iterator<NodeView>() {
            private final Iterator<Node> it = linkedNodes.iterator();

            @Override
            public boolean hasNext() {
                return it.hasNext();
            }

            @Override
            public NodeView next() {
                return it.next();
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("remove");
            }
        };
    }

    @Override
    public Vector3 getPosition() {
        return position;
    }

    /**
     * Log message.
     *
     * @param message the message
     */
    public void logMessage(String message) {
        level.log("Node_" + id, message);
    }

    /**
     * The type Node builder.
     */
    //this is not functional at all
    class NodeBuilder {
        private NodeBuilder(int id, Vector3 position) {
            Node.this.id = id;
            Node.this.position = position;
            Node.this.type = "default";
            Node.this.owner = null;
            Node.this.coreNumber = 1;
            Node.this.clockSpeed = 1;
            Node.this.RAM = 1;
            Node.this.HDD = 1;
            Node.this.status = 0;
        }

        /**
         * Sets type.
         *
         * @param type the type
         * @return the type
         */
        public NodeBuilder setType(String type) {
            Node.this.type = type;
            return this;
        }

        /**
         * Sets player.
         *
         * @param name the name
         * @return the player
         */
        public NodeBuilder setPlayer(String name) {
            if (Node.this.owner.getName().equals(name))
                Node.this.owner = level.getPlayers().get(name);
            return this;
        }

        /**
         * Sets core number.
         *
         * @param coreNumber the core number
         * @return the core number
         */
        public NodeBuilder setCoreNumber(int coreNumber) {
            Node.this.coreNumber = coreNumber;
            return this;
        }

        /**
         * Sets clock speed.
         *
         * @param clockSpeed the clock speed
         * @return the clock speed
         */
        public NodeBuilder setClockSpeed(double clockSpeed) {
            Node.this.clockSpeed = clockSpeed;
            return this;
        }

        /**
         * Sets ram.
         *
         * @param RAM the ram
         * @return the ram
         */
        public NodeBuilder setRAM(double RAM) {
            Node.this.RAM = RAM;
            return this;
        }

        /**
         * Sets hdd.
         *
         * @param HDD the hdd
         * @return the hdd
         */
        public NodeBuilder setHDD(double HDD) {
            Node.this.HDD = HDD;
            return this;
        }

        /**
         * Build node.
         *
         * @return the node
         */
        public Node build() {
            return Node.this;
        }
    }
}
