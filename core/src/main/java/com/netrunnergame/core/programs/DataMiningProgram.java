package com.netrunnergame.core.programs;

import com.netrunnergame.core.Level;
import com.netrunnergame.core.Player;
import com.netrunnergame.core.Program;

import java.util.List;

/**
 * Class of data mining program. 1 data per k/P ticks.
 *
 * @author Peto
 * @author Kirill Smirnov
 * @version 1.2
 */
public class DataMiningProgram extends GameProgram {
    private static final double k = 2;
    private double ticks = 0f;

    public DataMiningProgram() {
        super(
                0,
                3,
                "Data Mining",
                "This program add +1 to data"
        );
    }

    @Override
    public boolean update() {
        ticks += Level.UPDATE_RATE;
        if (ticks >= k / host.getClockSpeed()) {
            ticks = 0;
            Player player = (Player) host.getOwner();
            player.setDataAmount(host.getOwner().getDataAmount() + 1);
        }
        return true;
    }
}
