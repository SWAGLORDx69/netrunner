package com.netrunnergame.core.programs;

import com.netrunnergame.core.Level;
import com.netrunnergame.core.Node;
import com.netrunnergame.core.NodeView;
import com.netrunnergame.core.Player;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Program that captures nodes with a firewall
 *
 * @author Kirill Smirnov
 * @version 1.0
 */
public class ActiveProtectionProgram extends GameProgram {
    public static final double BASE_POWER = 0.01;
    public static final double TIME_MULTIPLIER = 0.1;
    private float ticks = 0;
    //private Node goal = null;

    public ActiveProtectionProgram() {
        super(
                0,
                0,
                "Active Protection",
                "This program will attack any node which attack host node of this program"
        );
    }


    @Override
    public boolean update() {
        Map<Integer, Node> it = host.getOwner().getNodes();
        for(int i = 1; i <= it.size(); i++) {
            Node node = it.get(i);
            if (node.getFlag("Attacking" + this.getHost().getId())) {
                ticks += Level.UPDATE_RATE;
                if (ticks >= TIME_MULTIPLIER / host.getClockSpeed()) {
                    ticks = 0;
                    if (!node.getFlag("AttackProtection")) {
                        node.setStatus(node.getStatus() + BASE_POWER / node.getClockSpeed());
                    }
                }

                if (node.getStatus() >= 1) {
                    node.setOwner(host.getOwner());
                }
            }
        }
        /*if (goal == null) {
            Iterator<NodeView> it = host.getLinkedNodes();
            while (it.hasNext()) {
                NodeView node = it.next();
                if (!host.getOwner().equals(node.getOwner())) {
                    goal = (Node) node;
                    break;
                }
            }
        } else {
            ticks += Level.UPDATE_RATE;
            if (ticks >= TIME_MULTIPLIER / host.getClockSpeed()) {
                ticks = 0;
                if (!goal.getFlag("AttackProtection")) {
                    goal.setStatus(goal.getStatus() + BASE_POWER / goal.getClockSpeed());
                }
            }

            if (goal.getStatus() >= 1) {
                goal.setOwner(host.getOwner());
                goal = null;
            }
        }*/
        return true;
    }
}
