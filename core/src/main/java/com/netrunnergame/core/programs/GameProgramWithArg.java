package com.netrunnergame.core.programs;

import com.netrunnergame.core.*;

/**
 * Game program with argument class
 *
 * @author Pozigun Mikhail
 * @author Kirill Smirnov
 * @version 1.2
 * @see Program
 */
public abstract class GameProgramWithArg extends GameProgram {
    /**
     * The node as argument for program.
     */
    protected Node arg = null;


    public GameProgramWithArg(double dataCost, double ram, String name, String info) {
        super(dataCost, ram, name, info);
    }

    @Override
    public void run(Representative level) {
        host.logMessage(name + " has been launched with argument \"node" + arg.getId() + "\"");
    }

    /**
     * get the arg
     *
     * @return the arg
     */
    public NodeView getArg() {
        return arg;
    }

    /**
     * Sets arg.
     *
     * @param arg the arg
     */
    public final void setArg(NodeView arg) {
        this.arg = (Node) arg;
    }
}
