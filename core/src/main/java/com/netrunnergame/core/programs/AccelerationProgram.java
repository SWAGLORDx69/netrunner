package com.netrunnergame.core.programs;

import com.netrunnergame.core.NodeView;
import com.netrunnergame.core.Program;

import java.util.ArrayList;
import java.util.List;

/**
 * Class of acceleration program. Increase clock speed of any friendly node
 *
 * @author Peto
 * @author Kirill Smirnov
 * @version 1.2
 */
public class AccelerationProgram extends GameProgramWithArg {
    private static final float k = 6000;
    private float accelerationTimes = 2;
    private float ticks = 0;

    public AccelerationProgram() {
        super(
                10,
                6,
                "Acceleration Program",
                "This program add +1 to clock speed of target computer"
        );
    }

    @Override
    public boolean update() {
        ticks++;
        if (ticks >= k / host.getClockSpeed()) {
            ticks = 0;
            accelerationTimes--;
            host.logMessage("Acceleration program add +1 speed on node" + arg.getId());
            arg.setClockSpeed(arg.getClockSpeed() + 1);
            if (accelerationTimes == 0) {
                return false;
            }
        }
        return true;
    }

    @Override
    public List<Integer> getIdOfPossibleArg() {
        List<Integer> idxs = new ArrayList<>();
        for (NodeView elem : host.getOwner()) {
            if (elem.getOwner() == host.getOwner() && elem != host) {
                idxs.add(elem.getId());
            }
        }
        return idxs;
    }
}
