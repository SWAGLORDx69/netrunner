package com.netrunnergame.core.programs;

import com.badlogic.gdx.Gdx;
import com.netrunnergame.core.Level;
import com.netrunnergame.core.NodeView;
import com.netrunnergame.core.Program;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Sample class of game program with argument. Does nothing
 *
 * @author Pozigun Mikhail
 * @author Kirill Smirnov
 * @version 1.2
 */
public class SampleProgramWithArg extends GameProgramWithArg {
    private float ticks = 0;

    /**
     * Instantiates a new Sample program with arg.
     */
    public SampleProgramWithArg() {
        super(
                1,
                1,
                "SampleProgramWithArg",
                "Sample program that does nothing with its argument"
        );
    }

    @Override
    public boolean update() {
        ticks += Level.UPDATE_RATE;
        if (ticks >= 10) {
            ticks = 0;
            host.logMessage("Sample program did nothing with argument " + arg.getId() + " on a node " + host.getId());
            Gdx.app.log("core", "Sample program did nothing with argument " + arg.getId() + " on a node " + host.getId());
        }
        return true;
    }
}
