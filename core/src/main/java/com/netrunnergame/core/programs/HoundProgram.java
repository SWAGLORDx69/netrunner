package com.netrunnergame.core.programs;

import com.netrunnergame.core.*;

import java.util.Iterator;
import java.util.Random;

/**
 * Program that likes to jump
 *
 * @author Kirill Smirnov
 * @version 1.0
 */
public class HoundProgram extends GameProgram {
    public static final double BASE_POWER = 0.01;
    public static final double TIME_MULTIPLIER = 0.1;
    public static final double JUMP_TIME = 10;
    private float ticks = 0;
    private Node goal = null;
    private Random gen;

    public HoundProgram() {
        super(
                0,
                0,
                "Hound Program",
                "your nightmare"
        );
    }

    @Override
    public void run(Representative level) {
        super.run(level);
        gen = new Random(Double.valueOf(level.getTimeRemaining()).hashCode() + host.hashCode() + level.hashCode());
    }

    @Override
    public boolean update() {
        ticks += Level.UPDATE_RATE;
        if (goal == null) {
            Iterator<NodeView> it = host.getLinkedNodes();
            while (it.hasNext()) {
                NodeView node = it.next();
                if (!host.getOwner().equals(node.getOwner())) {
                    goal = (Node) node;
                    break;
                }
            }

            if (ticks >= JUMP_TIME && host.numOfLinks() > 0) {
                ticks = 0;
                it = host.getLinkedNodes();
                for (int i = gen.nextInt(host.numOfLinks()); i > 0; i--) {
                    it.next();
                }
                Node nextNode = (Node) it.next();
                Program nextProg = this.clone();
                nextProg.setHost(nextNode);
                if (level.runProgram(nextProg)) {
                    return false;
                }
            }

        } else {
            if (ticks >= TIME_MULTIPLIER / host.getClockSpeed()) {
                ticks = 0;
                if (!goal.getFlag("AttackProtection")) {
                    goal.setStatus(goal.getStatus() + BASE_POWER / goal.getClockSpeed());
                }
            }

            if (goal.getStatus() >= 1) {
                goal.setOwner(host.getOwner());
                goal = null;
            }
        }
        return true;
    }
}
