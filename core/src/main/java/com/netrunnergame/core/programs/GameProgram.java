package com.netrunnergame.core.programs;

import com.netrunnergame.core.*;

import java.util.List;

/**
 * Defines common fields for game programs
 *
 * @author Kots Michael
 * @author Kirill Smirnov
 * @version 1.2
 * @see Program
 */
public abstract class GameProgram implements Program {
    /**
     * amount of data required for running this program
     */
    protected final double dataCost;
    /**
     * amount of memory required for running this program
     */
    protected final double ram;
    /**
     * name of this program
     */
    protected final String name;
    /**
     * information about this program
     */
    protected final String info;
    /**
     * pointer on node that runs this program
     */
    protected Node host = null;

    protected Representative level = null;

    public GameProgram(double dataCost, double ram, String name, String info) {
        this.dataCost = dataCost;
        this.ram = ram;
        this.name = name;
        this.info = info;
    }

    @Override
    public void run(Representative level) {
        this.level = level;
        host.logMessage(name + " program has been launched");
    }

    @Override
    public void dismiss() {
        this.level = null;
        host.logMessage(name + " program was terminated");
    }

    @Override
    public final String getName() {
        return name;
    }

    @Override
    public final double getRAM() {
        return ram;
    }

    @Override
    public final double getDataCost() {
        return dataCost;
    }

    @Override
    public final String getInfo() {
        return info;
    }

    @Override
    public final Node getHost() {
        return host;
    }

    @Override
    public final void setHost(NodeView host) {
        this.host = (Node) host;
    }

    @Override
    public final int getCore() {
        for (int i = 0; i < host.getCoreNumber(); i++)
            if (host.getProgram(i) == this)
                return i;
        return -1;
    }

    @Override
    public GameProgram clone() {
        //I don't sure that is good solution
        try {
            return (GameProgram) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @Override
    public List<Integer> getIdOfPossibleArg() {
        return null;
    }

}