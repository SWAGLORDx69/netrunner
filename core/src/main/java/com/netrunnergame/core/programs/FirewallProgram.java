package com.netrunnergame.core.programs;

import com.netrunnergame.core.Node;
import com.netrunnergame.core.NodeView;
import com.netrunnergame.core.Program;
import com.netrunnergame.core.Representative;

import java.util.List;

/**
 * Class of firewall program. this program make node immortal.
 *
 * @author Kirill Smirnov
 * @version 2.0
 * @see Program
 */
public class FirewallProgram extends GameProgram {
    public FirewallProgram() {
        super(
                0,
                0,
                "Firewall Program",
                "This program is absolute protection"
        );
    }

    public FirewallProgram(double dataCost, double ram) {
        super(
                dataCost,
                ram,
                "Firewall Program",
                "This program is absolute protection"
        );
    }

    @Override
    public void run(Representative level) {
        super.run(level);
        host.setFlag("AttackProtection");
    }

    @Override
    public boolean update() {
        return true;
    }

    @Override
    public void dismiss() {
        super.dismiss();
        host.dropFlag("AttackProtection");
    }
}