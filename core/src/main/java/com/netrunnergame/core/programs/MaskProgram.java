package com.netrunnergame.core.programs;

import com.netrunnergame.core.Program;
import com.netrunnergame.core.Representative;

import java.util.List;

/**
 * Class of mask program for neutral nodes. Make visible neighbour nodes with target neutral node
 *
 * @author Peto
 * @author Kirill Smirnov
 * @version 2.0
 */
public class MaskProgram extends GameProgram {

    public MaskProgram() {
        super(
                0,
                0,
                "Mask Program",
                "Mask program ignore spy program on this node"
        );
    }

    public void run(Representative level) {
        super.run(level);
        host.setFlag("Hidden");
    }

    public boolean update() {
        return true;
    }

    public void dismiss() {
        super.dismiss();
        host.dropFlag("Hidden");
    }

}
