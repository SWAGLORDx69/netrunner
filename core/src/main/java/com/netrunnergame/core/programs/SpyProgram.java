package com.netrunnergame.core.programs;

import com.netrunnergame.core.*;
import com.netrunnergame.utils.NonExistentNodeIdException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Class of spy program. Make visible neighbour nodes with target neutral node
 *
 * @author Peto
 * @author Kirill Smirnov
 * @version 2.0
 */
public class SpyProgram extends GameProgramWithArg {
    private static final double k = 2.5;
    private double ticks = 0;
    private int distToArg = 1;


    public SpyProgram() {
        super(
                1,
                1,
                "Spy Program",
                "This program make visible all neighbours of target neutral computer"
        );
    }

    @Override
    public void run(Representative level) {
        super.run(level);
        try {
            distToArg = host.calculateWayTo(arg.getId());
        } catch (NonExistentNodeIdException e) {

        }
    }

    @Override
    public boolean update() {
        ticks += Level.UPDATE_RATE;

        if (ticks >= k * distToArg / host.getClockSpeed() &&
                !arg.getFlag("Hidden")) {
            Iterator<NodeView> it = arg.getLinkedNodes();
            while (it.hasNext()) {
                NodeView cur = it.next();
                if (!host.getOwner().isVisibleNode(cur.getId())) {
                    ((Player)host.getOwner()).setNodeVisible(cur.getId());
                }
            }
            return false;
        }
        return true;
    }

    @Override
    public List<Integer> getIdOfPossibleArg() {
        List<Integer> idxs = new ArrayList<>();
        for (NodeView elem : host.getOwner()) {
            if ((elem.getOwner() != host.getOwner()) && !host.getOwner().isMaskedNode(elem.getId())) {
                idxs.add(elem.getId());
            }
        }
        return idxs;
    }

}
