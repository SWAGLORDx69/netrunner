package com.netrunnergame.core.programs;

import com.netrunnergame.core.Level;

/**
 * Sample class of game program. Does nothing
 *
 * @author Kots Michael
 * @author Kirill Smirnov
 * @version 1.2
 */
public class SampleProgram extends GameProgram {
    private float ticks = 0;

    public SampleProgram() {
        super(
                10,
                0,
                "SampleProgram",
                "Sample program that does nothing"
        );
    }

    @Override
    public boolean update() {
        ticks += Level.UPDATE_RATE;
        if (ticks >= 10) {
            ticks = 0;
            host.logMessage("Sample program did nothing on a node " + host.getId());
            return false;
        }
        return true;
    }
}
