package com.netrunnergame.core.programs;

import com.netrunnergame.core.Level;
import com.netrunnergame.core.NodeView;
import com.netrunnergame.core.Representative;
import com.netrunnergame.utils.NonExistentNodeIdException;

import java.util.ArrayList;
import java.util.List;

/**
 * Class of defensive program. Decrease  degree of capture node
 *
 * @author Peto
 * @author Kirill Smirnov
 * @version 1.2
 */
public class DefensiveProgram extends GameProgramWithArg {
    private static final double k = 1;
    private double ticks = 0;
    private int distToArg = 1;

    public DefensiveProgram() {
        super(
                4,
                1,
                "Defensive Program",
                "Defensive program decrease degree of capture node every several ticks on 1%"
        );
    }

    @Override
    public void run(Representative level) {
        super.run(level);
        try {
            distToArg = host.calculateWayTo(arg.getId());
        } catch (NonExistentNodeIdException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean update() {
        ticks += Level.UPDATE_RATE;

        if (ticks >= (k / host.getClockSpeed()) * (distToArg + 1)) {
            ticks = 0;
            if (arg.getStatus() > 0) {
                arg.setStatus(arg.getStatus() - 0.01);
            }
        }

        return true;
    }

    @Override
    public List<Integer> getIdOfPossibleArg() {
        List<Integer> idxs = new ArrayList<>();
        for (NodeView elem : host.getOwner()) {
            if (elem.getOwner() == host.getOwner()) {
                idxs.add(elem.getId());
            }
        }
        return idxs;
    }
}
