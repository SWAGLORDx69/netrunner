package com.netrunnergame.core.programs;

import com.netrunnergame.core.Level;
import com.netrunnergame.core.NodeView;
import com.netrunnergame.core.Program;
import com.netrunnergame.core.Representative;
import com.netrunnergame.utils.NonExistentNodeIdException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Program that captures neutral nodes
 *
 * @author Kots Michael
 * @author Kirill Smirnov
 * @version 2.0
 */
public class AttackProgram extends GameProgramWithArg {
    public static final double BASE_POWER = 0.01;
    public static final double TIME_MULTIPLIER = 0.1;
    private int distToArg = 1;
    private float ticks = 0;

    public AttackProgram() {
        super(
                3,
                2,
                "Attack Program",
                "This program captures the neutral node. It can be ran on any of your computers and target " +
                        "any neutral node that adjacent to any of your nodes."
        );
    }

    @Override
    public void run(Representative level) {
        super.run(level);
        try {
            distToArg = host.calculateWayTo(arg.getId());
            this.getHost().setFlag("Attacking" + arg.getId());
        } catch (NonExistentNodeIdException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
        this.getHost().dropFlag("Attacking" + arg.getId());
    }

    @Override
    public boolean update() {
        ticks += Level.UPDATE_RATE;
        if (ticks >= TIME_MULTIPLIER * distToArg / host.getClockSpeed()) {
            ticks = 0;
            if (!arg.getFlag("AttackProtection")) {
                arg.setStatus(arg.getStatus() + BASE_POWER / arg.getClockSpeed());
            }
        }

        if (arg.getStatus() >= 1) {
            arg.setOwner(host.getOwner());
        }

        return !arg.getOwner().equals(host.getOwner());
    }

    @Override
    public List<Integer> getIdOfPossibleArg() {
        List<Integer> idxs = new ArrayList<>();
        for (NodeView elem : host.getOwner()) {
            if (elem.getOwner() == host.getOwner()) {
                Iterator<NodeView> it = elem.getLinkedNodes();
                while (it.hasNext()) {
                    NodeView node = it.next();
                    if (node.getOwner() != elem.getOwner())
                        idxs.add(node.getId());
                }
            }
        }
        return idxs;
    }

}
