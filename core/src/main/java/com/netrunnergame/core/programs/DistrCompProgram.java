package com.netrunnergame.core.programs;

import com.netrunnergame.core.Level;
import com.netrunnergame.core.NodeView;
import com.netrunnergame.core.Program;

import java.util.ArrayList;
import java.util.List;

/**
 * Class of distributed computing program.
 *
 * @author Pozigun Mikhail
 * @author Kirill Smirnov
 * @version 1.2
 */
public class DistrCompProgram extends GameProgramWithArg {
    /**
     * The time of loading constant.
     */
    private static final float LOADING_TIME = 5.0f;//sec
    /**
     * The ticks count.
     */
    private float ticks = 0.0f;
    /**
     * Is the program loaded.
     */
    private boolean isLoaded = false;

    /**
     * Instantiates a new Distributed computing program.
     */
    public DistrCompProgram() {
        super(
                1,
                1,
                "Distributed Computing",
                "Program that give 1 unit of its RAM to argument computer after " + LOADING_TIME + "s."
        );
    }

    @Override
    public boolean update() {
        if (!isLoaded) {
            ticks += Level.UPDATE_RATE;
            if (ticks >= LOADING_TIME) {
                ticks = 0;
                arg.setRAM(arg.getRAM() + ram);
                host.logMessage("Distributed computing program loaded on node " + host.getId() + " and gave" + ram + " unit of its Ram to node " + arg.getId());
                isLoaded = true;
            }
        }
        return true;
    }

    @Override
    public void dismiss() {
        if (arg.getRAM() == 0) {
            host.setRAM(host.getRAM() - ram);
            return;
        }
        arg.setRAM(arg.getRAM() - ram);
        super.dismiss();
    }

    @Override
    public List<Integer> getIdOfPossibleArg() {
        List<Integer> idxs = new ArrayList<>();
        for (NodeView elem : host.getOwner()) {
            if (elem.getOwner() == host.getOwner() && elem != host) {
                idxs.add(elem.getId());
            }
        }
        return idxs;
    }
}
