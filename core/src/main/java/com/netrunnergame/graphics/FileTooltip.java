package com.netrunnergame.graphics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.netrunnergame.NetRunnerGame;
import com.netrunnergame.core.File;
import com.netrunnergame.core.Program;

/**
 * Tooltip for file(chosen in context menu).
 *
 * @author Pozigun Mikhail
 * @version 0.1
 * @see Stage
 */
public class FileTooltip extends Stage {
    /**
     * The constant width of every row in context menu table.
     */
    private static int TABLE_ROW_WIDTH;
    /**
     * The constant padding for text (left and right).
     */
    private static int PAD_RIGHT_LEFT;
    /**
     * The constant padding for text (top).
     */
    private static int PAD_TOP_TEXT;
    /**
     * The constant padding for text (bottom).
     */
    private static int PAD_BOTTOM_TEXT;
    /**
     * The constant padding for name (top and bottom).
     */
    private static int PAD_TOP_BOTTOM_NAME;
    /**
     * The table with info about file.
     */
    private Table fileTooltipTable;
    /**
     * The game (here for getting fonts).
     */
    private NetRunnerGame game;
    /**
     * The skin of all buttons.
     */
    private Skin skin = new Skin();
    /**
     * The text button with common info about file.
     */
    private TextButton fileInfo;
    /**
     * The text button with file name, title for tooltip.
     */
    private TextButton fileName;
    /**
     * The viewport screen height.
     */
    private float screenHeight;
    /**
     * The viewport screen width.
     */
    private float screenWidth;
    /**
     * Is show context menu.
     */
    private boolean isShow = false;

    /**
     * Instantiates a new File tooltip.
     *
     * @param game the game
     */
    public FileTooltip(NetRunnerGame game) {
        super(new ExtendViewport(480, 800));
        screenWidth = super.getViewport().getWorldWidth();
        screenHeight = super.getViewport().getWorldHeight();
        initSizes();

        this.game = game;

        fileTooltipTable = new Table();
        fileTooltipTable.setVisible(false);
        addActor(fileTooltipTable);
        initSkin();
        //fill table
        fileInfo = new TextButton("", skin);
        fileInfo.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                hide();
            }
        });
        fileName = new TextButton("", skin, "head");
        fileName.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                hide();
            }
        });
        fileName.getLabel().setAlignment(Align.center);
        fileInfo.getLabel().setAlignment(Align.left);
        fileInfo.getLabel().setWrap(true);
        fileName.getLabel().setWrap(true);
        fileName.padBottom(PAD_TOP_BOTTOM_NAME).padTop(PAD_TOP_BOTTOM_NAME);
        fileInfo.padLeft(PAD_RIGHT_LEFT).padRight(PAD_RIGHT_LEFT).padBottom(PAD_BOTTOM_TEXT).padTop(PAD_TOP_TEXT);
        fileTooltipTable.add(fileName).width(TABLE_ROW_WIDTH).row();
        fileTooltipTable.add(fileInfo).width(TABLE_ROW_WIDTH).row();
    }

    /**
     * Init sizes.
     */
    private void initSizes() {
        TABLE_ROW_WIDTH = (int) (screenWidth / 6.0f);
        PAD_RIGHT_LEFT = TABLE_ROW_WIDTH / 16;
        PAD_TOP_BOTTOM_NAME = TABLE_ROW_WIDTH / 14;
        PAD_TOP_TEXT = TABLE_ROW_WIDTH / 16;
        PAD_BOTTOM_TEXT = TABLE_ROW_WIDTH / 14;
    }

    /**
     * Init skin.
     */
    private void initSkin() {
        skin.add("default", game.getFont("text"));
        skin.add("head", game.getFont("button"));
        Pixmap pixmap = new Pixmap(TABLE_ROW_WIDTH, PAD_RIGHT_LEFT, Pixmap.Format.RGB888);
        pixmap.setColor(Color.WHITE);
        pixmap.fill();
        skin.add("background", new Texture(pixmap));

        TextButton.TextButtonStyle textButtonStyleInfo = new TextButton.TextButtonStyle();
        textButtonStyleInfo.up = skin.newDrawable("background", game.getColor("text background"));
        textButtonStyleInfo.font = skin.getFont("default");
        textButtonStyleInfo.fontColor = game.getColor("text");
        skin.add("default", textButtonStyleInfo);

        TextButton.TextButtonStyle textButtonStyleHead = new TextButton.TextButtonStyle();
        textButtonStyleHead.up = skin.newDrawable("background", game.getColor("head background"));
        textButtonStyleHead.font = skin.getFont("head");
        textButtonStyleHead.fontColor = game.getColor("text head");
        skin.add("head", textButtonStyleHead);
    }

    /**
     * Is context menu is visible.
     */
    public boolean isVisible() {
        return isShow;
    }

    /**
     * Hide tooltip.
     */
    public void hide() {
        isShow = false;
        fileTooltipTable.setVisible(false);
    }

    /**
     * Show context menu.
     */
    public void show() {
        isShow = true;
        fileTooltipTable.setVisible(true);
    }

    @Override
    public void draw() {
        if (isShow) {
            super.draw();
        }
    }

    /**
     * Sets tooltip for chosen file.
     *
     * @param pos  the pos
     * @param file the file
     */
    public void setFile(Vector2 pos, File file) {
        Gdx.app.log("setProgram", "Set tooltip for file " + file.getName());
        fileName.setText(file.getName());
        fileInfo.setText(file.getInfo());

        fileTooltipTable.pack();
        fileTooltipTable.setPosition(pos.x - Math.max(0, pos.x + fileTooltipTable.getWidth() - screenWidth),
                pos.y - Math.max(0, pos.y + fileTooltipTable.getHeight() - screenHeight));
        isShow = true;
    }

}