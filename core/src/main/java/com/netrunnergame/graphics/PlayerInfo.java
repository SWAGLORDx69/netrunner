package com.netrunnergame.graphics;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.netrunnergame.NetRunnerGame;
import com.netrunnergame.core.LevelController;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Class for time remaining and data amount labels.
 *
 * @author Pozigun Mikhail
 * @version 0.1
 * @see Stage
 */
public class PlayerInfo extends Stage {
    /**
     * The viewport screen height.
     */
    private float screenHeight;
    /**
     * The viewport screen width.
     */
    private float screenWidth;
    /**
     * The label with time.
     */
    private Label timeRemainingLabel;
    /**
     * The label with data.
     */
    private Label dataAmountLabel;
    /**
     * The level.
     */
    private LevelController level;

    /**
     * Instantiates a new Player info.
     *
     * @param game  the game
     * @param level the level
     */
    public PlayerInfo(final NetRunnerGame game, final LevelController level) {
        super(new ExtendViewport(480, 800));

        this.level = level;

        screenWidth = super.getViewport().getWorldWidth();
        screenHeight = super.getViewport().getWorldHeight();

        Skin skin = new Skin();
        skin.add("default", game.getFont("info"));
        Label.LabelStyle labelStyle = new Label.LabelStyle();
        labelStyle.font = skin.getFont("default");
        skin.add("default", labelStyle);
        timeRemainingLabel = new Label("Time: " + getFormattedTimeString((float) level.getTimeRemaining()), skin);
        dataAmountLabel = new Label("Data: " + level.getDataAmount() + "/" + level.getMaxDataAmount(), skin);
        timeRemainingLabel.setVisible(true);
        dataAmountLabel.setVisible(true);
        timeRemainingLabel.setX(screenWidth / 2 - timeRemainingLabel.getWidth());
        timeRemainingLabel.setY(screenHeight - timeRemainingLabel.getHeight());
        dataAmountLabel.setX(0);
        dataAmountLabel.setY(dataAmountLabel.getHeight());
        addActor(timeRemainingLabel);
        addActor(dataAmountLabel);
    }

    @Override
    public void draw() {
        timeRemainingLabel.setText("Time: " + getFormattedTimeString((float) level.getTimeRemaining()));
        timeRemainingLabel.setX(screenWidth / 2 - timeRemainingLabel.getWidth() / 2);
        timeRemainingLabel.setY(screenHeight - timeRemainingLabel.getHeight());
        dataAmountLabel.setText("Data: " + level.getDataAmount() + "/" + level.getMaxDataAmount());
        super.draw();
    }

    /**
     * Get formatted hh:mm:ss string from time remaining.
     */
    private String getFormattedTimeString(float time) {
        return String.format(Locale.getDefault(), "%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours((int) (time * 1000)),
                TimeUnit.MILLISECONDS.toMinutes((int) (time * 1000)) % TimeUnit.HOURS.toMinutes(1),
                TimeUnit.MILLISECONDS.toSeconds((int) (time * 1000)) % TimeUnit.MINUTES.toSeconds(1));
    }

}
