package com.netrunnergame.graphics;


import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.netrunnergame.NetRunnerGame;
import com.netrunnergame.core.LevelController;

/**
 * Class for error message for player.
 *
 * @author Pozigun Mikhail
 * @version 0.1
 * @see Stage
 */
public class ErrorMsgForPlayer extends Stage {
    /**
     * The constant width of log panel.
     */
    private static int MSG_WIDTH;
    /**
     * The constant padding for text of messages(left and right).
     */
    private static int PAD_RIGHT_LEFT;
    /**
     * The constant padding for text of messages(top and bottom).
     */
    private static int PAD_TOP_BOTTOM;
    /**
     * The constant height of log and menu buttons.
     */
    private static int BUTTON_HEIGHT;
    /**
     * The text button with message.
     */
    private TextButton message;
    /**
     * The level.
     */
    private LevelController level;
    /**
     * The game.
     */
    private NetRunnerGame game;
    /**
     * The start time of showing message.
     */
    private float timeStart;
    /**
     * The time of showing of message.
     */
    private final float TIME_DELTA = 1.0f;//seconds
    /**
     * The skin.
     */
    private Skin skin;
    /**
     * Is to show message or not.
     */
    private boolean isShow = false;
    /**
     * The viewport screen width.
     */
    private float screenWidth;
    /**
     * The viewport screen width.
     */
    private float screenHeight;

    /**
     * Instantiates a new Player info.
     *
     * @param game  the game
     * @param level the level
     */
    public ErrorMsgForPlayer(NetRunnerGame game, LevelController level) {
        super(new ExtendViewport(480, 800));

        this.level = level;
        this.game = game;

        screenWidth = super.getViewport().getWorldWidth();
        screenHeight = super.getViewport().getWorldHeight();

        initSizes();
        initSkin();

        message = new TextButton("", skin);
        message.setVisible(false);
        message.getLabel().setWrap(true);
        message.getLabel().setAlignment(Align.left);
        message.padLeft(PAD_RIGHT_LEFT).padRight(PAD_RIGHT_LEFT).padTop(PAD_TOP_BOTTOM).padBottom(PAD_TOP_BOTTOM);
        addActor(message);
    }

    private void initSkin() {
        skin = new Skin();
        skin.add("text", game.getFont("message"));
        Pixmap pixmapText = new Pixmap(MSG_WIDTH, BUTTON_HEIGHT / 2, Pixmap.Format.RGB888);
        pixmapText.setColor(game.getColor("text background"));
        pixmapText.fill();
        skin.add("backgroundText", new Texture(pixmapText));
        TextButton.TextButtonStyle textButtonStyleText = new TextButton.TextButtonStyle();
        //textButtonStyleText.up = skin.newDrawable("backgroundText", game.getColor("text background"));
        textButtonStyleText.font = skin.getFont("text");
        textButtonStyleText.fontColor = game.getColor("text head");
        skin.add("default", textButtonStyleText);
    }

    /**
     * Show context menu.
     *
     * @param msg the msg
     */
    public void showMessage(MessageType msg) {
        isShow = true;
        timeStart = (float) level.getTimeRemaining();
        switch (msg) {
            case ERROR_MSG_BUSY_CORES:
                message.setText(" All cores are busy in this node.");
                break;
            case ERROR_MSG_NO_DATA:
                message.setText("Not enough data for executing program.");
                break;
            case ERROR_MSG_NEUTRAL_NODE:
                message.setText("You can't execute program on neutral node.");
                break;
            case ERROR_MSG_NO_RAM:
                message.setText("Not enough RAM for executing program.");
                break;
        }
        message.setPosition(screenHeight / 2 - message.getWidth(), screenHeight - message.getHeight() - BUTTON_HEIGHT);
        message.setVisible(true);
    }

    /**
     * Init sizes.
     */
    private void initSizes() {
        BUTTON_HEIGHT = (int) (screenHeight / 10);
        MSG_WIDTH = (int) (screenWidth / 4);
        PAD_RIGHT_LEFT = (int) (screenWidth / 100);
        PAD_TOP_BOTTOM = (int) (screenWidth / 100);
    }

    @Override
    public void draw() {
        if (isShow && timeStart - TIME_DELTA > level.getTimeRemaining()) {
            isShow = false;
            timeStart = 0.0f;
        }

        if (isShow) {
            super.draw();
        }
    }

    /**
     * The enum Message type.
     */
    public enum MessageType {

        ERROR_MSG_BUSY_CORES,
        ERROR_MSG_NO_DATA,
        ERROR_MSG_NEUTRAL_NODE,
        ERROR_MSG_NO_RAM
    }
}
