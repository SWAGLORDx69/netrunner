package com.netrunnergame.graphics;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

import java.util.List;

/**
 * The type of a scroll menu stage.
 *
 * @author Kirill Smirnov
 * @version 1.1
 * @see Stage
 */
public class ScrollMenuStage extends Stage {
    /**
     * size of a space in percent
     */
    private static final float TABLE_SPACE_PCT = 0.03f;
    /**
     * size of a button in percent
     */
    private static final float HEAD_HEIGHT_PCT = 0.15f;
    /**
     * size of a pad in percent
     */
    private static final float TABLE_PAD_PCT = 0.08f;
    /**
     * num of rows of the table with buttons
     */
    private static final int NUM_OF_ROW = 3;


    /**
     * Instantiates a new scroll menu.
     *
     * @param head      the headline of the menu
     * @param buttons   the array of a description of buttons which are containing in the menu
     * @param bottomBut the description of the bottom button
     * @param skin      the skin
     * @see Skin
     * @see ButtonSettings
     */
    public ScrollMenuStage(String head, List<ButtonSettings> buttons, ButtonSettings bottomBut, Skin skin) {
        super(new ExtendViewport(800, 480));

        Table mainTable = new Table();
        mainTable.setPosition(0,0);
        mainTable.setHeight(getViewport().getWorldHeight());
        mainTable.setWidth(getViewport().getWorldWidth());
        mainTable.center();
        mainTable.setSkin(skin);
        addActor(mainTable);

        Value headHeight = Value.percentHeight(HEAD_HEIGHT_PCT, mainTable);
        Value tablePad = Value.percentHeight(TABLE_PAD_PCT, mainTable);
        Value tableSpace = Value.percentHeight(TABLE_SPACE_PCT, mainTable);

        mainTable.add(head)
                .expandX()
                .height(headHeight)
                .fill()
                .getActor().setAlignment(Align.center);
        mainTable.row();

        Table buttonsTable = new Table(skin);

        ScrollPane scrollPane = new ScrollPane(buttonsTable);
        scrollPane.setScrollingDisabled(false, true);
        scrollPane.setForceScroll(true, false);
        mainTable.add(scrollPane)
                .grow();
        mainTable.row();

        mainTable.add(createButton(bottomBut.text, bottomBut.listener, skin))
                .width(Value.percentWidth(2f))
                .height(Value.percentHeight(0.1f, mainTable))
                .pad(Value.percentHeight(1f));

        buttonsTable.left();
        buttonsTable.padLeft(tablePad);
        buttonsTable.padRight(tablePad);

        int numOfBut = buttons.size();
        int lineSize = numOfBut / NUM_OF_ROW;
        lineSize = numOfBut % NUM_OF_ROW > 0 ? lineSize + 1 : lineSize;
        for (int j = 0; j < numOfBut; j++) {
            if (j != 0 && j % lineSize == 0) {
                buttonsTable.row();
            }
            ButtonSettings settings = buttons.get(j);
            Button button = createButton(settings.text, settings.listener, skin);
            buttonsTable.add(button)
                    .expandY()
                    .fill()
                    .uniform()
                    .space(tableSpace);
        }
    }

    /**
     * create a text button
     *
     * @param text     the text which will be wrote on the button
     * @param listener if is null then the button is disabled
     * @param skin     the skin
     * @return button
     * @see ClickListener
     * @see Button
     */
    private Button createButton(String text, ClickListener listener, Skin skin) {
        TextButton button = new TextButton(text, skin);
        if (listener != null) {
            button.addListener(listener);
        } else {
            button.setDisabled(true);
        }
        return button;
    }

    /**
     * The type of button settings.
     *
     * @author Kirill Smirnov
     * @version 1.0
     */
    public static class ButtonSettings {
        /**
         * The text
         */
        String text;
        /**
         * The Listener
         */
        ClickListener listener;

        /**
         * Instantiates a new button's settings.
         *
         * @param text     the text
         * @param listener the listener
         * @see ClickListener
         */
        public ButtonSettings(String text, ClickListener listener) {
            this.text = text;
            this.listener = listener;
        }
    }

}