package com.netrunnergame.graphics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.assets.loaders.SkinLoader;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.ObjectMap;
import com.netrunnergame.NetRunnerGame;
import com.netrunnergame.core.LevelLoader;

import java.util.LinkedList;
import java.util.List;


/**
 * The screen that displays menus
 *
 * @author Kirill Smirnov
 * @version 0.2
 * @see ScreenAdapter
 */
public class MenuScreen extends ScreenAdapter {
    protected static String SKIN_PATH = "data/skins/uiskin.json";
    protected final NetRunnerGame game;
    protected Stage currentStage;
    private Stage loadingStage;
    private Stage levelMenuStage;
    private Stage creditsStage;
    protected Stage mainMenuStage;
    private float delay = 0.5f;
    private boolean isLoading = true;

    /**
     * Instantiates a new Menu screen.
     *
     * @param game is the game
     */
    public MenuScreen(final NetRunnerGame game) {
        this.game = game;

        makeLoadingScreen();
        ObjectMap<String, Object> fontMap = new ObjectMap<String, Object>();
        fontMap.put("default-font", game.getFont("menu button"));
        fontMap.put("head-font", game.getFont("info"));
        fontMap.put("small-font", game.getFont("copyright"));

        setCurrentStage(loadingStage);
        game.assets.load(SKIN_PATH, Skin.class, new SkinLoader.SkinParameter(fontMap));

        Gdx.app.log("graphics", "scene of level has been created");
    }

    private void makeLoadingScreen() {
        loadingStage = new LoadingStage(game);
        Gdx.app.log("graphics", "Loading Screen has been created");
    }

    protected void setCurrentStage(Stage st) {
        currentStage = st;
        Gdx.input.setInputProcessor(currentStage);
    }

    /**
     * Make after loading.
     */
    void makeAfterLoading() {
        Skin skin = game.assets.get(SKIN_PATH);
        List<MenuStage.ButtonSettings> butons = new LinkedList<>();
        butons.add(new MenuStage.ButtonSettings("play", new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                setCurrentStage(levelMenuStage);
            }
        }));
        butons.add(new MenuStage.ButtonSettings("Credits", new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                setCurrentStage(creditsStage);
            }
        }));
        butons.add(new MenuStage.ButtonSettings("exit", new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.debug("button", "pressed button exit");
                Gdx.app.exit();
            }
        }));
        mainMenuStage = new MenuStage("Netrunner DEMO", butons, skin);
        List<ScrollMenuStage.ButtonSettings> levelsButton = new LinkedList<>();
        /*levelsButton.add(new ScrollMenuStage.ButtonSettings("test level", new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setCurrentLevelFile("data/levels/test.json");
                game.setScreen(new LevelScene(game, new LevelLoader(game, "data/levels/test.json").buildLevel()));
            }
        }));*/
        levelsButton.add(new ScrollMenuStage.ButtonSettings("level 1", new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setCurrentLevelFile("data/levels/level1.json");
                game.setScreen(new LevelScene(game, new LevelLoader(game, "data/levels/level1.json").buildLevel()));
            }
        }));
        levelsButton.add(new ScrollMenuStage.ButtonSettings("level 2", new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setCurrentLevelFile("data/levels/level2.json");
                game.setScreen(new LevelScene(game, new LevelLoader(game, "data/levels/level2.json").buildLevel()));
            }
        }));
        levelsButton.add(new ScrollMenuStage.ButtonSettings("level 3", new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setCurrentLevelFile("data/levels/level3.json");
                game.setScreen(new LevelScene(game, new LevelLoader(game, "data/levels/level3.json").buildLevel()));
            }
        }));
        levelsButton.add(new ScrollMenuStage.ButtonSettings("level 4", new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setCurrentLevelFile("data/levels/level4.json");
                game.setScreen(new LevelScene(game, new LevelLoader(game, "data/levels/level4.json").buildLevel()));
            }
        }));
        levelsButton.add(new ScrollMenuStage.ButtonSettings("level 5", new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setCurrentLevelFile("data/levels/level5.json");
                game.setScreen(new LevelScene(game, new LevelLoader(game, "data/levels/level5.json").buildLevel()));
            }
        }));
        levelsButton.add(new ScrollMenuStage.ButtonSettings("level 6", new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setCurrentLevelFile("data/levels/level6.json");
                game.setScreen(new LevelScene(game, new LevelLoader(game, "data/levels/level6.json").buildLevel()));
            }
        }));
        levelsButton.add(new ScrollMenuStage.ButtonSettings("level 7", new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setCurrentLevelFile("data/levels/level7.json");
                game.setScreen(new LevelScene(game, new LevelLoader(game, "data/levels/level7.json").buildLevel()));
            }
        }));
        levelsButton.add(new ScrollMenuStage.ButtonSettings("level 8", new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setCurrentLevelFile("data/levels/level8.json");
                game.setScreen(new LevelScene(game, new LevelLoader(game, "data/levels/level8.json").buildLevel()));
            }
        }));
        ScrollMenuStage.ButtonSettings bottomBut =
                new ScrollMenuStage.ButtonSettings("Back", new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        setCurrentStage(mainMenuStage);
                    }
                });
        levelMenuStage = new ScrollMenuStage("Levels", levelsButton, bottomBut, skin);
        creditsStage = new CreditsStage("Credits", bottomBut, skin);
        setCurrentStage(mainMenuStage);
        currentStage.getViewport().update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1);
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT |
                (Gdx.graphics.getBufferFormat().coverageSampling ? GL20.GL_COVERAGE_BUFFER_BIT_NV : 0));

        if (isLoading || delay > 0) {
            delay -= delta;
            if (isLoading && game.assets.update()) {
                isLoading = false;
                makeAfterLoading();
                Gdx.app.log("graphics", "loading is complete");
            }
        }

        currentStage.act(delta);
        currentStage.draw();

    }

    @Override
    public void dispose() {
        loadingStage.dispose();
        mainMenuStage.dispose();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        currentStage.getViewport().update(width, height, false);
    }
}