package com.netrunnergame.graphics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.math.collision.Ray;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.google.common.collect.ImmutableMap;
import com.netrunnergame.NetRunnerGame;
import com.netrunnergame.core.Level;
import com.netrunnergame.core.LevelController;
import com.netrunnergame.core.NodeView;
import com.netrunnergame.core.programs.GameProgramWithArg;

import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;


/**
 * The type Level scene.
 */
public class LevelScene implements Screen, Observer {

    private final NetRunnerGame game;
    private final LevelController level;
    private int markedNodeId = -1;
    //private final ModelBatch modelBatch = new ModelBatch();
    private GMIManager instancesContainer;
    private Stage stage;
    //private Environment environment;
    private PerspectiveCamera cam;
    private ImprovedCameraInputController camController;
    private Label label;
    private Stage loadingStage;
    private float delay = 0;
    private boolean isLoading = true;
    private ContextMenu contextMenu;
    private ProgramTooltip programTooltip;
    private FileTooltip fileTooltip;
    private LogPanel logPanel;
    private StartPauseButton startPauseButton;
    private GameMenu gameMenu;
    private ProgramPanel programPanel;
    private ResetArgsButton resetArgsButton;
    private GameProgramWithArg waitingForArgProgram;
    private PlayerInfo playerInfo;
    private ErrorMsgForPlayer errorMsgForPlayer;
    private Help help;

    /**
     * Instantiates a new Level scene.
     *
     * @param game the main class
     * @param lvl  is the object that represents the logic part this game
     */
    public LevelScene(final NetRunnerGame game, final Level lvl) {
        this.game = game;
        this.level = lvl.player();
        this.level.addObserver(this);
        makeLoadingScreen();

        Iterator<String> itTypes = level.getNodeTypes();
        while (itTypes.hasNext()) {
            String type = itTypes.next();
            game.assets.load("data/models/" + type + "model.obj", Model.class);
        }

        game.assets.load("data/sprites/mark.txt", TextureAtlas.class);
        game.assets.load("data/sprites/programicon.txt", TextureAtlas.class);
        game.assets.load("data/sprites/programiconnotavail.txt", TextureAtlas.class);
        
        makeCam();
        makeUI();

        lvl.run();

        Gdx.app.log("graphics", "scene of level has been created");
    }

    private void makeLoadingScreen() {
        loadingStage = new LoadingStage(game);
        Gdx.app.log("graphics", "Loading Screen has been created");
    }

    /*
        private void makeEnvironment() {
            environment = new Environment();
            environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
            environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));
            environment.add(new DirectionalLight().set(0.5f, 0.5f, 0.5f, 1f, 1.2f, 0.5f));
            Gdx.app.log("graphics", "environment has been created");
        }
    */
    private void makeModels() {
        ModelBuilder modelBuilder = new ModelBuilder();
        ImmutableMap.Builder<String, Model> mapBuilder = ImmutableMap.builder();
        Iterator<String> itTypes = level.getNodeTypes();
        while (itTypes.hasNext()) {
            String type = itTypes.next();
            mapBuilder.put(type, game.assets.get("data/models/" + type + "model.obj", Model.class));
        }

        modelBuilder.begin();
        MeshPartBuilder builder = modelBuilder.part("line", 1, 3, new Material());
        builder.setColor(Color.WHITE);
        builder.line(0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f);
        Model linkModel = modelBuilder.end();
        GMFactory factory = new GMFactory(mapBuilder.build(), linkModel, level.getName(),
                game.getColor("node player"), game.getColor("node neutral"), game.getColor("node candidate"));
        instancesContainer = new GMIManager(factory, null);
        Gdx.app.log("graphics", "instances container has been created");

    }

    private void makeCam() {
        cam = new PerspectiveCamera(50, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        cam.position.set(-0.084269725f, -0.57546264f, 17.310768f);
        cam.lookAt(0, 0, 0);
        cam.near = 1f;
        cam.far = 300f;
        cam.update();
        Gdx.app.log("graphics", "camera has been created");
    }

    private void makeUI() {
        help = new Help(game, level);
        logPanel = new LogPanel(game);
        level.addObserver(logPanel);
        startPauseButton = new StartPauseButton(game, level);
        gameMenu = new GameMenu(game, level, startPauseButton);
        playerInfo = new PlayerInfo(game, level);
        errorMsgForPlayer = new ErrorMsgForPlayer(game, level);
        label = new Label(" ", new Label.LabelStyle(game.getFont("text"), Color.WHITE));
        stage = new Stage();
        stage.addActor(label);
        programTooltip = new ProgramTooltip(game);
        fileTooltip = new FileTooltip(game);
        resetArgsButton = new ResetArgsButton(game, level, this, contextMenu);
        programPanel = new ProgramPanel(level, this, game, programTooltip,
                fileTooltip, instancesContainer, resetArgsButton, errorMsgForPlayer);
        contextMenu = new ContextMenu(cam, level, game, programTooltip, fileTooltip, programPanel, logPanel);
        Gdx.app.log("graphics", "UI has been created");
    }

    private void makeInstances() {
        try {
            GMIManager.Builder bld = instancesContainer.builder();
            for (NodeView n : level) {
                bld.newNode(n.getId(), n.getType(), n.getPosition(),
                        (n.getOwner() != null ? n.getOwner().getName() : null));
                Iterator<NodeView> itt = n.getLinkedNodes();
                while (itt.hasNext()) {
                    bld.linkFromTo(n.getId(), itt.next().getId());
                }
            }
            bld.build();
        } catch (Exception e) {
            Gdx.app.error("graphics", "instances container exception", e);
        }
        level.addObserver(instancesContainer);
        Gdx.app.log("graphics", "instances container has been filled");
    }

    private void makeController() {
        camController = new ImprovedCameraInputController(cam);
        InputMultiplexer multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(gameMenu);
        multiplexer.addProcessor(help);
        multiplexer.addProcessor(programPanel);
        multiplexer.addProcessor(programTooltip);
        multiplexer.addProcessor(fileTooltip);
        multiplexer.addProcessor(startPauseButton);
        multiplexer.addProcessor(resetArgsButton);
        multiplexer.addProcessor(contextMenu);
        multiplexer.addProcessor(logPanel);
        multiplexer.addProcessor(new GraphInputProcessor());
        multiplexer.addProcessor(camController);
        Gdx.input.setInputProcessor(multiplexer);
        gameMenu.saveGameSceneInputProcessor(multiplexer);
        help.saveGameSceneInputProcessor(multiplexer);
    }

    /**
     * Mark clean.
     */
    public void markClean() {
        markedNodeId = -1;
        contextMenu.hide();
        logPanel.hide();
    }

    /**
     * Is mark is visible.
     */
    private boolean isMarkVisible() {
        return contextMenu.isVisible();
    }

    /**
     * Mark node.
     *
     * @param id the id
     */
    public void markNode(int id) {
        if (markedNodeId >= 0) {
            markClean();
        }
        markedNodeId = id;
        contextMenu.setCurNode(id);
        programPanel.setCurNode(id);
        camController.setCamCenter(instancesContainer.getNode(id).getPos());
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1);
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT |
                (Gdx.graphics.getBufferFormat().coverageSampling ? GL20.GL_COVERAGE_BUFFER_BIT_NV : 0));

        if (isLoading || delay > 0) {
            loadingStage.act(delta);
            loadingStage.draw();
            if (!isLoading) {
                delay -= delta;
            }
            if (isLoading && game.assets.update()) {
                makeModels();
                makeInstances();
                makeController();
                programPanel.initResources(instancesContainer);
                contextMenu.initResources(instancesContainer);
                resetArgsButton.initInstContainer(instancesContainer);
                isLoading = false;
                level.addObserver(instancesContainer);
                level.start();
                Gdx.app.log("graphics", "loading is complete");
            }
        } else {
            camController.update(delta);

            instancesContainer.act(delta);
            instancesContainer.draw(cam);

            label.setText(" FPS: " + Gdx.graphics.getFramesPerSecond());
            stage.draw();

            contextMenu.act(delta);
            contextMenu.draw();

            programPanel.act(delta);
            programPanel.draw();

            logPanel.act(delta);
            logPanel.draw();

            startPauseButton.act(delta);
            startPauseButton.draw();

            resetArgsButton.act(delta);
            resetArgsButton.draw();

            playerInfo.act(delta);
            playerInfo.draw();

            errorMsgForPlayer.act(delta);
            errorMsgForPlayer.draw();

            programTooltip.act(delta);
            programTooltip.draw();

            fileTooltip.act(delta);
            fileTooltip.draw();

            help.act(delta);
            help.draw();

            gameMenu.act(delta);
            gameMenu.draw();
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        LevelController player = (LevelController) o;
        if (arg == null) {
            String winnerName = player.getWinnerName();
            if (winnerName != null) {
                Gdx.app.log("level", winnerName + " win");
                if (winnerName.equals(player.getName())) {
                    game.setScreen(new VictoryScreen(game));
                } else {
                    game.setScreen(new DefeatScreen(game));
                }
            }
        }
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void show() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    public GameProgramWithArg getWaitingProgram() {
        return waitingForArgProgram;
    }

    public void setWaitingProgram(GameProgramWithArg programFactory) {
        waitingForArgProgram = programFactory;
    }

    /**
     * Gesture listener for multitouch zooming
     */
    class GraphGestureListener extends GestureDetector.GestureAdapter {

        @Override
        public boolean tap(float x, float y, int count, int button) {
            LevelScene.this.markClean();
            return false;
        }
    }

    /**
     * Graph node click/touch input controller
     *
     * @author Pozigun Mikhail
     * @version 0.2
     * @see InputAdapter
     */
    public class GraphInputProcessor extends GestureDetector {
        /**
         * id of selecting node(not yet selected)
         */
        private int selectingNodeId = -1;
        /**
         * id of current selected node
         */
        private int selectedNodeId = -1;

        public GraphInputProcessor() {
            this(new GraphGestureListener());
        }

        public GraphInputProcessor(GraphGestureListener gestureListener) {
            super(gestureListener);
        }

        /**
         * Getting id of current selected node by node's position on screen
         *
         * @param screenX screen position x
         * @param screenY screen position y
         * @return id of selected node
         */
        private int getCurNodeSelectedId(int screenX, int screenY) {
            Ray ray = cam.getPickRay(screenX, screenY);
            Vector3 position = new Vector3();
            position.set(cam.position);
            int minDistNodeId = -1;
            float minDist = -1;
            Iterator<GMIManager.Node> it = instancesContainer.nodeIterator();
            while (it.hasNext()) {
                GMIManager.Node curNode = it.next();
                position.set(curNode.getPos());
                BoundingBox bounds = curNode.getBoundingBox();
                Vector3 center = new Vector3();
                Vector3 dimensions = new Vector3();
                bounds.getCenter(center);
                bounds.getDimensions(dimensions);
                position.add(center);
                float curDist = ray.origin.dst2(position);
                if (minDist == -1 || curDist < minDist) {
                    if (Intersector.intersectRaySphere(ray, position, dimensions.len() / 2, null)) {
                        minDist = curDist;
                        minDistNodeId = curNode.id;
                    }
                }
            }
            return minDistNodeId;
        }

        /**
         * Updating nodes: old and new selected(for now it's only marking models of nodes)
         *
         * @param newSelectedNodeId id of new selected node
         */
        private void updateSelectedNode(int newSelectedNodeId) {
            if (waitingForArgProgram != null && !instancesContainer.getNode(newSelectedNodeId).isMarked()) {
                return;
            }
            if (waitingForArgProgram != null && instancesContainer.getNode(newSelectedNodeId).isMarked()) {
                int nodeWithWaitingProgId = waitingForArgProgram.getHost().getId();
                waitingForArgProgram.setArg(level.getNodeView(newSelectedNodeId));
                instancesContainer.unmarkAll();
                resetArgsButton.setResetButtonVisible(false);
                level.runProgram(waitingForArgProgram, nodeWithWaitingProgId);
                waitingForArgProgram = null;
                return;
            }
            if (!LevelScene.this.isMarkVisible()) {
                selectingNodeId = -1;
                selectedNodeId = -1;
            }
            if (newSelectedNodeId == selectedNodeId) {
                //Selected same as current node case
                LevelScene.this.markClean();
                selectingNodeId = -1;
                selectedNodeId = -1;
                return;
            }
            if (newSelectedNodeId != -1) {
                //Some node was selected, should make it "selected"
                LevelScene.this.markClean();
                LevelScene.this.markNode(newSelectedNodeId);
            }
            selectedNodeId = newSelectedNodeId;
        }

        @Override
        public boolean touchDown(int screenX, int screenY, int pointer, int button) {
            selectingNodeId = getCurNodeSelectedId(screenX, screenY);
            return selectingNodeId == -1 && super.touchDown(screenX, screenY, pointer, button);
        }

        @Override
        public boolean touchDragged(int screenX, int screenY, int pointer) {
            return false;
        }

        @Override
        public boolean touchUp(int screenX, int screenY, int pointer, int button) {
            if (selectingNodeId != -1) {
                if (selectingNodeId == getCurNodeSelectedId(screenX, screenY)) {
                    updateSelectedNode(selectingNodeId);
                    selectingNodeId = -1;
                }
                return false;
            }
            return super.touchUp(screenX, screenY, pointer, button);
        }
    }

}
