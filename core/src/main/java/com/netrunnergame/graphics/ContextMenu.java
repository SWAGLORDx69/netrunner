package com.netrunnergame.graphics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.netrunnergame.NetRunnerGame;
import com.netrunnergame.core.File;
import com.netrunnergame.core.LevelController;
import com.netrunnergame.core.NodeView;
import com.netrunnergame.core.Program;

import java.util.*;

/**
 * Context menu of chosen node class.
 *
 * @author Pozigun Mikhail, Kirill Smirnov
 * @version 0.6
 * @see Stage
 */
public class ContextMenu extends Stage implements Observer {
    /**
     * The constant height for files buttons in context menu table.
     */
    private static int TABLE_ROW_FILES_HEIGHT;
    /**
     * The constant padding for files buttons in context menu table (top and bottom).
     */
    private static int PAD_FILES_TOP_BOTTOM;
    /**
     * The constant width of every row in context menu table.
     */
    private static int TABLE_ROW_WIDTH;
    /**
     * The constant padding for id,characteristics (left and right).
     */
    private static int PAD_RIGHT_LEFT;
    /**
     * The constant padding for id (top and bottom).
     */
    private static int PAD_TOP_BOTTOM_ID;
    /**
     * The constant padding for characteristics section (top).
     */
    private static int PAD_TOP_CHAR;
    /**
     * The constant padding for characteristics section (bottom).
     */
    private static int PAD_BOTTOM_CHAR;
    /**
     * The table with info about node.
     */
    private Table nodeInfoTable;
    /**
     * The table with info about files.
     */
    private Table filesTable;
    /**
     * The table with info about programs.
     */
    private Table programsTable;
    /**
     * Id of current chosen node.
     */
    private int curNodeId = -1;
    /**
     * The model instances container.
     */
    private GMIManager instancesContainer;
    /**
     * The camera.
     */
    private PerspectiveCamera camera;
    /**
     * The level (here for getting info about chosen node).
     */
    private LevelController level;
    /**
     * The game (here for getting fonts).
     */
    private NetRunnerGame game;
    /**
     * The skin of all buttons.
     */
    private Skin skin = new Skin();
    /**
     * The node Id.
     */
    private TextButton nodeIdLabel;
    /**
     * The label with clock speed.
     */
    private TextButton speed;
    /**
     * The label with number of cores.
     */
    private TextButton cores;
    /**
     * The label with volume of hdd.
     */
    private TextButton hdd;
    /**
     * The label with volume of ram.
     */
    private TextButton ram;
    /**
     * The viewport screen height.
     */
    private float screenHeight;
    /**
     * The viewport screen width.
     */
    private float screenWidth;
    /**
     * The accentuation image.
     */
    private AnimatedImage accentuationImg;
    /**
     * Is show context menu.
     */
    private boolean isShow = false;
    /**
     * Program tooltip.
     */
    private ProgramTooltip programTooltip;
    /**
     * File tooltip.
     */
    private FileTooltip fileTooltip;
    /**
     * The current icon of program for that tooltip is shown.
     */
    private ImageButton curClickedImg;
    /**
     * The current program for that tooltip is shown.
     */
    private Program curProgram;
    /**
     * Current file button that tooltip is shown.
     */
    private TextButton curFileButton;
    /**
     * Current file  that tooltip is shown.
     */
    private File curFile;
    /**
     * The count of programs running on current node.
     */
    private int curProgRunCount;
    /**
     * The program panel.
     */
    private ProgramPanel programPanel;
    /**
     * The log panel.
     */
    private LogPanel logPanel;

    /**
     * Instantiates a new Context menu.
     *
     * @param camera         the camera
     * @param level          the level
     * @param game           the game
     * @param programTooltip the program tooltip
     * @param fileTooltip    the file tooltip
     * @param programPanel   the program panel
     * @param logPanel       the log panel
     */
    public ContextMenu(PerspectiveCamera camera,
                       LevelController level,
                       NetRunnerGame game,
                       ProgramTooltip programTooltip,
                       FileTooltip fileTooltip,
                       ProgramPanel programPanel,
                       LogPanel logPanel) {
        super(new ExtendViewport(480, 800));
        screenWidth = super.getViewport().getWorldWidth();
        screenHeight = super.getViewport().getWorldHeight();
        initSizes();

        this.camera = camera;
        this.level = level;
        this.game = game;
        this.programTooltip = programTooltip;
        this.fileTooltip = fileTooltip;
        this.programPanel = programPanel;
        this.logPanel = logPanel;

        nodeInfoTable = new Table();
        programsTable = new Table();
        filesTable = new Table();
        addActor(nodeInfoTable);
        addActor(programsTable);
        addActor(filesTable);
        initSkin();
        //fill table
        nodeIdLabel = new TextButton("", skin, "head");
        speed = new TextButton("", skin, "characteristics");
        cores = new TextButton("", skin, "characteristics");
        hdd = new TextButton("", skin, "characteristics");
        ram = new TextButton("", skin, "characteristics");
        nodeIdLabel.getLabel().setAlignment(Align.left);
        speed.getLabel().setAlignment(Align.left);
        cores.getLabel().setAlignment(Align.left);
        hdd.getLabel().setAlignment(Align.left);
        ram.getLabel().setAlignment(Align.left);

        nodeIdLabel.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                hide();
            }
        });
        speed.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                hide();
            }
        });
        cores.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                hide();
            }
        });
        hdd.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                hide();
            }
        });
        ram.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                hide();
            }
        });
        nodeIdLabel.padLeft(PAD_RIGHT_LEFT).padRight(PAD_RIGHT_LEFT).padTop(PAD_TOP_BOTTOM_ID).padBottom(PAD_TOP_BOTTOM_ID);
        speed.padLeft(PAD_RIGHT_LEFT).padRight(PAD_RIGHT_LEFT).padTop(PAD_TOP_CHAR);
        cores.padLeft(PAD_RIGHT_LEFT).padRight(PAD_RIGHT_LEFT);
        hdd.padLeft(PAD_RIGHT_LEFT).padRight(PAD_RIGHT_LEFT);
        ram.padLeft(PAD_RIGHT_LEFT).padRight(PAD_RIGHT_LEFT).padBottom(PAD_BOTTOM_CHAR);
        hide();

        level.addObserver(this);
    }

    /**
     * Init resources.
     *
     * @param instancesContainer the instances container
     */
    public void initResources(GMIManager instancesContainer) {
        this.instancesContainer = instancesContainer;

        TextureAtlas t = game.assets.get("data/sprites/mark.txt");
        Animation<TextureRegion> loadingAnimation =
                new Animation<TextureRegion>(0.05f, t.getRegions(), Animation.PlayMode.LOOP);
        accentuationImg = new AnimatedImage(loadingAnimation);
        addActor(accentuationImg);
    }

    private void initSizes() {
        TABLE_ROW_WIDTH = (int) (screenWidth / 8);
        TABLE_ROW_FILES_HEIGHT = TABLE_ROW_WIDTH / 3;
        PAD_FILES_TOP_BOTTOM = TABLE_ROW_WIDTH / 11;
        PAD_RIGHT_LEFT = TABLE_ROW_WIDTH / 12;
        PAD_TOP_BOTTOM_ID = TABLE_ROW_WIDTH / 12;
        PAD_TOP_CHAR = TABLE_ROW_WIDTH / 12;
        PAD_BOTTOM_CHAR = TABLE_ROW_WIDTH / 11;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof NodeView) {
            NodeView node = (NodeView) arg;
            if (node.getId() == curNodeId) {
                if (node.getStatus() != 0.0f && node.getStatus() != 1.0f) {
                    return;
                }
                setCurNode(curNodeId);
            }
        }
    }

    /**
     * Init skin.
     */
    private void initSkin() {
        skin.add("default", game.getFont("text"));
        skin.add("head", game.getFont("button"));
        Pixmap pixmap = new Pixmap(TABLE_ROW_WIDTH, PAD_TOP_BOTTOM_ID, Pixmap.Format.RGB888);
        pixmap.setColor(Color.WHITE);
        pixmap.fill();
        skin.add("background", new Texture(pixmap));

        Pixmap pixmapPrograms = new Pixmap(TABLE_ROW_WIDTH / 2, TABLE_ROW_WIDTH / 2, Pixmap.Format.RGB888);
        pixmapPrograms.setColor(Color.WHITE);
        pixmapPrograms.fill();
        skin.add("backgroundPrograms", new Texture(pixmapPrograms));

        TextButton.TextButtonStyle textButtonStyleChar = new TextButton.TextButtonStyle();
        textButtonStyleChar.up = skin.newDrawable("background", game.getColor("text background"));
        textButtonStyleChar.font = skin.getFont("default");
        textButtonStyleChar.fontColor = game.getColor("text");
        skin.add("characteristics", textButtonStyleChar);

        TextButton.TextButtonStyle textButtonStyleFiles = new TextButton.TextButtonStyle();
        textButtonStyleFiles.up = skin.newDrawable("background", game.getColor("files background"));
        textButtonStyleFiles.font = skin.getFont("default");
        textButtonStyleFiles.fontColor = game.getColor("text head");
        skin.add("files", textButtonStyleFiles);

        TextButton.TextButtonStyle textButtonStyleHead = new TextButton.TextButtonStyle();
        textButtonStyleHead.up = skin.newDrawable("background", game.getColor("head background"));
        textButtonStyleHead.font = skin.getFont("head");
        textButtonStyleHead.fontColor = game.getColor("text head");
        skin.add("head", textButtonStyleHead);
    }

    /**
     * Is context menu is visible.
     *
     * @return the boolean
     */
    public boolean isVisible() {
        return isShow;
    }

    /**
     * Hide context menu.
     */
    public void hide() {
        if (!programTooltip.getOwner().equals(ProgramTooltip.Owner.OWNER_NO_OWNER) ||
                fileTooltip.isVisible() ||
                logPanel.isVisible()) {
            logPanel.hide();
            programTooltip.hide();
            fileTooltip.hide();
            return;
        }
        isShow = false;
        nodeInfoTable.setVisible(false);
        filesTable.setVisible(false);
        programsTable.setVisible(false);
        programPanel.hide();
    }

    /**
     * Show context menu.
     */
    public void show() {
        isShow = true;
        nodeInfoTable.setVisible(true);
        filesTable.setVisible(true);
        programsTable.setVisible(true);
    }

    /**
     * Sets cur node.
     *
     * @param nodeId the node id
     */
    public void setCurNode(int nodeId) {
        Gdx.app.log("setCurNode", "Set context menu for node with id " + nodeId);

        nodeInfoTable.clear();
        filesTable.clear();
        programsTable.clear();

        if (programTooltip.getOwner().equals(ProgramTooltip.Owner.OWNER_CONTEXT_MENU)) {
            programTooltip.hide();
        }

        fileTooltip.hide();

        curNodeId = nodeId;
        NodeView node = level.getNodeView(nodeId);

        //node info section
        nodeIdLabel.setText("Id: " + nodeId);
        speed.setText("Speed: " + node.getClockSpeed());
        cores.setText("Cores: " + node.getCoreNumber());
        hdd.setText("HDD:   " + node.getHDD());
        ram.setText("RAM:   " + node.getRAM());
        nodeInfoTable.add(nodeIdLabel).width(TABLE_ROW_WIDTH).row();
        nodeInfoTable.add(speed).width(TABLE_ROW_WIDTH).row();
        nodeInfoTable.add(cores).width(TABLE_ROW_WIDTH).row();
        nodeInfoTable.add(hdd).width(TABLE_ROW_WIDTH).row();
        nodeInfoTable.add(ram).width(TABLE_ROW_WIDTH).row();

        //files section
        List<File> files = node.getFiles();

        if (files.size() > 0) {
            TextButton filesTitle = new TextButton("Files:", skin, "head");
            filesTitle.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    hide();
                }
            });
            filesTitle.getLabel().setAlignment(Align.left);
            filesTitle.padLeft(PAD_RIGHT_LEFT).padRight(PAD_RIGHT_LEFT).padTop(PAD_TOP_BOTTOM_ID).padBottom(PAD_TOP_BOTTOM_ID);
            filesTable.add(filesTitle).width(TABLE_ROW_WIDTH).row();
        }

        for (final File file : files) {
            final TextButton fileButton = new TextButton(file.getName(), skin, "files");
            fileButton.getLabel().setAlignment(Align.left);
            fileButton.getLabel().setWrap(true);
            fileButton.padLeft(PAD_RIGHT_LEFT).padRight(PAD_RIGHT_LEFT).padTop(PAD_TOP_CHAR);
            fileButton.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    if (fileButton.equals(curFileButton) && fileTooltip.isVisible()) {
                        fileTooltip.hide();
                        return;
                    }
                    curFileButton = fileButton;
                    curFile = file;
                    fileTooltip.show();
                    programTooltip.hide();
                }
            });
            filesTable.add(fileButton.padBottom(PAD_FILES_TOP_BOTTOM)
                    .padTop(PAD_FILES_TOP_BOTTOM)).width(TABLE_ROW_WIDTH).height(TABLE_ROW_FILES_HEIGHT).row();
        }

        //programs section
        boolean isHasPrograms = false;
        for (int i = 0; i < level.getNodeView(nodeId).getCoreNumber(); i++) {
            if (level.getNodeView(nodeId).getProgram(i) != null) {
                isHasPrograms = true;
                break;
            }
        }
        if (isHasPrograms) {

            TextButton programsTitle = new TextButton("Running:", skin, "head");
            programsTitle.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    hide();
                }
            });
            programsTitle.getLabel().setAlignment(Align.left);
            programsTitle.padLeft(PAD_RIGHT_LEFT).padRight(PAD_RIGHT_LEFT).padTop(PAD_TOP_BOTTOM_ID).padBottom(PAD_TOP_BOTTOM_ID);
            programsTable.add(programsTitle).width(TABLE_ROW_WIDTH).row();

            final Table programIconsTable = new Table();
            programsTable.add(programIconsTable);

            final HashMap<ImageButton, Integer> cores = new HashMap<>();
            int curColIdx = 0;
            TextureAtlas programIconAtlas = game.assets.get("data/sprites/programicon.txt");
            for (int i = 0; i < level.getNodeView(nodeId).getCoreNumber(); i++) {
                if (level.getNodeView(nodeId).getProgram(i) != null) {
                    final Program program = level.getNodeView(nodeId).getProgram(i);
                    TextureRegion programIconRegion = programIconAtlas.findRegion(program.getName());
                    TextureRegionDrawable programIconRegDrawable = new TextureRegionDrawable(programIconRegion);
                    final ImageButton programImg = new ImageButton(programIconRegDrawable);

                    cores.put(programImg, i);

                    programImg.setBackground(programIconRegDrawable);
                    programImg.addListener(new ActorGestureListener(20, 0.4f, 0.6f, 0.15f) {
                        @Override
                        public void tap(InputEvent event, float x, float y, int count, int button) {
                            if (programTooltip.getOwner().equals(ProgramTooltip.Owner.OWNER_CONTEXT_MENU) && curClickedImg.equals(programImg)) {
                                programTooltip.hide();
                                return;
                            }
                            curClickedImg = programImg;
                            curProgram = program;
                            programTooltip.setOwner(ProgramTooltip.Owner.OWNER_CONTEXT_MENU);
                            fileTooltip.hide();
                        }

                        @Override
                        public boolean longPress(Actor actor, float x, float y) {
                            if (level.getNodeView(curNodeId).getOwner().getName().equals("")) {
                                return false;
                            }
                            if (programTooltip.getOwner().equals(ProgramTooltip.Owner.OWNER_CONTEXT_MENU) && curClickedImg.equals(programImg)) {
                                programTooltip.hide();
                            }
                            instancesContainer.getNode(curNodeId).decRotationAngle();
                            programIconsTable.removeActor(programImg);
                            level.getNodeView(curNodeId).getOwner().stopProgram(curNodeId, cores.get(programImg));
                            //cores.remove(programImg);
                            //setCurNode(curNodeId);
                            return false;
                        }
                    });
                    if (curColIdx == 0) {
                        programIconsTable.add(programImg).width(TABLE_ROW_WIDTH / 2).height(TABLE_ROW_WIDTH / 2).left();
                    } else {
                        programIconsTable.add(programImg).width(TABLE_ROW_WIDTH / 2).height(TABLE_ROW_WIDTH / 2).left().row();
                    }
                    curColIdx++;
                    curColIdx %= 2;
                }
            }

            curProgRunCount = cores.size();
        }

        show();

    }

    @Override
    public void draw() {
        if (isShow) {
            if (updatePos()) {
                super.draw();
            }
        }
    }


    private float projectSphere(Vector3 pos, float rad) {
        Vector3 cam = camera.position;
        Vector3 dir = camera.direction;
        float x = (pos.x - cam.x);
        float y = (pos.y - cam.y);
        float z = (pos.z - cam.z);
        if (x * dir.x + y * dir.y + z * dir.z <= 0.0) {
            return 0;
        }
        float f = (x * dir.x + y * dir.y + z * dir.z) / (x * x + y * y + z * z);
        x = dir.x - f * x;
        y = dir.y - f * y;
        z = dir.z - f * z;
        float l = (float) Math.sqrt(x * x + y * y + z * z);
        Vector3 vec = new Vector3(x / l * rad + pos.x, y / l * rad + pos.y, z / l * rad + pos.z);
        camera.project(vec, super.getViewport().getScreenX(),
                super.getViewport().getScreenY(),
                super.getViewport().getWorldWidth(),
                super.getViewport().getWorldHeight());
        camera.project(pos, super.getViewport().getScreenX(),
                super.getViewport().getScreenY(),
                super.getViewport().getWorldWidth(),
                super.getViewport().getWorldHeight());
        x = vec.x - pos.x;
        y = vec.y - pos.y;
        return (float) Math.sqrt(x * x + y * y);
    }

    /**
     * Update drawing positions of context menu and accentuation of node.
     */
    private boolean updatePos() {
        nodeInfoTable.pack();
        filesTable.pack();
        programsTable.pack();

        GMIManager.Node node = instancesContainer.getNode(curNodeId);
        Vector3 pos = new Vector3(node.getPos());

        float projRad = projectSphere(pos, node.getModelInstRad());
        float imageHeight = accentuationImg.getHeight();
        if (projRad > 0) {
            accentuationImg.setScale(2 * projRad / imageHeight);
            accentuationImg.setX(pos.x - projRad);
            accentuationImg.setY(pos.y - projRad);
            //update positions of tables

            nodeInfoTable.setPosition(accentuationImg.getX() + 2 * projRad,
                    accentuationImg.getY() - nodeInfoTable.getHeight() + 2 * projRad);

            filesTable.setPosition(accentuationImg.getX() - filesTable.getWidth(),
                    accentuationImg.getY() - filesTable.getHeight() + 2 * projRad);

            programsTable.setPosition(accentuationImg.getX() + 2 * projRad,
                    accentuationImg.getY() - nodeInfoTable.getHeight() - programsTable.getHeight() + 2 * projRad);
            //update tooltips

            if (programTooltip.getOwner().equals(ProgramTooltip.Owner.OWNER_CONTEXT_MENU)) {
                if (curProgRunCount == 1) {
                    programTooltip.setProgram(new Vector2(programsTable.getX() + curClickedImg.getX() + TABLE_ROW_WIDTH / 2,
                            programsTable.getY() + curClickedImg.getY() + TABLE_ROW_WIDTH / 4), curProgram);
                } else {
                    programTooltip.setProgram(new Vector2(programsTable.getX() + curClickedImg.getX() + TABLE_ROW_WIDTH / 4,
                            programsTable.getY() + curClickedImg.getY() + TABLE_ROW_WIDTH / 4), curProgram);
                }
            }

            if (fileTooltip.isVisible()) {
                fileTooltip.setFile(new Vector2(filesTable.getX() + curFileButton.getX() + TABLE_ROW_WIDTH / 2,
                        filesTable.getY() + curFileButton.getY() + TABLE_ROW_FILES_HEIGHT / 2), curFile);
            }

            return true;
        } else {
            return false;
        }
    }
}
