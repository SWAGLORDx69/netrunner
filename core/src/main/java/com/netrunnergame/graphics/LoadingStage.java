package com.netrunnergame.graphics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.netrunnergame.NetRunnerGame;

public class LoadingStage extends Stage {
    private NetRunnerGame game;
    private VerticalGroup verticalGroup = new VerticalGroup();
    private AnimatedImage image;
    private Label loadingText;

    public LoadingStage(NetRunnerGame gm) {
        this.game = gm;

        if (!game.assets.isLoaded("data/sprites/loading.txt")) {
            game.assets.load("data/sprites/loading.txt", TextureAtlas.class);
            long time = System.nanoTime();
            game.assets.finishLoadingAsset("data/sprites/loading.txt");
            Gdx.app.log("graphics", "time of loading start animation: " +
                    (System.nanoTime() - time) / 1000000000.0 + " sec");
        }

        TextureAtlas t = game.assets.get("data/sprites/loading.txt");
        Animation<TextureRegion> loadingAnimation =
                new Animation<TextureRegion>(0.05f, t.getRegions(), Animation.PlayMode.LOOP);
        image = new AnimatedImage(loadingAnimation);

        loadingText = new Label("Test", new Label.LabelStyle(game.getFont("head"), Color.WHITE)) {
            @Override
            public void act(float delta) {
                setText("Loading: " + (int) (game.assets.getProgress() * 100) + "%");
                super.act(delta);
            }
        };

        verticalGroup.setScale(Gdx.graphics.getHeight() / image.getHeight() / 3);
        verticalGroup.columnCenter().center();
        verticalGroup.setPosition(Gdx.graphics.getWidth() / 2,
                Gdx.graphics.getHeight() / 2);
        System.out.println(verticalGroup.getHeight());
        verticalGroup.addActor(image);
        verticalGroup.space(20);
        verticalGroup.addActor(loadingText);
        addActor(verticalGroup);
        //setDebugAll(true);
    }

}
