package com.netrunnergame.graphics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.netrunnergame.NetRunnerGame;
import com.netrunnergame.core.LevelLoader;

import java.util.LinkedList;
import java.util.List;

/**
 * Displays the end of game screen
 *
 * @author Andrew Konstantinov, Pozigun Mikhail
 * @version 0.2
 * @see MenuScreen
 */

public class EndOfGameScreen extends MenuScreen {

    protected String message = "";

    /**
     * Instantiates a new Menu screen.
     *
     * @param game is the game
     */
    public EndOfGameScreen(NetRunnerGame game) {
        super(game);
    }

    @Override
    void makeAfterLoading() {
        Skin skin = game.assets.get(SKIN_PATH);
        List<MenuStage.ButtonSettings> buttons = new LinkedList<>();
        buttons.add(new MenuStage.ButtonSettings(
                "play again",
                new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        game.setScreen(new LevelScene(game, new LevelLoader(game, game.getCurrentLevelFile()).buildLevel()));
                    }
                }
        ));
        buttons.add(new MenuStage.ButtonSettings(
                "main menu",
                new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        game.setScreen(new MenuScreen(game));
                    }
                }
        ));
        mainMenuStage = new EndOfGameStage(message, buttons, skin);
        setCurrentStage(mainMenuStage);
        currentStage.getViewport().update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }
}
