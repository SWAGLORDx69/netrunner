package com.netrunnergame.graphics;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.math.Vector3;
import com.google.common.collect.ImmutableMap;
import com.netrunnergame.utils.NonExistentNodeIdException;
import com.netrunnergame.utils.NonExistentNodeTypeException;

public class GMFactory implements GMIManager.Factory {
    private final ImmutableMap<String, Model> nodeModels;
    private final Model linkModel;
    private final String playerName;
    private final Color playerColor;
    private final Color neutralColor;
    private final Color candidateColor;

    public GMFactory(ImmutableMap<String, Model> nodeModels, Model linkModel, String playerName,
                     Color playerColor, Color neutralColor, Color candidateColor) {
        this.nodeModels = nodeModels;
        this.linkModel = linkModel;
        this.playerName = playerName;
        this.playerColor = playerColor;
        this.neutralColor = neutralColor;
        this.candidateColor = candidateColor;
    }

    public ImmutableMap<String, Model> getNodeModels() {
        return nodeModels;
    }

    public Model getLinkModel() {
        return linkModel;
    }

    public String getPlayerName() {
        return playerName;
    }

    public Color getPlayerColor() {
        return playerColor;
    }

    public Color getNeutralColor() {
        return neutralColor;
    }

    public Color getCandidateColor() {
        return candidateColor;
    }

    @Override
    public GMIManager.Node createNode(int id, String type, Vector3 pos, String owner) throws NonExistentNodeTypeException {
        return new GMNode(id, type, pos, owner, this);
    }

    @Override
    public GMIManager.Link createLink(GMIManager.Node a, GMIManager.Node b, GMIManager.Direction direction)
            throws NonExistentNodeIdException {
        return new GMLink(a, b, direction, linkModel);
    }
}
