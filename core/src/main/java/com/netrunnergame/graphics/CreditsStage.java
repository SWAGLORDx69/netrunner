package com.netrunnergame.graphics;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import java.util.List;

/**
 * The Credits stage.
 *
 * @author Pozigun Mikhail
 * @version 0.1
 * @see Stage
 */
public class CreditsStage extends Stage {
    /**
     * size of a space in percent
     */
    private static final float SPACE_PCT = 0.03f;
    /**
     * size of a pad in percent for head
     */
    private static final float HEAD_PAD_PCT = 0.05f;
    /**
     * size of a pad in percent
     */
    private static final float PAD_PCT = 0.04f;

    /**
     * Instantiates a new Credits stage.
     *
     * @param head      the head
     * @param bottomBut the bottom but
     * @param skin      the skin
     */
    public CreditsStage(String head, ScrollMenuStage.ButtonSettings bottomBut, Skin skin) {
        super(new ExtendViewport(800, 480));

        Table mainTable = new Table();

        mainTable.setPosition(0, 0);
        mainTable.setHeight(getViewport().getWorldHeight());
        mainTable.setWidth(getViewport().getWorldWidth());
        mainTable.center();
        mainTable.setSkin(skin);
        addActor(mainTable);

        Value space = Value.percentHeight(SPACE_PCT, mainTable);
        Value padY = Value.percentHeight(PAD_PCT, mainTable);
        Value padYHead = Value.percentHeight(HEAD_PAD_PCT, mainTable);
        Value zero = Value.zero;

        mainTable.add(head, "head")
                .expand()
                .pad(padY, zero, padYHead, zero)
                .space(space)
                .fill().getActor().setAlignment(Align.center);
        mainTable.row();

        mainTable.add("The team:")
                .expand()
                .pad(padY, zero, padYHead, zero)
                .space(space)
                .fill().getActor().setAlignment(Align.center);
        mainTable.row();

        String[] theTeamNames = new String[]{"Konstantinov Andrew",
                "Kots Michael", "Pozigun Mikhail", "Provorov Peter", "Smirnov Kirill"};

        for (String name : theTeamNames) {
            mainTable.add(name)
                    .expand()
                    .pad(zero, zero, zero, zero)
                    .space(space)
                    .fill().getActor().setAlignment(Align.center);
            mainTable.row();
        }

        mainTable.add("© Netrunner team", "small")
                .expand()
                .pad(zero, zero, zero, zero)
                .space(space)
                .fill().getActor().setAlignment(Align.center);
        mainTable.row();

        mainTable.add(createButton(bottomBut.text, bottomBut.listener, skin))
                .width(Value.percentWidth(2f))
                .height(Value.percentHeight(0.1f, mainTable))
                .pad(Value.percentHeight(1f));
    }

    /**
     * create a text button
     *
     * @param text     the text which will be wrote on the button
     * @param listener if is null then the button is disabled
     * @param skin     the skin
     * @return button
     * @see ClickListener
     * @see Button
     */
    private Button createButton(String text, ClickListener listener, Skin skin) {
        TextButton button = new TextButton(text, skin);
        if (listener != null) {
            button.addListener(listener);
        } else {
            button.setDisabled(true);
        }
        return button;
    }
}
