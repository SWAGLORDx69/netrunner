package com.netrunnergame.graphics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.*;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.netrunnergame.NetRunnerGame;
import com.netrunnergame.core.LevelController;
import com.netrunnergame.core.NodeView;
import com.netrunnergame.core.Program;
import com.netrunnergame.core.programs.GameProgramWithArg;

import java.util.*;

/**
 * The type Program panel.
 *
 * @author Provorov Piotr, Pozigun Mikhail, Kots Michael
 * @version 0.3
 * @see Stage
 */
public class ProgramPanel extends Stage implements Observer {
    /**
     * The constant width of every program in program panel.
     */
    private int BUTTON_WIDTH;
    /**
     * The constant height of every program in program panel.
     */
    private int BUTTON_HEIGHT;
    /**
     * The viewport screen height.
     */
    private float screenHeight;
    /**
     * The viewport screen width.
     */
    private float screenWidth;
    /**
     * The table with info about programs.
     */
    private Table programPanelTable;
    /**
     * The level (here for getting info about chosen node).
     */
    private LevelController level;
    /**
     * The Level scene
     */
    private LevelScene levelScene;
    /**
     * The game (here for getting fonts).
     */
    private NetRunnerGame game;
    /**
     * The skin of all buttons.
     */
    private Skin skin = new Skin();
    /**
     * Contains node instances
     */
    private GMIManager instancesContainer;
    /**
     * The program tooltip.
     */
    private ProgramTooltip programTooltip;
    /**
     * The file tooltip.
     */
    private FileTooltip fileTooltip;
    /**
     * Current program that tooltip is shown.
     */
    private ImageButton curProgramButton;
    /**
     * Menu background
     */
    private ImageButton background;
    /**
     * Button for resetting argument selection.
     */
    private ResetArgsButton resetArgsButton;

    private ErrorMsgForPlayer errorMsgForPlayer;

    private int curNodeId = -1;

    private boolean isShow;

    private List<Double> programsDataCosts;

    private double prevDataAmount;

    /**
     * Instantiates a new Program panel.
     *
     * @param level              the level
     * @param levelScene         the level scene
     * @param game               the game
     * @param programTooltip     the program tooltip
     * @param fileTooltip        the file tooltip
     * @param instancesContainer the instances container
     * @param resetArgsButton    the reset args button
     * @param errorMsgForPlayer  the error msg for player
     */
    public ProgramPanel(LevelController level,
                        LevelScene levelScene,
                        NetRunnerGame game,
                        ProgramTooltip programTooltip,
                        FileTooltip fileTooltip,
                        GMIManager instancesContainer,
                        ResetArgsButton resetArgsButton,
                        ErrorMsgForPlayer errorMsgForPlayer) {
        super(new ExtendViewport(480, 800));
        screenWidth = super.getViewport().getWorldWidth();
        screenHeight = super.getViewport().getWorldHeight();
        initSizes();

        this.level = level;
        this.levelScene = levelScene;
        this.game = game;
        this.programTooltip = programTooltip;
        this.fileTooltip = fileTooltip;
        this.instancesContainer = instancesContainer;
        this.resetArgsButton = resetArgsButton;
        this.errorMsgForPlayer = errorMsgForPlayer;
        this.prevDataAmount = level.getDataAmount();

        programsDataCosts = new ArrayList<>();
        for (Program program : level.getAvailablePrograms()) {
            programsDataCosts.add(program.getDataCost());
        }
        Collections.sort(programsDataCosts);

        programPanelTable = new Table();
        programPanelTable.setVisible(true);
        programPanelTable.setPosition(screenWidth / 2 - BUTTON_WIDTH, 0);
        programPanelTable.setHeight(BUTTON_HEIGHT);
        programPanelTable.setWidth(BUTTON_WIDTH);
        initSkin();

        background = new ImageButton(skin, "background");
        background.setVisible(false);

        addActor(background);
        addActor(programPanelTable);

        level.addObserver(this);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof NodeView) {
            NodeView node = (NodeView) arg;
            if (node.getId() == curNodeId) {
                if (node.getStatus() != 0.0f && node.getStatus() != 1.0f) {
                    return;
                }
                setCurNode(curNodeId);
            }
        }
    }

    /**
     * Init skin.
     */
    private void initSkin() {
        skin.add("default", game.getFont("text"));
        TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.font = skin.getFont("default");

        Pixmap pixmap = new Pixmap(BUTTON_WIDTH * 5, BUTTON_HEIGHT, Pixmap.Format.RGB888);
        pixmap.setColor(Color.WHITE);
        pixmap.fill();
        skin.add("backgr", new Texture(pixmap));

        ImageButton.ImageButtonStyle imageButtonStyle = new ImageButton.ImageButtonStyle();
        imageButtonStyle.up = skin.newDrawable("backgr", game.getColor("program menu background"));
        skin.add("background", imageButtonStyle);
    }

    /**
     * Init sizes.
     */
    private void initSizes() {
        this.BUTTON_WIDTH = (int) (screenWidth / 13);
        this.BUTTON_HEIGHT = (int) (screenHeight / 10);
    }

    /**
     * Hide program panel.
     */
    public void hide() {
        isShow = false;
        programPanelTable.setVisible(false);
        if (programTooltip.getOwner().equals(ProgramTooltip.Owner.OWNER_PROGRAM_PANEL)) {
            programTooltip.hide();
        }
    }

    /**
     * Show program panel.
     */
    public void show() {
        isShow = true;
        programPanelTable.setVisible(true);
    }

    /**
     * Add programs.
     *
     * @param nodeId the node id
     */
    public void setCurNode(int nodeId) {
        programPanelTable.clear();
        curNodeId = nodeId;

        for (final Program programPrototype : level.getAvailablePrograms()) {
            if (level.getNodeView(curNodeId).getOwner() == level &&
                    level.isAbleToRunProgram(programPrototype, curNodeId)) {
                TextureAtlas programIconAtlas = game.assets.get("data/sprites/programicon.txt");
                TextureRegion programIconRegion = programIconAtlas.findRegion(programPrototype.getName());
                TextureRegionDrawable programIconRegDrawable = new TextureRegionDrawable(programIconRegion);
                final ImageButton programButton = new ImageButton(programIconRegDrawable);

                programButton.setWidth(BUTTON_WIDTH);
                programButton.setHeight(BUTTON_HEIGHT);

                programButton.addListener(new ActorGestureListener(20, 0.4f, 0.6f, 0.15f) {

                    boolean isLongPressed = false;

                    @Override
                    public void tap(InputEvent event, float x, float y, int count, int button) {
                        if (isLongPressed) {
                            isLongPressed = false;
                            return;
                        }
                        if (programTooltip.getOwner().equals(ProgramTooltip.Owner.OWNER_PROGRAM_PANEL) &&
                                curProgramButton.equals(programButton)) {
                            programTooltip.hide();
                            return;
                        }
                        curProgramButton = programButton;
                        programTooltip.setOwner(ProgramTooltip.Owner.OWNER_PROGRAM_PANEL);
                        programTooltip.setProgram(
                                new Vector2(programPanelTable.getX() + curProgramButton.getX() + BUTTON_WIDTH / 2,
                                        programPanelTable.getY() + curProgramButton.getY() + BUTTON_HEIGHT / 2),
                                programPrototype);

                        fileTooltip.hide();
                    }

                    @Override
                    public boolean longPress(Actor actor, float x, float y) {
                        isLongPressed = true;

                        if (levelScene.getWaitingProgram() != null) {
                            Gdx.app.debug("graphics", "stop long press without action");
                            return false;
                        }
                        Gdx.app.debug("graphics", "long press");

                        levelScene.markClean();

                        if (programPrototype instanceof GameProgramWithArg) {
                            GameProgramWithArg program = (GameProgramWithArg) programPrototype.clone();
                            program.setHost(level.getNodeView(curNodeId));
                            levelScene.setWaitingProgram(program);
                            resetArgsButton.setResetButtonVisible(true);
                            resetArgsButton.setCurNode(curNodeId);
                            if (program.getIdOfPossibleArg() != null) {
                                for (int idx : program.getIdOfPossibleArg()) {
                                    instancesContainer.getNode(idx).mark();
                                }
                            }
                        } else {
                            Program program = programPrototype.clone();
                            program.setHost(level.getNodeView(curNodeId));
                            if (level.runProgram(program, curNodeId)) {
                                instancesContainer.getNode(curNodeId).incRotationAngle();
                            }
                        }
                        return false;
                    }
                });
                programPanelTable.add(programButton).width(BUTTON_WIDTH).height(BUTTON_HEIGHT);
            } else {
                TextureAtlas programIconAtlas = game.assets.get("data/sprites/programiconnotavail.txt");
                TextureRegion programIconRegion = programIconAtlas.findRegion(programPrototype.getName());
                TextureRegionDrawable programIconRegDrawable = new TextureRegionDrawable(programIconRegion);
                final ImageButton programButton = new ImageButton(programIconRegDrawable);

                programButton.setWidth(BUTTON_WIDTH);
                programButton.setHeight(BUTTON_HEIGHT);

                programButton.addListener(new ActorGestureListener() {
                    @Override
                    public void tap(InputEvent event, float x, float y, int count, int button) {
                        if (programTooltip.getOwner().equals(ProgramTooltip.Owner.OWNER_PROGRAM_PANEL) &&
                                curProgramButton.equals(programButton)) {
                            programTooltip.hide();
                            return;
                        }
                        curProgramButton = programButton;
                        programTooltip.setOwner(ProgramTooltip.Owner.OWNER_PROGRAM_PANEL);
                        programTooltip.setProgram(
                                new Vector2(programPanelTable.getX() + curProgramButton.getX() + BUTTON_WIDTH / 2,
                                        programPanelTable.getY() + curProgramButton.getY() + BUTTON_HEIGHT / 2),
                                programPrototype);
                    }
                });
                programPanelTable.add(programButton).width(BUTTON_WIDTH).height(BUTTON_HEIGHT);
            }
        }
        programPanelTable.pack();
        programPanelTable.setPosition(screenWidth / 2 - programPanelTable.getWidth() / 2, 0);
        background.setVisible(true);
        background.setBounds(programPanelTable.getX(),
                0,
                programPanelTable.getWidth(),
                programPanelTable.getMinHeight());

        show();
    }

    /**
     * Init resources.
     *
     * @param instancesContainer the instances container
     */
    public void initResources(GMIManager instancesContainer) {
        this.instancesContainer = instancesContainer;
    }

    private boolean isNeedUpdateBecauseOfDataAmount(double curDataAmount) {
        boolean isOneOfDataCosts = false;
        for (double cost : programsDataCosts) {
            if (curDataAmount == cost) {
                isOneOfDataCosts = true;
                break;
            }
        }
        return isOneOfDataCosts;
    }

    @Override
    public void draw() {
        if (isShow) {
            double curDataAmount = level.getDataAmount();
            if (prevDataAmount != curDataAmount && curDataAmount >= programsDataCosts.get(0) &&
                    curDataAmount <= programsDataCosts.get(programsDataCosts.size() - 1)) {
                if (isNeedUpdateBecauseOfDataAmount(curDataAmount)) {
                    setCurNode(curNodeId);
                }
                prevDataAmount = curDataAmount;
            }
            super.draw();
        }
    }

}
