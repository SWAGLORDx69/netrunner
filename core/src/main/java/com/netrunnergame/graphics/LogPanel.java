package com.netrunnergame.graphics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.netrunnergame.NetRunnerGame;
import com.netrunnergame.core.LevelController;
import com.netrunnergame.core.Log;

import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;

/**
 * Log panel.
 *
 * @author Pozigun Mikhail
 * @version 0.3
 * @see Stage
 */
public class LogPanel extends Stage implements Observer{
    /**
     * The constant width of every message in log panel.
     */
    private static int BUTTON_WIDTH;
    /**
     * The constant height of every message in log panel.
     */
    private static int BUTTON_HEIGHT;
    /**
     * The constant width of log panel.
     */
    private static int LOG_WIDTH;
    /**
     * The constant height of log panel.
     */
    private static int LOG_HEIGHT;
    /**
     * The constant padding for text of messages(left and right).
     */
    private static int PAD_RIGHT_LEFT;
    /**
     * The constant padding for text of messages(top and bottom).
     */
    private static int PAD_TOP_BOTTOM;
    /**
     * The vertical group with log.
     */
    private VerticalGroup logVerGroup;
    /**
     * The game (here for getting fonts).
     */
    private NetRunnerGame game;
    /**
     * The skin of all buttons.
     */
    private Skin skin = new Skin();
    /**
     * The scrollbar.
     */
    private ScrollPane scrollPane;
    /**
     * The viewport screen height.
     */
    private float screenHeight;
    /**
     * The viewport screen width.
     */
    private float screenWidth;
    /**
     * The background for log.
     */
    private ImageButton background;
    /**
     * The log button.
     */
    private TextButton logButton;

    /**
     * Instantiates a new Log panel.
     *
     * @param game  the game
     */
    public LogPanel(NetRunnerGame game) {
        super(new ExtendViewport(480, 800));
        screenWidth = super.getViewport().getWorldWidth();
        screenHeight = super.getViewport().getWorldHeight();
        initSizes();

        this.game = game;
        logVerGroup = new VerticalGroup();
        logVerGroup.setVisible(false);
        logVerGroup.setPosition(screenWidth - LOG_WIDTH,
                screenHeight - LOG_HEIGHT - BUTTON_HEIGHT);
        scrollPane = new ScrollPane(logVerGroup);
        scrollPane.setVisible(false);
        scrollPane.setScrollingDisabled(true, false);
        scrollPane.setBounds(screenWidth - LOG_WIDTH,
                screenHeight - LOG_HEIGHT - BUTTON_HEIGHT, LOG_WIDTH, LOG_HEIGHT);

        initSkin();

        background = new ImageButton(skin, "background");
        background.setVisible(false);
        background.setBounds(screenWidth - LOG_WIDTH,
                screenHeight - LOG_HEIGHT - BUTTON_HEIGHT, LOG_WIDTH, LOG_HEIGHT);

        logButton = new TextButton("Log", skin, "button");
        logButton.setVisible(true);
        logButton.setX(screenWidth - BUTTON_WIDTH);
        logButton.setY(screenHeight - BUTTON_HEIGHT);
        logButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (logVerGroup.isVisible()) {
                    logVerGroup.setVisible(false);
                    scrollPane.setVisible(false);
                    background.setVisible(false);
                } else {
                    logVerGroup.setVisible(true);
                    scrollPane.setVisible(true);
                    background.setVisible(true);
                }
            }
        });
        logButton.getLabel().setAlignment(Align.center);
        logButton.padLeft(PAD_RIGHT_LEFT).padRight(PAD_RIGHT_LEFT).padBottom(PAD_TOP_BOTTOM).padTop(PAD_TOP_BOTTOM);

        addActor(background);
        addActor(logButton);
        addActor(scrollPane);
    }

    /**
     * Is visible boolean.
     *
     * @return the boolean
     */
    public boolean isVisible()  {
        return logVerGroup.isVisible();
    }

    /**
     * Hide log.
     */
    public void hide() {
        logButton.setChecked(false);
        logVerGroup.setVisible(false);
        scrollPane.setVisible(false);
        background.setVisible(false);
    }

    /**
     * Init sizes.
     */
    private void initSizes() {
        BUTTON_WIDTH = (int) (screenWidth / 10);
        BUTTON_HEIGHT = (int) (screenHeight / 10);
        LOG_WIDTH = (int) (screenWidth / 3);
        LOG_HEIGHT = (int) (screenHeight / 2);
        PAD_RIGHT_LEFT = (int) (screenWidth / 100);
        PAD_TOP_BOTTOM = (int) (screenWidth / 100);
    }

    /**
     * Init skin.
     */
    private void initSkin() {
        skin.add("button", game.getFont("button"));
        skin.add("text", game.getFont("text"));
        Pixmap pixmapButton = new Pixmap(BUTTON_WIDTH, BUTTON_HEIGHT, Pixmap.Format.RGB888);
        pixmapButton.setColor(game.getColor("text head"));
        pixmapButton.fill();
        skin.add("backgroundButton", new Texture(pixmapButton));

        TextButton.TextButtonStyle textButtonStyleButton = new TextButton.TextButtonStyle();
        textButtonStyleButton.up = skin.newDrawable("backgroundButton", game.getColor("button"));
        textButtonStyleButton.checked = skin.newDrawable("backgroundButton", game.getColor("button touched"));
        textButtonStyleButton.font = skin.getFont("button");
        textButtonStyleButton.fontColor = game.getColor("text head");
        skin.add("button", textButtonStyleButton);

        Pixmap pixmapText = new Pixmap(LOG_WIDTH, PAD_RIGHT_LEFT, Pixmap.Format.RGB888);
        pixmapText.setColor(game.getColor("text background"));
        pixmapText.fill();
        skin.add("backgroundText", new Texture(pixmapText));

        TextButton.TextButtonStyle textButtonStyleText = new TextButton.TextButtonStyle();
        textButtonStyleText.up = skin.newDrawable("backgroundText", new Color(0xFFFFFF00));
        textButtonStyleText.font = skin.getFont("text");
        textButtonStyleText.fontColor = game.getColor("text");
        skin.add("default", textButtonStyleText);

        ImageButton.ImageButtonStyle background = new ImageButton.ImageButtonStyle();
        background.up = skin.newDrawable("backgroundText", game.getColor("text background"));
        skin.add("background", background);
    }


    @Override
    public void update(Observable o, Object arg) {
        if(arg instanceof Log) {
            Log log = (Log) arg;
            Iterator<String> iterator = log.getLog();
            TextButton button = new TextButton("", skin);
            while (iterator.hasNext()) {
                String logStr = iterator.next();
                Gdx.app.log("log", "posted a message" + logStr);
                button.setText(logStr);
                button.getLabel().setWrap(true);
                button.getLabel().setAlignment(Align.left);
                logVerGroup.addActor(button.padRight(PAD_RIGHT_LEFT).padLeft(PAD_RIGHT_LEFT));
                scrollPane.validate();
                scrollPane.validate();
                scrollPane.scrollTo(0, 0, 0, 0);
            }
        }
    }
}
