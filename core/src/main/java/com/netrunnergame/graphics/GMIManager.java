package com.netrunnergame.graphics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.utils.IntMap;
import com.netrunnergame.core.LevelController;
import com.netrunnergame.core.NodeView;
import com.netrunnergame.utils.NonExistentNodeIdException;
import com.netrunnergame.utils.NonExistentNodeTypeException;
import com.netrunnergame.utils.PairInt;
import com.netrunnergame.utils.SymmetricPairComparator;

import java.util.*;

import static java.lang.Math.abs;
import static java.lang.Math.max;

/**
 * Contains and manages models and model instances
 * Graph Instance Model Manager
 *
 * @author Kirill Smirnov
 * @version 1.0
 * @see Model
 * @see ModelInstance
 * @see NonExistentNodeIdException
 * @see NonExistentNodeTypeException
 * @see PairInt
 */
public class GMIManager implements Iterable<GMIManager.Node>, Observer {
    //содержит узлы в которых содержится необходимая для управления и отрисовки информация
    //в том числе и ModelInstance
    //ключами являются id узла для быстрого поиска
    private final IntMap<Node> nodes = new IntMap<>();
    //содержит ребра в которых содержится необходимая для управления и отрисовки информация
    private final TreeMap<PairInt, Link> links = new TreeMap<>(new SymmetricPairComparator());
    private final Vector3 center = new Vector3();
    private final Vector3 radius = new Vector3();
    //проводит соответствие между типом узла и его графическим представлением
    //ключами являются все допустимые типы узлов
    //значениями являются модели из которых в процессе работы создаются ModelInstance
    //private ImmutableMap<String, Model> nodeModels;
    //модель из которых в процессе работы создаются ModelInstance для связей графа
    //в дальнейшем эта часть будет дополнена и расширена
    //private Model linkModel;
    private ModelBatch modelBatch;
    private Environment environment;
    private Factory factory;


    /**
     * Instantiates a new container.
     *
     * @param factory is
     */
    public GMIManager(Factory factory, Environment environment) {
        this.factory = factory;
        this.modelBatch = new ModelBatch();
        this.environment = environment;
        if (environment == null) {
            makeEnvironment();
        }
    }

    public static Builder builder(Factory factory, Environment environment) {
        return new GMIManager(factory, environment).new Builder();
    }

    private void updateRect() {
        center.set(0, 0, 0);
        for (Node node : nodes.values()) {
            center.add(node.getPos());
        }
        center.scl(1.0f / nodes.size);
        radius.set(10, 10, 10);
        for (Node node : nodes.values()) {
            radius.x = max(radius.x, abs(node.getPos().x - center.x));
            radius.y = max(radius.y, abs(node.getPos().y - center.y));
            radius.z = max(radius.z, abs(node.getPos().z - center.z));
        }
        radius.add(1, 1, 1);
    }

    public Vector3 getCenter() {
        return center;
    }

    public Vector3 getRadius() {
        return radius;
    }

    /**
     * Add new node.
     *
     * @param id   is the id of node
     * @param type is the type of node
     * @param pos  is the position of node
     * @throws NonExistentNodeTypeException the non existent node type exception
     */
    public void addNode(int id, String type, Vector3 pos, String owner) throws NonExistentNodeTypeException {
        nodes.put(id, factory.createNode(id, type, pos, owner));
    }

    /**
     * Add a bilateral link between nodes A and B
     *
     * @param idA is the id of a node A
     * @param idB is the id of a node B
     * @throws NonExistentNodeIdException if one of A or B doesn't exist
     */
    public void addLink(int idA, int idB) throws NonExistentNodeIdException {
        addLink(idA, idB, Direction.BOTH);
    }

    /**
     * Gets a node by ID.
     *
     * @param id is the id
     * @return the Node with requested ID
     */
    public Node getNode(int id) {
        return nodes.get(id);
    }

    /**
     * Add a link between nodes A and B
     *
     * @param idA is the id of a node A
     * @param idB is the id of a node B
     * @param dir is a direction of the link
     * @throws NonExistentNodeIdException if one of A or B doesn't exist
     */
    public void addLink(int idA, int idB, Direction dir) throws NonExistentNodeIdException {
        if (!nodes.containsKey(idA)) {
            throw new NonExistentNodeIdException(idA);
        }
        if (!nodes.containsKey(idB)) {
            throw new NonExistentNodeIdException(idB);
        }
        links.put(new PairInt(idA, idB), factory.createLink(nodes.get(idA), nodes.get(idB), dir));
    }

    /**
     * get the link between nodes A and B .
     *
     * @param idA the id a
     * @param idB the id b
     * @return the link if it exists or null else
     */
    public Link getLink(int idA, int idB) {
        return links.get(new PairInt(idA, idB));
    }

    public Link getLink(PairInt pair) {
        return links.get(pair);
    }

    public Builder builder() {
        return new Builder();
    }

    @Override
    public Iterator<Node> iterator() {
        return nodeIterator();
    }

    /**
     * Node iterator
     *
     * @return the iterator by the nodes
     */
    public Iterator<Node> nodeIterator() {
        return new Iterator<Node>() {
            private final Iterator<IntMap.Entry<Node>> itNode = nodes.iterator();

            @Override
            public boolean hasNext() {
                return itNode.hasNext();
            }

            @Override
            public Node next() {
                return itNode.next().value;
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("remove");

            }
        };
    }

    public void act() {
        act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
    }

    public void act(float delta) {
        for (IntMap.Entry<Node> entry : nodes) {
            entry.value.act(delta);
        }
        for (Link link : links.values()) {
            link.act(delta);
        }
    }

    public void unmarkAll() {
        for (IntMap.Entry<Node> entry : nodes) {
            entry.value.unmark();
        }
    }

    public void draw(Camera cam) {
        modelBatch.begin(cam);
        for (IntMap.Entry<Node> entry : nodes) {
            entry.value.draw(modelBatch, environment);
        }
        for (Link link : links.values()) {
            link.draw(modelBatch, environment);
        }
        modelBatch.end();
    }

    private void makeEnvironment() {
        environment = new Environment();
        environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
        environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));
        environment.add(new DirectionalLight().set(0.5f, 0.5f, 0.5f, 1f, 1.2f, 0.5f));
    }

    @Override
    public void update(Observable o, Object arg) {
        LevelController lvl = (LevelController) o;
        //TODO rewrite this shit
        if (arg instanceof NodeView) {
            NodeView nodeView = (NodeView) arg;
            int id = nodeView.getId();
            Node node = getNode(id);
            try {
                boolean isNeedUpdateLink = false;
                if (node == null) {
                    addNode(id, nodeView.getType(), nodeView.getPosition(), nodeView.getOwnerName());
                    node = getNode(id);
                    isNeedUpdateLink = true;
                }
                if (nodeView.isSameOwner(node.getOwner())) {
                    isNeedUpdateLink = true;
                }
                getNode(id).update(nodeView);
                if (isNeedUpdateLink) {
                    Iterator<NodeView> it = nodeView.getLinkedNodes();
                    while (it.hasNext()) {
                        NodeView ln = it.next();
                        Direction dir = Direction.ATOB;
                        Iterator<NodeView> itt = ln.getLinkedNodes();
                        while (itt.hasNext()) {
                            if (itt.next().getId() == id) {
                                dir = Direction.BOTH;
                                break;
                            }
                        }
                        PairInt pair = new PairInt(id, ln.getId());
                        Link link = getLink(pair);
                        if (link == null) {
                            try {
                                addLink(pair.getA(), pair.getB());
                            } catch (NonExistentNodeIdException e) {
                            }

                        } else {
                            link.setDirection(dir);
                        }
                    }


                    for (NodeView nv : lvl) {
                        boolean is = false;
                        Iterator<NodeView> itt = nv.getLinkedNodes();
                        while (itt.hasNext()) {
                            if (itt.next().getId() == id) {
                                is = true;
                                break;
                            }
                        }
                        if (is) {
                            Link link = getLink(nv.getId(), id);
                            if (link == null) {
                                addLink(nv.getId(), id, Direction.ATOB);
                            } else {
                                link.setDirection(Direction.BOTH);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                Gdx.app.error("gmim", "gmim failed", e);
            }
            updateRect();
            Gdx.app.debug("gmim", "node " + nodeView.getId() + "is updated");
        }
    }

    /**
     * the enum listed the possible directions of link
     *
     * @author Kirill Smirnov
     * @version 1.0
     */
    public enum Direction {
        /**
         * From node A to node B.
         */
        ATOB,
        /**
         * From node B to node A.
         */
        BTOA,
        /**
         * bilateral
         */
        BOTH
    }

    public interface Factory {

        Node createNode(int id, String type, Vector3 pos, String owner) throws NonExistentNodeTypeException;

        Link createLink(Node a, Node b, Direction direction) throws NonExistentNodeIdException;
    }

    /**
     * Class of node
     *
     * @author Kirill Smirnov
     * @version 1.0
     */
    public static abstract class Node {
        /**
         * The Id.
         */
        final int id;
        private final BoundingBox boundingBox = new BoundingBox();
        protected boolean isMarked = false;
        private Vector3 pos;
        private String type;
        private String owner;

        /**
         * Instantiates a new Node.
         *
         * @param id   the id
         * @param type the type
         * @param pos  the pos
         * @throws NonExistentNodeTypeException the non existent node type exception
         */
        public Node(int id, String type, Vector3 pos, String owner) throws NonExistentNodeTypeException {
            this.id = id;
            this.type = type;
            this.pos = new Vector3(pos);
        }

        @Override
        public boolean equals(Object o) {
            return this == o || !(o == null || getClass() != o.getClass()) && id == ((Node) o).id;
        }

        @Override
        public int hashCode() {
            return id;
        }

        /**
         * Gets pos.
         *
         * @return the pos
         */
        public Vector3 getPos() {
            return pos;
        }

        /**
         * Sets pos.
         *
         * @param pos the pos
         */
        void setPos(Vector3 pos) {
            this.pos.set(pos);
        }

        public boolean isMarked() {
            return isMarked;
        }

        public abstract void mark();

        public abstract void unmark();

        public final void incRotationAngle() {
        }

        ;

        public final void decRotationAngle() {
        }

        ;

        public String getOwner() {
            return owner;
        }

        public void setOwner(String owner) {
            this.owner = owner;
        }

        /**
         * Gets type.
         *
         * @return the type
         */
        public String getType() {
            return type;
        }

        /**
         * Sets type.
         *
         * @param type the type
         * @throws NonExistentNodeTypeException the non existent node type exception
         */
        void setType(String type) throws NonExistentNodeTypeException {
            this.type = type;
        }

        public BoundingBox getBoundingBox() {
            return boundingBox;
        }

        public float getModelInstRad() {
            return 0.5f * (float) Math.sqrt(
                    boundingBox.getHeight() * boundingBox.getHeight() +
                            boundingBox.getDepth() * boundingBox.getDepth() +
                            boundingBox.getWidth() * boundingBox.getWidth()
            );
        }

        public void act(float delta) {
            //nothing
        }

        public void update(NodeView node) throws NonExistentNodeTypeException {
            if (!getPos().equals(node.getPosition())) {
                setPos(node.getPosition());
            }
            if (!getType().equals(node.getType())) {
                setType(node.getType());
            }
            if (!getOwner().equals(node.getOwner().getName())) {
                setOwner(node.getOwner().getName());
            }
        }

        public abstract void draw(ModelBatch modelBatch, Environment environment);
    }

    /**
     * Class of link
     *
     * @author Kirill Smirnov
     * @version 1.0
     */
    public static abstract class Link {
        private final Node a;
        private final Node b;
        Direction direction;

        /**
         * Instantiates a new Link.
         *
         * @param a         the a
         * @param b         the b
         * @param direction the direction
         */
        public Link(Node a, Node b, Direction direction) {
            this.a = a;
            this.b = b;
            this.direction = direction;
        }

        @Override
        public final boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Link link = (Link) o;
            return a.equals(link.a) && b.equals(link.b);

        }

        /**
         * Gets id a.
         *
         * @return the id a
         */
        public int getIdA() {
            return a.id;
        }

        /**
         * Gets id b.
         *
         * @return the id b
         */
        public int getIdB() {
            return b.id;
        }

        public Node getA() {
            return a;
        }

        /**
         * Gets id b.
         *
         * @return the id b
         */
        public Node getB() {
            return b;
        }

        public abstract void update();

        @Override
        public final int hashCode() {
            return 31 * a.hashCode() + b.hashCode();
        }

        /**
         * Gets direction.
         *
         * @return the direction
         */
        public Direction getDirection() {
            return direction;
        }

        /**
         * Sets direction.
         *
         * @param direction the direction
         */
        public void setDirection(Direction direction) {
            this.direction = direction;
        }

        public void act(float delta) {
            //nothing
        }

        public abstract void draw(ModelBatch modelBatch, Environment environment);
    }

    /**
     * builder
     *
     * @author Kirill Smirnov
     * @version 1.0
     */
    public class Builder {
        private final List<PairInt> linksTmp = new LinkedList<>();

        /**
         * Instantiates a new Builder.
         */
        public Builder() {
        }

        /**
         * New node builder.
         *
         * @param id   the id
         * @param type the type
         * @param pos  the pos
         * @return the builder
         * @throws NonExistentNodeTypeException the non existent node type exception
         */
        Builder newNode(int id, String type, Vector3 pos, String owner) throws NonExistentNodeTypeException {
            nodes.put(id, factory.createNode(id, type, pos, owner));
            return this;
        }

        /**
         * Link from to builder.
         *
         * @param idFrom the id from
         * @param idTo   the id to
         * @return the builder
         */
        Builder linkFromTo(int idFrom, int idTo) {
            linksTmp.add(new PairInt(idFrom, idTo));
            return this;
        }

        /**
         * Build graph model instances container.
         *
         * @return the graph model instances container
         */
        public GMIManager build() throws NonExistentNodeIdException {
            for (PairInt pair : linksTmp) {
                Link link = links.get(pair);
                if (link == null) {
                    Node from = nodes.get(pair.getA());
                    Node to = nodes.get(pair.getB());
                    if (from != null && to != null) {
                        link = factory.createLink(from, to, Direction.ATOB);
                        links.put(pair, link);
                    }
                } else if (link.getIdA() != pair.getA()) {
                    link.setDirection(Direction.BOTH);
                }
            }
            GMIManager.this.updateRect();
            return GMIManager.this;
        }
    }
}
