package com.netrunnergame.graphics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.netrunnergame.NetRunnerGame;
import com.netrunnergame.core.LevelController;

/**
 * Help.
 *
 * @author Pozigun Mikhail
 * @version 0.2
 * @see Stage
 */
public class Help extends Stage {
    /**
     * The width of help button.
     */
    private static int HELP_BUTTON_WIDTH;
    /**
     * The height of help button.
     */
    private static int HELP_BUTTON_HEIGHT;
    /**
     * The width.
     */
    private static int TABLE_WIDTH;
    /**
     * The height.
     */
    private static int TABLE_HEIGHT;
    /**
     * The buttons height.
     */
    private static int BUTTONS_HEIGHT;
    /**
     * The constant padding for text (left and right).
     */
    private static int PAD_RIGHT_LEFT;
    /**
     * The constant padding for text (bottom).
     */
    private static int PAD_BOTTOM_TEXT;
    /**
     * The constant padding for text (top).
     */
    private static int PAD_TOP_TEXT;
    /**
     * The constant padding for name (top and bottom).
     */
    private static int PAD_TOP_BOTTOM_NAME;
    /**
     * The game (here for getting fonts).
     */
    private NetRunnerGame game;
    /**
     * The skin of all buttons.
     */
    private Skin skin = new Skin();
    /**
     * The viewport screen height.
     */
    private float screenHeight;
    /**
     * The viewport screen width.
     */
    private float screenWidth;
    /**
     * The input processor for the game scene.
     */
    private InputProcessor inputProcessor;
    /**
     * The level.
     */
    private LevelController level;
    /**
     * The help head TextButton.
     */
    private TextButton helpHead;
    /**
     * The ImageButton for background.
     */
    private ImageButton background;
    /**
     * The scrollPane.
     */
    private ScrollPane scrollPane;
    /**
     * The close button.
     */
    private TextButton closeButton;

    /**
     * Instantiates a new Help.
     *
     * @param game  the game
     * @param level the level
     */
    public Help(NetRunnerGame game, LevelController level) {
        super(new ExtendViewport(480, 800));

        this.game = game;
        this.level = level;

        if (level.getCurLevelHelp() == null) {
            return;
        }

        screenWidth = super.getViewport().getWorldWidth();
        screenHeight = super.getViewport().getWorldHeight();
        initSizes();

        initSkin();
        //fill table
        final TextButton helpText = new TextButton(level.getCurLevelHelp(), skin);
        helpHead = new TextButton("Help", skin, "head");

        TextButton helpButton = new TextButton("Help", skin, "button");
        helpButton.setVisible(true);
        helpButton.setWidth(HELP_BUTTON_WIDTH);
        helpButton.setHeight(HELP_BUTTON_HEIGHT);
        helpButton.setPosition((1.0f + 0.05f) * HELP_BUTTON_WIDTH, screenHeight - HELP_BUTTON_HEIGHT);
        helpButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                background.setVisible(true);
                helpHead.setVisible(true);
                closeButton.setVisible(true);
                helpText.setVisible(true);
                scrollPane.setVisible(true);
                Gdx.input.setInputProcessor(Help.this);
            }
        });

        background = new ImageButton(skin, "background");
        background.setVisible(true);

        closeButton = new TextButton("Close", skin, "head");
        closeButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                background.setVisible(false);
                helpHead.setVisible(false);
                closeButton.setVisible(false);
                helpText.setVisible(false);
                scrollPane.setVisible(false);
                Gdx.input.setInputProcessor(inputProcessor);
            }
        });
        closeButton.padBottom(PAD_TOP_BOTTOM_NAME).padTop(PAD_TOP_BOTTOM_NAME);
        closeButton.setWidth(TABLE_WIDTH / 2);
        closeButton.setHeight(BUTTONS_HEIGHT);
        helpHead.getLabel().setAlignment(Align.center);
        helpText.getLabel().setAlignment(Align.left);
        helpText.getLabel().setWrap(true);
        helpText.getLabel().setWrap(true);
        helpHead.padBottom(PAD_TOP_BOTTOM_NAME).padTop(PAD_TOP_BOTTOM_NAME);
        helpHead.setWidth(TABLE_WIDTH);
        helpHead.setHeight(BUTTONS_HEIGHT);
        helpText.padLeft(PAD_RIGHT_LEFT).padRight(PAD_RIGHT_LEFT).padBottom(PAD_BOTTOM_TEXT).padTop(PAD_TOP_TEXT);

        VerticalGroup verGroup = new VerticalGroup();
        verGroup.addActor(helpText);
        scrollPane = new ScrollPane(verGroup);
        scrollPane.setVisible(true);
        scrollPane.setScrollingDisabled(true, false);
        scrollPane.setBounds(screenWidth / 2 - TABLE_WIDTH / 2, screenHeight / 2 - TABLE_HEIGHT / 2, TABLE_WIDTH, TABLE_HEIGHT);
        background.setBounds(scrollPane.getX(), scrollPane.getY(), scrollPane.getWidth(), scrollPane.getHeight());
        helpHead.setPosition(scrollPane.getX(), scrollPane.getY() + scrollPane.getHeight());
        closeButton.setPosition(scrollPane.getX() + scrollPane.getWidth() / 2, scrollPane.getY() - closeButton.getHeight());

        addActor(helpButton);
        addActor(helpHead);
        addActor(background);
        addActor(scrollPane);
        addActor(closeButton);
    }

    /**
     * Init sizes.
     */
    private void initSizes() {
        TABLE_WIDTH = (int) (screenWidth / 2.0f);
        TABLE_HEIGHT = (int) (screenHeight / 2.0f);
        BUTTONS_HEIGHT = TABLE_HEIGHT / 5;
        PAD_RIGHT_LEFT = TABLE_WIDTH / 20;
        PAD_TOP_BOTTOM_NAME = TABLE_HEIGHT / 4;
        PAD_TOP_TEXT = TABLE_HEIGHT / 16;
        PAD_BOTTOM_TEXT = TABLE_HEIGHT / 14;
        HELP_BUTTON_WIDTH = (int) (screenWidth / 10.0f);
        HELP_BUTTON_HEIGHT = (int) (screenHeight / 10.0f);
    }

    /**
     * Init skin.
     */
    private void initSkin() {
        skin.add("default", game.getFont("message"));
        skin.add("head", game.getFont("info"));
        skin.add("button", game.getFont("button"));
        Pixmap pixmap = new Pixmap(TABLE_WIDTH, PAD_RIGHT_LEFT, Pixmap.Format.RGB888);
        pixmap.setColor(Color.WHITE);
        pixmap.fill();
        skin.add("background", new Texture(pixmap));

        TextButton.TextButtonStyle textButtonStyleInfo = new TextButton.TextButtonStyle();
        textButtonStyleInfo.up = skin.newDrawable("background", new Color(0xFFFFFF00));
        textButtonStyleInfo.font = skin.getFont("default");
        textButtonStyleInfo.fontColor = game.getColor("text");
        skin.add("default", textButtonStyleInfo);

        TextButton.TextButtonStyle textButtonStyleHead = new TextButton.TextButtonStyle();
        textButtonStyleHead.up = skin.newDrawable("background", game.getColor("head background"));
        textButtonStyleHead.font = skin.getFont("head");
        textButtonStyleHead.fontColor = game.getColor("text head");
        skin.add("head", textButtonStyleHead);

        ImageButton.ImageButtonStyle background = new ImageButton.ImageButtonStyle();
        background.up = skin.newDrawable("background", game.getColor("text background"));
        skin.add("background", background);

        ImageButton.ImageButtonStyle shadow = new ImageButton.ImageButtonStyle();
        shadow.up = skin.newDrawable("background", game.getColor("shadow"));
        skin.add("shadow", shadow);

        TextButton.TextButtonStyle textButtonStyleButton = new TextButton.TextButtonStyle();
        textButtonStyleButton.up = skin.newDrawable("background", game.getColor("button"));
        textButtonStyleButton.font = skin.getFont("button");
        textButtonStyleButton.fontColor = game.getColor("text head");
        skin.add("button", textButtonStyleButton);
    }

    /**
     * Save game scene input processor.
     *
     * @param inputProcessor the input processor
     */
    public void saveGameSceneInputProcessor(InputProcessor inputProcessor) {
        this.inputProcessor = inputProcessor;
        if (level.getCurLevelHelp() != null) {
            Gdx.input.setInputProcessor(this);
        }
    }

    @Override
    public void draw() {
        super.draw();
    }

}