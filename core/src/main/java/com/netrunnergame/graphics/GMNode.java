package com.netrunnergame.graphics;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.math.Vector3;
import com.netrunnergame.core.NodeView;
import com.netrunnergame.utils.NonExistentNodeTypeException;

/**
 * Created by kirill on 29.03.17.
 */
public class GMNode extends GMIManager.Node {
    public static final int ROTATION_ANGLE_ONE_PROGRAM = 50;
    private ModelInstance modelInst;
    //private boolean isRotation = false;
    private GMFactory parent;
    private int curRotationAngle;
    private ColorAttribute color = ColorAttribute.createDiffuse(0, 0, 0, 1);

    /**
     * Instantiates a new Node.
     *
     * @param id   the id
     * @param type the type
     * @param pos  the pos
     * @throws NonExistentNodeTypeException the non existent node type exception
     */
    public GMNode(int id, String type, Vector3 pos, String owner, GMFactory factory)
            throws NonExistentNodeTypeException {
        super(id, type, pos, owner);
        parent = factory;
        setType(type);
        setPos(pos);
        setOwner(owner);
    }

    @Override
    public void setOwner(String owner) {
        if (owner == null) {
            assert (false);
        } else if (owner.equals(parent.getPlayerName())) {
            modelInst.materials.get(0).set(ColorAttribute.createDiffuse(parent.getPlayerColor()));
        } else {
            modelInst.materials.get(0).set(ColorAttribute.createDiffuse(parent.getNeutralColor()));
        }
        super.setOwner(owner);
    }

    /**
     * Sets pos.
     *
     * @param newPos the pos
     */
    void setPos(Vector3 newPos) {
        Vector3 pos = getPos();
        float x = newPos.x - pos.x;
        float y = newPos.y - pos.y;
        float z = newPos.z - pos.z;
        modelInst.transform.translate(x, y, z);
        super.setPos(newPos);
    }

    /**
     * Sets type.
     *
     * @param type the type
     * @throws NonExistentNodeTypeException the non existent node type exception
     */
    void setType(String type) throws NonExistentNodeTypeException {
        //if(!getType().equals(type)) {
        if (!parent.getNodeModels().containsKey(type)) {
            throw new NonExistentNodeTypeException(type);
        }

        modelInst = new ModelInstance(parent.getNodeModels().get(type));
        modelInst.transform.translate(getPos());

        modelInst.calculateBoundingBox(getBoundingBox());
        super.setType(type);
        //}
    }


    @Override
    public void mark() {
        modelInst.materials.get(0).set(ColorAttribute.createDiffuse(parent.getCandidateColor()));
        isMarked = true;
    }

    @Override
    public void unmark() {
        setOwner(getOwner());
        isMarked = false;
    }

    //не нужно
//    @Override
//    public void incRotationAngle() {
//        curRotationAngle += ROTATION_ANGLE_ONE_PROGRAM;
//    }
//
//    @Override
//    public void decRotationAngle() {
//        curRotationAngle -= ROTATION_ANGLE_ONE_PROGRAM;
//    }


    @Override
    public void update(NodeView node) throws NonExistentNodeTypeException {
        super.update(node);
        Color pl = parent.getPlayerColor(), ne = parent.getNeutralColor();
        float st = (float) node.getStatus();
        if (getOwner().equals(parent.getPlayerName())) {
            color.color.r = ne.r * st + pl.r * (1 - st);
            color.color.g = ne.g * st + pl.g * (1 - st);
            color.color.b = ne.b * st + pl.b * (1 - st);
        } else {
            color.color.r = pl.r * st + ne.r * (1 - st);
            color.color.g = pl.g * st + ne.g * (1 - st);
            color.color.b = pl.b * st + ne.b * (1 - st);
        }
        modelInst.materials.get(0).set(color);
        curRotationAngle = 0;
        for (int i = 0; i < node.getCoreNumber(); i++) {
            if (node.getProgram(i) != null) {
                curRotationAngle += ROTATION_ANGLE_ONE_PROGRAM;
            }
        }
    }

    @Override
    public void draw(ModelBatch modelBatch, Environment environment) {
        modelBatch.render(modelInst, environment);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        modelInst.transform.rotate(1, 1, -1, delta * curRotationAngle);
    }
}
