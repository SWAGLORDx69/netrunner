package com.netrunnergame.graphics;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.netrunnergame.NetRunnerGame;
import com.netrunnergame.core.LevelController;
import com.netrunnergame.core.LevelLoader;

/**
 * Class for the game menu.
 *
 * @author Pozigun Mikhail
 * @version 0.1
 */
public class GameMenu extends Stage {
    /**
     * The constant width of menu button.
     */
    private static int MENU_BUTTON_WIDTH;
    /**
     * The constant padding for buttons in menu(top and bottom).
     */
    private static int BUTTON_PAD_TOP_BOTTOM;
    /**
     * The constant padding for buttons in menu(left and right).
     */
    private static int BUTTON_PAD_RIGHT_LEFT;
    /**
     * The constant height of menu button.
     */
    private static int MENU_BUTTON_HEIGHT;
    /**
     * The constant width of button in menu.
     */
    private static int BUTTON_WIDTH;
    /**
     * The constant height of button in menu.
     */
    private static int BUTTON_HEIGHT;
    /**
     * The constant width of menu.
     */
    private static int MENU_WIDTH;
    /**
     * The constant height of menu.
     */
    private static int MENU_HEIGHT;
    /**
     * The viewport screen height.
     */
    private float screenHeight;
    /**
     * The viewport screen width.
     */
    private float screenWidth;
    /**
     * The skin of all buttons.
     */
    private Skin skin = new Skin();
    /**
     * The game.
     */
    private NetRunnerGame game;
    /**
     * The input processor for the game scene.
     */
    private InputProcessor inputProcessor;

    /**
     * Instantiates a new Game menu.
     *
     * @param game             the game
     * @param level            the level
     * @param startPauseButton the start pause button
     */
    public GameMenu(final NetRunnerGame game, final LevelController level, final StartPauseButton startPauseButton) {
        super(new ExtendViewport(480, 800));

        this.game = game;

        screenWidth = super.getViewport().getWorldWidth();
        screenHeight = super.getViewport().getWorldHeight();

        initSizes();
        initSkin();

        final ImageButton shadow = new ImageButton(skin, "shadow");
        shadow.setVisible(false);

        final ImageButton background = new ImageButton(skin, "background");
        background.setVisible(false);

        final Table gameMenuTable = new Table();

        final TextButton resume = new TextButton("Resume", skin, "button");
        resume.setVisible(true);
        resume.addListener(new ClickListener() {

            @Override
            public void clicked(InputEvent event, float x, float y) {
                background.setVisible(false);
                shadow.setVisible(false);
                gameMenuTable.setVisible(false);
                Gdx.input.setInputProcessor(inputProcessor);
            }
        });
        resume.getLabel().setAlignment(Align.center);

        final TextButton restart = new TextButton("Restart", skin, "button");
        restart.setVisible(true);
        restart.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new LevelScene(game, new LevelLoader(game, game.getCurrentLevelFile()).buildLevel()));
            }
        });
        restart.getLabel().setAlignment(Align.center);

        final TextButton mainMenu = new TextButton("Main Menu", skin, "button");
        mainMenu.setVisible(true);
        mainMenu.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new MenuScreen(game));
            }
        });
        mainMenu.getLabel().setAlignment(Align.center);
        gameMenuTable.add(resume).width(BUTTON_WIDTH).pad(BUTTON_PAD_TOP_BOTTOM,
                BUTTON_PAD_RIGHT_LEFT,
                BUTTON_PAD_TOP_BOTTOM,
                BUTTON_PAD_RIGHT_LEFT).row();
        gameMenuTable.add(restart).width(BUTTON_WIDTH).pad(BUTTON_PAD_TOP_BOTTOM,
                BUTTON_PAD_RIGHT_LEFT,
                BUTTON_PAD_TOP_BOTTOM,
                BUTTON_PAD_RIGHT_LEFT).row();
        gameMenuTable.add(mainMenu).width(BUTTON_WIDTH).pad(BUTTON_PAD_TOP_BOTTOM,
                BUTTON_PAD_RIGHT_LEFT,
                BUTTON_PAD_TOP_BOTTOM,
                BUTTON_PAD_RIGHT_LEFT).row();

        gameMenuTable.pack();
        gameMenuTable.setPosition(screenWidth / 2 - gameMenuTable.getWidth() / 2, screenHeight / 2 - gameMenuTable.getHeight() / 2);
        background.setBounds(screenWidth / 2 - gameMenuTable.getWidth() / 2 - BUTTON_PAD_RIGHT_LEFT,
                screenHeight / 2 - gameMenuTable.getHeight() / 2 - BUTTON_PAD_TOP_BOTTOM,
                gameMenuTable.getWidth() + 2 * BUTTON_PAD_RIGHT_LEFT,
                gameMenuTable.getHeight() + 2 * BUTTON_PAD_TOP_BOTTOM);
        shadow.setBounds(0, 0, screenWidth, screenHeight);

        gameMenuTable.setVisible(false);


        final TextButton menuButton = new TextButton("Menu", skin, "menu button");
        menuButton.setVisible(true);
        menuButton.setX(0);
        menuButton.setY(screenHeight - MENU_BUTTON_HEIGHT);
        menuButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                level.pause();
                startPauseButton.setPaused();
                background.setVisible(true);
                shadow.setVisible(true);
                gameMenuTable.setVisible(true);
                Gdx.input.setInputProcessor(GameMenu.this);
            }
        });
        menuButton.getLabel().setAlignment(Align.center);

        addActor(menuButton);
        addActor(shadow);
        addActor(background);
        addActor(gameMenuTable);
    }

    /**
     * Save game scene input processor.
     *
     * @param inputProcessor the input processor
     */
    public void saveGameSceneInputProcessor(InputProcessor inputProcessor) {
        this.inputProcessor = inputProcessor;
    }

    /**
     * Init sizes.
     */
    private void initSizes() {
        MENU_BUTTON_WIDTH = (int) (screenWidth / 10);
        MENU_BUTTON_HEIGHT = (int) (screenHeight / 10);

        BUTTON_WIDTH = (int) (screenWidth / 3);
        BUTTON_HEIGHT = (int) (screenHeight / 6);
        BUTTON_PAD_RIGHT_LEFT = (int) (screenWidth / 40);
        BUTTON_PAD_TOP_BOTTOM = (int) (screenHeight / 40);
        MENU_WIDTH = (int) (screenWidth / 2);
        MENU_HEIGHT = (int) (screenHeight / 2);
    }

    /**
     * Init skin.
     */
    private void initSkin() {
        skin.add("menu", game.getFont("info"));
        skin.add("button", game.getFont("button"));

        Pixmap pixmapMenuButton = new Pixmap(MENU_BUTTON_WIDTH, MENU_BUTTON_HEIGHT, Pixmap.Format.RGB888);
        pixmapMenuButton.setColor(Color.WHITE);
        pixmapMenuButton.fill();
        skin.add("backgroundMenuButton", new Texture(pixmapMenuButton));

        TextButton.TextButtonStyle textButtonStyleMenuButton = new TextButton.TextButtonStyle();
        textButtonStyleMenuButton.up = skin.newDrawable("backgroundMenuButton", game.getColor("button"));
        textButtonStyleMenuButton.font = skin.getFont("button");
        textButtonStyleMenuButton.fontColor = game.getColor("text head");
        skin.add("menu button", textButtonStyleMenuButton);

        Pixmap pixmapButton = new Pixmap(BUTTON_WIDTH, BUTTON_HEIGHT, Pixmap.Format.RGB888);
        pixmapButton.setColor(Color.WHITE);
        pixmapButton.fill();
        skin.add("buttonBackground", new Texture(pixmapButton));

        TextButton.TextButtonStyle textButtonStyleButton = new TextButton.TextButtonStyle();
        textButtonStyleButton.up = skin.newDrawable("buttonBackground", game.getColor("game menu button"));
        textButtonStyleButton.down = skin.newDrawable("buttonBackground", game.getColor("game menu button checked"));
        textButtonStyleButton.font = skin.getFont("menu");
        textButtonStyleButton.fontColor = game.getColor("text head");
        skin.add("button", textButtonStyleButton);

        Pixmap pixmapMenu = new Pixmap(MENU_WIDTH, MENU_HEIGHT, Pixmap.Format.RGB888);
        pixmapMenu.setColor(Color.WHITE);
        pixmapMenu.fill();
        skin.add("menuBackground", new Texture(pixmapMenu));

        ImageButton.ImageButtonStyle imageButtonStyleBackground = new ImageButton.ImageButtonStyle();
        imageButtonStyleBackground.up = skin.newDrawable("menuBackground", game.getColor("game menu background"));
        skin.add("background", imageButtonStyleBackground);

        ImageButton.ImageButtonStyle shadow = new ImageButton.ImageButtonStyle();
        shadow.up = skin.newDrawable("menuBackground", game.getColor("shadow"));
        skin.add("shadow", shadow);
    }

    @Override
    public void draw() {
        super.draw();
    }

}
