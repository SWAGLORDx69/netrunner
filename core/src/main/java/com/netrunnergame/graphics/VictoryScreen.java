package com.netrunnergame.graphics;

import com.netrunnergame.NetRunnerGame;

/**
 * The Victory end of game screen
 *
 * @author Andrew Konstantinov
 * @version 0.1
 * @see EndOfGameScreen
 */

public class VictoryScreen extends EndOfGameScreen {
    /**
     * Instantiates a new Menu screen.
     *
     * @param game is the game
     */
    public VictoryScreen(NetRunnerGame game) {
        super(game);
        message = "You win!";
    }
}
