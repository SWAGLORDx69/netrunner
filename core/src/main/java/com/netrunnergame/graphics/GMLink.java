package com.netrunnergame.graphics;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;


public class GMLink extends GMIManager.Link {
    private ModelInstance modelInst;

    /**
     * Instantiates a new Link.
     *
     * @param a         the a
     * @param b         the b
     * @param direction the direction
     */
    public GMLink(GMIManager.Node a, GMIManager.Node b, GMIManager.Direction direction,
                  Model linkModel) {
        super(a, b, direction);

        modelInst = new ModelInstance(linkModel);
        setupInst();
    }

    private void setupInst() {
        modelInst.transform.translate(getA().getPos());
        Vector3 tmp = new Vector3(getB().getPos()).mulAdd(getA().getPos(),-1);
        Vector3 dir = new Vector3(1,0,0).crs(tmp).nor();
        float len = tmp.len();
        modelInst.transform.scale(len,len,len);
        System.out.println(Math.acos(tmp.dot(1,0,0)/len));
        modelInst.transform.rotateRad(dir,(float)Math.acos(tmp.dot(1,0,0)/len));

    }

    @Override
    public void setDirection(GMIManager.Direction direction) {
        super.setDirection(direction);
    }

    @Override
    public void draw(ModelBatch modelBatch, Environment environment) {
        modelBatch.render(modelInst, environment);
    }

    @Override
    public void update() {
        //nothing
    }
}
