package com.netrunnergame.graphics;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

import java.util.List;

/**
 * The type end of game stage.
 *
 * @author Pozigun Mikhail
 * @version 0.1
 * @see Stage
 */

public class EndOfGameStage extends Stage {
    /**
     * size of a space in percent
     */
    private static final float SPACE_PCT = 0.07f;
    /**
     * width of a button in percent
     */
    private static final float BUTTON_WIDTH_PCT = 0.5f;
    /**
     * height of a button in percent
     */
    private static final float BUTTON_HEIGHT_PCT = 0.08f;
    /**
     * size of a pad in percent
     */
    private static final float PAD_PCT = 0.07f;

    /**
     * Instantiates a new Main menu stage.
     *
     * @param head    the head title of the menu
     * @param buttons the list of button's setting
     * @param skin    the skin
     * @see MenuStage.ButtonSettings
     */
    public EndOfGameStage(String head, List<MenuStage.ButtonSettings> buttons, Skin skin) {
        super(new ExtendViewport(800, 480));

        Table mainTable = new Table();

        mainTable.setPosition(0,0);
        mainTable.setHeight(getViewport().getWorldHeight() * 0.7f);
        mainTable.setWidth(getViewport().getWorldWidth());
        mainTable.center();
        mainTable.setSkin(skin);
        addActor(mainTable);

        Value butWidth = Value.percentWidth(BUTTON_WIDTH_PCT, mainTable);
        Value butHeight = Value.percentWidth(BUTTON_HEIGHT_PCT, mainTable);
        Value space = Value.percentHeight(SPACE_PCT, mainTable);
        Value padY = Value.percentHeight(PAD_PCT, mainTable);
        Value zero = Value.zero;

        mainTable.add(head, "head")
                .expand()
                .pad(padY, zero, zero, zero)
                .space(space)
                .fill().getActor().setAlignment(Align.center);

        for (MenuStage.ButtonSettings bs : buttons) {
            mainTable.row();
            mainTable.add(createButton(bs.text, bs.listener, skin))
                    //.fillY()
                    // .expandY()
                    .width(butWidth)
                    .height(butHeight)
                    .space(space)
                    .uniform();
        }
        //mainTable.pack();
        mainTable.setY(getViewport().getWorldHeight() * 0.3f);
    }

    /**
     * create a text button
     *
     * @param text     the text which will be wrote on the button
     * @param listener if is null then the button is disabled
     * @param skin     the skin
     * @return button
     * @see ClickListener
     * @see Button
     */
    private Button createButton(String text, ClickListener listener, Skin skin) {
        TextButton button = new TextButton(text, skin);
        if (listener != null) {
            button.addListener(listener);
        } else {
            button.setDisabled(true);
        }
        return button;
    }
}
