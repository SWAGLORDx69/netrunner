package com.netrunnergame.graphics;


import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.netrunnergame.NetRunnerGame;
import com.netrunnergame.core.LevelController;

/**
 * Class for the start/pause button
 *
 * @author Kots Michael
 * @version 1.0
 */
public class StartPauseButton extends Stage {

    private LevelController level;
    private TextButton startButton;
    private TextButton pauseButton;
    /**
     * Instantiates a new Start pause button.
     *
     * @param game  the game
     * @param level the level
     */
    public StartPauseButton(final NetRunnerGame game, final LevelController level) {
        super(new ExtendViewport(480, 800));

        this.level = level;

        float screenWidth = super.getViewport().getWorldWidth();
        float screenHeight = super.getViewport().getWorldHeight();
        int buttonWidth = (int) (screenWidth / 10);
        int buttonHeight = (int) (screenHeight / 10);

        Skin skin = new Skin();
        skin.add("button", game.getFont("button"));
        Pixmap pixmapButton = new Pixmap(buttonWidth, buttonHeight, Pixmap.Format.RGB888);
        pixmapButton.setColor(Color.WHITE);
        pixmapButton.fill();
        skin.add("backgroundButton", new Texture(pixmapButton));
        TextButton.TextButtonStyle textButtonStyleButton = new TextButton.TextButtonStyle();
        textButtonStyleButton.up = skin.newDrawable("backgroundButton", game.getColor("button"));
        textButtonStyleButton.font = skin.getFont("button");
        textButtonStyleButton.fontColor = game.getColor("text head");
        skin.add("button", textButtonStyleButton);

        startButton = new TextButton("Start", skin, "button");
        pauseButton = new TextButton("Pause", skin, "button");
        startButton.setVisible(false);
        startButton.setX(screenWidth - buttonWidth);
        startButton.setY(0);
        startButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                level.start();
                startButton.setVisible(false);
                pauseButton.setVisible(true);
            }
        });
        pauseButton.setVisible(true);
        pauseButton.setX(screenWidth - buttonWidth);
        pauseButton.setY(0);
        pauseButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                level.pause();
                startButton.setVisible(true);
                pauseButton.setVisible(false);
            }
        });
        startButton.getLabel().setAlignment(Align.center);
        pauseButton.getLabel().setAlignment(Align.center);
        addActor(startButton);
        addActor(pauseButton);
    }

    public void setPaused() {
        level.pause();
        startButton.setVisible(true);
        pauseButton.setVisible(false);
    }

    @Override
    public void draw() {
        super.draw();
    }

}
