package com.netrunnergame.graphics;


import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.netrunnergame.NetRunnerGame;
import com.netrunnergame.core.LevelController;

/**
 * Class for reset selection of argument button
 *
 * @author Pozigun Mikhail
 * @version 0.2
 */
public class ResetArgsButton extends Stage {
    private TextButton resetArgsButton;
    private GMIManager instancesContainer;
    private int curNodeId = -1;

    /**
     * Instantiates a new Reset args button.
     *
     * @param game        the game
     * @param level       the level
     * @param levelScene  the level scene
     * @param contextMenu the context menu
     */
    public ResetArgsButton(NetRunnerGame game, final LevelController level, final LevelScene levelScene, final ContextMenu contextMenu) {
        super(new ExtendViewport(480, 800));

        float screenWidth = super.getViewport().getWorldWidth();
        float screenHeight = super.getViewport().getWorldHeight();
        int buttonWidth = (int) (screenWidth / 10);
        int buttonHeight = (int) (screenHeight / 10);

        Skin skin = new Skin();
        skin.add("button", game.getFont("button"));
        Pixmap pixmapButton = new Pixmap(buttonWidth, buttonHeight, Pixmap.Format.RGB888);
        pixmapButton.setColor(Color.WHITE);
        pixmapButton.fill();
        skin.add("backgroundButton", new Texture(pixmapButton));
        TextButton.TextButtonStyle textButtonStyleButton = new TextButton.TextButtonStyle();
        textButtonStyleButton.up = skin.newDrawable("backgroundButton", game.getColor("button"));
        textButtonStyleButton.font = skin.getFont("button");
        textButtonStyleButton.fontColor = game.getColor("text head");
        skin.add("button", textButtonStyleButton);

        resetArgsButton = new TextButton("Reset", skin, "button");
        resetArgsButton.setVisible(false);
        resetArgsButton.setX(screenWidth - buttonWidth);
        resetArgsButton.setY(buttonHeight + buttonHeight / 20);
        resetArgsButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                levelScene.setWaitingProgram(null);
                instancesContainer.unmarkAll();
                resetArgsButton.setVisible(false);

                levelScene.markNode(curNodeId);
            }
        });
        resetArgsButton.getLabel().setAlignment(Align.center);
        addActor(resetArgsButton);
    }

    /**
     * Init inst container.
     *
     * @param instancesContainer the instances container
     */
    public void initInstContainer(GMIManager instancesContainer) {
        this.instancesContainer = instancesContainer;
    }

    /**
     * Sets reset button visible.
     *
     * @param b the b
     */
    public void setResetButtonVisible(boolean b) {
        resetArgsButton.setVisible(b);
    }

    /**
     * Sets cur node.
     *
     * @param nodeId the node id
     */
    public void setCurNode(int nodeId) {
        curNodeId = nodeId;
    }

    @Override
    public void draw() {
        super.draw();
    }

}
