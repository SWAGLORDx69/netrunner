package com.netrunnergame.graphics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.netrunnergame.NetRunnerGame;
import com.netrunnergame.core.Program;

/**
 * Tooltip for program(chosen in context menu or bottom menu).
 *
 * @author Pozigun Mikhail
 * @version 0.2
 * @see Stage
 */
public class ProgramTooltip extends Stage {
    /**
     * The constant width of every row in context menu table.
     */
    private static int TABLE_ROW_WIDTH;
    /**
     * The constant padding for text,characteristics (left and right).
     */
    private static int PAD_RIGHT_LEFT;
    /**
     * The constant padding for text (top).
     */
    private static int PAD_TOP_TEXT;
    /**
     * The constant padding for text (bottom).
     */
    private static int PAD_BOTTOM_TEXT;
    /**
     * The constant padding for name (top and bottom).
     */
    private static int PAD_TOP_BOTTOM_NAME;
    /**
     * The constant padding for characteristics section (top and bottom).
     */
    private static int PAD_TOP_BOTTOM_CHAR;
    /**
     * The table with info about program.
     */
    private Table programTooltipTable;
    /**
     * The game (here for getting fonts).
     */
    private NetRunnerGame game;
    /**
     * The skin of all buttons.
     */
    private Skin skin = new Skin();
    /**
     * The text button with common info about program.
     */
    private TextButton programInfo;
    /**
     * The text button with program name, title for tooltip.
     */
    private TextButton programName;
    /**
     * The text button with data requirements.
     */
    private TextButton dataReq;
    /**
     * The text button with RAM requirements.
     */
    private TextButton ramReq;
    /**
     * The viewport screen height.
     */
    private float screenHeight;
    /**
     * The viewport screen width.
     */
    private float screenWidth;
    /**
     * The current host.
     */
    private Owner curOwner;

    /**
     * Instantiates a new Program tooltip.
     *
     * @param game the game
     */
    public ProgramTooltip(NetRunnerGame game) {
        super(new ExtendViewport(480, 800));
        screenWidth = super.getViewport().getWorldWidth();
        screenHeight = super.getViewport().getWorldHeight();
        initSizes();

        this.game = game;

        programTooltipTable = new Table();
        programTooltipTable.setVisible(false);
        addActor(programTooltipTable);
        initSkin();
        //fill table
        programInfo = new TextButton("", skin);
        programInfo.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                hide();
            }
        });
        programName = new TextButton("", skin, "head");
        programName.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                hide();
            }
        });
        dataReq = new TextButton("", skin, "characteristics");
        dataReq.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                hide();
            }
        });
        ramReq = new TextButton("", skin, "characteristics");
        ramReq.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                hide();
            }
        });
        programName.getLabel().setAlignment(Align.center);
        dataReq.getLabel().setAlignment(Align.left);
        ramReq.getLabel().setAlignment(Align.left);
        programInfo.getLabel().setAlignment(Align.left);
        programInfo.getLabel().setWrap(true);
        dataReq.getLabel().setWrap(true);
        ramReq.getLabel().setWrap(true);
        programName.getLabel().setWrap(true);
        programName.padBottom(PAD_TOP_BOTTOM_NAME).padTop(PAD_TOP_BOTTOM_NAME);
        programInfo.padLeft(PAD_RIGHT_LEFT).padRight(PAD_RIGHT_LEFT).padBottom(PAD_BOTTOM_TEXT).padTop(PAD_TOP_TEXT);
        dataReq.padLeft(PAD_RIGHT_LEFT).padRight(PAD_RIGHT_LEFT).padTop(PAD_TOP_BOTTOM_CHAR);
        ramReq.padLeft(PAD_RIGHT_LEFT).padRight(PAD_RIGHT_LEFT).padBottom(PAD_TOP_BOTTOM_CHAR);
        programTooltipTable.add(programName).width(TABLE_ROW_WIDTH).row();
        programTooltipTable.add(dataReq).width(TABLE_ROW_WIDTH).row();
        programTooltipTable.add(ramReq).width(TABLE_ROW_WIDTH).row();
        programTooltipTable.add(programInfo).width(TABLE_ROW_WIDTH).row();

        setOwner(Owner.OWNER_NO_OWNER);
    }

    /**
     * Gets host.
     *
     * @return the host
     */
    public Owner getOwner() {
        return curOwner;
    }

    /**
     * Sets host.
     *
     * @param owner the host
     */
    public void setOwner(Owner owner) {
        curOwner = owner;
    }

    /**
     * Init sizes.
     */
    private void initSizes() {
        TABLE_ROW_WIDTH = (int) (screenWidth / 4.5f);
        PAD_RIGHT_LEFT = TABLE_ROW_WIDTH / 16;
        PAD_TOP_BOTTOM_NAME = TABLE_ROW_WIDTH / 14;
        PAD_TOP_TEXT = TABLE_ROW_WIDTH / 16;
        PAD_BOTTOM_TEXT = TABLE_ROW_WIDTH / 14;
        PAD_TOP_BOTTOM_CHAR = TABLE_ROW_WIDTH / 16;
    }

    /**
     * Init skin.
     */
    private void initSkin() {
        skin.add("default", game.getFont("text"));
        skin.add("head", game.getFont("button"));
        Pixmap pixmap = new Pixmap(TABLE_ROW_WIDTH, PAD_RIGHT_LEFT, Pixmap.Format.RGB888);
        pixmap.setColor(Color.WHITE);
        pixmap.fill();
        skin.add("background", new Texture(pixmap));

        TextButton.TextButtonStyle textButtonStyleInfo = new TextButton.TextButtonStyle();
        textButtonStyleInfo.up = skin.newDrawable("background", game.getColor("text background"));
        textButtonStyleInfo.font = skin.getFont("default");
        textButtonStyleInfo.fontColor = game.getColor("text");
        skin.add("default", textButtonStyleInfo);

        TextButton.TextButtonStyle textButtonStyleChar = new TextButton.TextButtonStyle();
        textButtonStyleChar.up = skin.newDrawable("background", game.getColor("program char background"));
        textButtonStyleChar.font = skin.getFont("default");
        textButtonStyleChar.fontColor = game.getColor("text head");
        skin.add("characteristics", textButtonStyleChar);

        TextButton.TextButtonStyle textButtonStyleHead = new TextButton.TextButtonStyle();
        textButtonStyleHead.up = skin.newDrawable("background", game.getColor("head background"));
        textButtonStyleHead.font = skin.getFont("head");
        textButtonStyleHead.fontColor = game.getColor("text head");
        skin.add("head", textButtonStyleHead);
    }

    /**
     * Hide tooltip.
     */
    public void hide() {
        programTooltipTable.setVisible(false);
        setOwner(Owner.OWNER_NO_OWNER);
    }

    @Override
    public void draw() {
        if (programTooltipTable.isVisible()) {
            super.draw();
        }
    }

    /**
     * Sets tooltip for chosen program.
     *
     * @param pos     the pos
     * @param program the program
     */
    public void setProgram(Vector2 pos, Program program) {
        Gdx.app.log("setProgram", "Set tooltip for program " + program.getName());
        programName.setText(program.getName());
        dataReq.setText("Data required: " + program.getDataCost());
        ramReq.setText("RAM required: " + program.getRAM());
        programInfo.setText(program.getInfo());

        programTooltipTable.pack();
        programTooltipTable.setPosition(pos.x - Math.max(0, pos.x + programTooltipTable.getWidth() - screenWidth),
                pos.y - Math.max(0, pos.y + programTooltipTable.getHeight() - screenHeight));
        programTooltipTable.setVisible(true);
    }

    /**
     * The enum with possible owners of tooltip.
     */
    public enum Owner {

        OWNER_CONTEXT_MENU,
        OWNER_PROGRAM_PANEL,
        OWNER_NO_OWNER
    }
}