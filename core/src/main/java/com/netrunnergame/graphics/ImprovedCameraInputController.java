package com.netrunnergame.graphics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

class CameraWithTarget {
    private final Vector3 pivotPoint = new Vector3();
    private final Vector3 camX = new Vector3();
    private final Vector3 camY = new Vector3();
    private final Vector3 camZ = new Vector3();
    private final Camera camera;

    public CameraWithTarget(Camera camera, Vector3 pivotPoint) {
        //TODO add setting camera
        this.camera = camera;
        this.pivotPoint.set(pivotPoint);
    }

    public CameraWithTarget(Camera camera) {
        this(camera, new Vector3());
    }

    private Vector3 calcCamX(Vector3 vec) {
        return vec.set(camera.direction).crs(camera.up).nor();
    }

    private Vector3 calcCamY(Vector3 vec) {
        return vec.set(camera.up).nor();
    }

    private Vector3 calcCamZ(Vector3 vec) {
        return vec.set(camera.direction).nor();
    }

    private Vector3 calcCamX() {
        return calcCamX(camX);
    }

    private Vector3 calcCamY() {
        return calcCamY(camY);
    }

    private Vector3 calcCamZ() {
        return calcCamZ(camZ);
    }

    public Vector3 getTarget() {
        return pivotPoint;
    }

    public void setTarget(Vector3 target) {
        pivotPoint.scl(-1).add(target);
        camera.translate(pivotPoint.x, pivotPoint.y, pivotPoint.z + 0.01f);//TODO I understand nothing, pithdetz
        pivotPoint.set(target);
        camera.update();
    }

    public void translate(float deltaX, float deltaY, float deltaZ) {
        Vector3 tmp = calcCamX().scl(deltaX)
                .mulAdd(calcCamY(), deltaY)
                .mulAdd(calcCamZ(), deltaZ);

        camera.translate(tmp);
        pivotPoint.add(tmp);
        camera.update();
    }

    public void translateAbs(float deltaX, float deltaY, float deltaZ) {
        camera.translate(deltaX, deltaY, deltaZ);
        pivotPoint.add(deltaX, deltaY, deltaZ);
        camera.update();
    }

    public void rotate(float angleX, float angleY, float angleZ) {
        camera.rotateAround(pivotPoint, calcCamX(), angleX);
        camera.rotateAround(pivotPoint, calcCamY(), angleY);
        camera.rotateAround(pivotPoint, calcCamZ(), angleZ);
        camera.update();
    }

    public boolean zoom(float amount) {
        float len = new Vector3(camera.position).mulAdd(pivotPoint, -1).len2();
        if ((len >= 20 && amount > 0) || (len <= 800 && amount < 0)) {
            Vector3 tmpV = new Vector3();
            camera.translate(tmpV.set(camera.direction).scl(amount));
            if (!camera.frustum.pointInFrustum(pivotPoint)) {
                camera.translate(tmpV.scl(-amount));
            }
            camera.update();
        }
        return true;
    }

    public void update(float delta) {

    }
}

class CameraWithInertia extends CameraWithTarget {
    private static final float FACTOR_ROT_SPEED_REDUCTION_X = 1f;
    private static final float FACTOR_ROT_SPEED_REDUCTION_Y = 1f;
    private static final float FACTOR_ROT_SPEED_REDUCTION_Z = 1f;
    private static final float FACTOR_ZOOM_SPEED_REDUCTION = 0.0f;
    private static final int NUM_TR_STEP = 9;

    private boolean isInertia = false;

    private int numOfStep = 0;
    private float lastRotDeltaX = 0.0f;
    private float lastRotDeltaY = 0.0f;
    private float lastRotDeltaZ = 0.0f;
    private float lastZoomDelta = 0.0f;

    private float rotSpeedX = 0.0f;
    private float rotSpeedY = 0.0f;
    private float rotSpeedZ = 0.0f;
    private float zoomSpeed = 0.0f;

    private Vector3 pivotSpeed = new Vector3();

    public CameraWithInertia(Camera camera, Vector3 pivotPoint) {
        super(camera, pivotPoint);
    }

    public CameraWithInertia(Camera camera) {
        super(camera);
    }

    public void setInertia(boolean state) {
        isInertia = state;
        if (!isInertia) {
            rotSpeedX = 0.0f;
            rotSpeedY = 0.0f;
            rotSpeedZ = 0.0f;
            zoomSpeed = 0.0f;
        }
    }

    @Override
    public void setTarget(Vector3 target) {
        pivotSpeed.set(target).mulAdd(getTarget(), -1);
        pivotSpeed.scl(1f / NUM_TR_STEP);
        numOfStep = NUM_TR_STEP;
    }

    @Override
    public void rotate(float angleX, float angleY, float angleZ) {
        lastRotDeltaX += angleX;
        lastRotDeltaY += angleY;
        lastRotDeltaZ += angleZ;
        super.rotate(angleX, angleY, angleZ);
    }

    @Override
    public boolean zoom(float amount) {
        lastZoomDelta += amount;
        return super.zoom(amount);
    }

    private float reduce(float val, float factor) {
        if (factor > 0) {
            float temp = val - Math.signum(val) * factor;
            return (val * temp > 0.0f ? temp : 0.0f);
        } else {
            return 0;
        }
    }

    @Override
    public void update(float delta) {
        if (numOfStep > 0) {
            numOfStep--;
            translateAbs(pivotSpeed.x, pivotSpeed.y + 0.01f, pivotSpeed.z);
        }
        rotSpeedX = lastRotDeltaX / delta;
        lastRotDeltaX = 0.0f;
        rotSpeedY = lastRotDeltaY / delta;
        lastRotDeltaY = 0.0f;
        zoomSpeed = lastZoomDelta / delta;
        lastZoomDelta = 0.0f;
        rotSpeedZ = lastRotDeltaZ / delta;
        lastRotDeltaZ = 0.0f;
        if (isInertia) {
            if ((rotSpeedX != 0.0f || rotSpeedY != 0.0f || rotSpeedZ != 0.0f)) {
                rotSpeedX = reduce(rotSpeedX * delta, FACTOR_ROT_SPEED_REDUCTION_X);
                rotSpeedY = reduce(rotSpeedY * delta, FACTOR_ROT_SPEED_REDUCTION_Y);
                rotSpeedZ = reduce(rotSpeedZ * delta, FACTOR_ROT_SPEED_REDUCTION_Z);
                rotate(rotSpeedX, rotSpeedY, rotSpeedZ);
            }
            if (FACTOR_ZOOM_SPEED_REDUCTION > 0.0f && zoomSpeed != 0.0f) {
                zoomSpeed = reduce(zoomSpeed * delta, FACTOR_ZOOM_SPEED_REDUCTION);
                zoom(zoomSpeed);
            }
        }
    }
}


/**
 * Camera input controller class (improved version of CameraInputController class)
 *
 * @author Pozigun Mikhail
 * @author Kirill Smirnov
 * @version 4.0
 * @see GestureDetector
 * @see GestureAdapter
 */
public class ImprovedCameraInputController extends GestureDetector {
    /**
     * The button for rotating the camera.
     */
    private static final int ROTATE_BUTTON = Input.Buttons.LEFT;
    /**
     * The button for rotating the camera.
     */
    private static final int TILT_BUTTON = Input.Buttons.RIGHT;
    /**
     * The angle to rotate when moved the full width or height of the screen.
     */
    private static final float ROTATE_ANGLE = 120.0f;
    private static final float TILT_ANGLE = 1.0f;
    /**
     * The units to translate the camera when moved the full width or height of the screen.
     */
    private static final float TRANSLATE_UNITS = 30.0f;
    /**
     * The weight for each scrolled amount.
     */
    private static final float SCROLL_FACTOR = -0.02f;
    /**
     * World units per screen size.
     */
    private static final float PINCH_ZOOM_FACTOR = 20f;
    private static final float PAN_TILT_FACTOR = 120f;

    /**
     * The camera.
     */
    private CameraWithInertia camera;
    private Mode mode = Mode.ROTATION;
    private boolean isBlocked = true;


    /**
     * Instantiates a new Improved camera input controller.
     *
     * @param gestureListener the gesture listener
     * @param camera          the camera
     */
    protected ImprovedCameraInputController(final ImprovedCameraInputController.CameraGestureListener gestureListener,
                                            final Camera camera) {
        super(gestureListener);
        gestureListener.controller = this;
        this.camera = new CameraWithInertia(camera);
    }

    /**
     * Instantiates a new Improved camera input controller.
     *
     * @param camera the camera
     */
    public ImprovedCameraInputController(final Camera camera) {
        this(new ImprovedCameraInputController.CameraGestureListener(), camera);
    }

    public void setCamCenter(Vector3 point) {
        camera.setTarget(point);
    }

    public void setStrictMode(boolean strictMode) {
        camera.setInertia(!strictMode);
    }


    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        setStrictMode(true);
        isBlocked = false;
        switch (button) {
            case TILT_BUTTON:
                mode = Mode.TILT;
                break;
            case ROTATE_BUTTON:
            default:
                mode = Mode.ROTATION;
                break;
        }
        return super.touchDown(screenX, screenY, pointer, button);
    }

    @Override
    public boolean touchUp(float x, float y, int pointer, int button) {
        setStrictMode(false);
        isBlocked = true;
        return super.touchUp(x, y, pointer, button);
    }

    @Override
    public boolean scrolled(int amount) {
        if (!isBlocked) {
            zoom(amount * SCROLL_FACTOR * TRANSLATE_UNITS);
        }
        return super.scrolled(amount);
    }

    protected void rotate(float deltaX, float deltaY) {
        if (!isBlocked) {
            camera.rotate(deltaY * -ROTATE_ANGLE, deltaX * -ROTATE_ANGLE, 0);
        }
    }

    protected void tilt(float angle) {
        if (!isBlocked) {
            camera.rotate(0, 0, angle * TILT_ANGLE);
        }
    }

    public void update(float delta) {
        camera.update(delta);
    }

    /**
     * Zooming camera translation.
     *
     * @param amount the amount of zooming
     * @return the boolean
     */
    public void zoom(float amount) {
        if (!isBlocked) {
            camera.zoom(amount);
        }
    }

    enum Mode {
        ROTATION,
        TRANSLATION,
        TILT
    }

    /**
     * Gesture listener for multitouch zooming
     */
    protected static class CameraGestureListener extends GestureAdapter {
        private ImprovedCameraInputController controller;
        /**
         * Previous zoom amount.
         */
        private float previousZoom;
        private float previousDeltaAngle;

        @Override
        public boolean touchDown(float x, float y, int pointer, int button) {
            previousZoom = 0.0f;
            previousDeltaAngle = 0.0f;
            return false;
        }

        @Override
        public boolean pan(float x, float y, float deltaX, float deltaY) {
            if (controller.mode == Mode.ROTATION) {
                controller.rotate((deltaX) / Gdx.graphics.getWidth(),
                        (deltaY) / Gdx.graphics.getHeight());
            } else if (controller.mode == Mode.TILT) {
                controller.tilt(deltaX * PAN_TILT_FACTOR / Gdx.graphics.getWidth());
            }
            return false;
        }

        @Override
        public boolean zoom(float initialDistance, float distance) {
            float newZoom = distance - initialDistance;
            float amount = newZoom - previousZoom;
            previousZoom = newZoom;
            float w = Gdx.graphics.getWidth(), h = Gdx.graphics.getHeight();
            controller.zoom(PINCH_ZOOM_FACTOR * amount / ((w > h) ? h : w));
            return true;
        }

        @Override
        public boolean pinch(Vector2 initPoint1, Vector2 initPoint2, Vector2 point1, Vector2 point2) {
            Vector2 tmpV1 = new Vector2().set(initPoint1).mulAdd(initPoint2, -1);
            Vector2 tmpV2 = new Vector2().set(point1).mulAdd(point2, -1);
            float angle = tmpV2.angle(tmpV1);
            controller.tilt(angle - previousDeltaAngle);
            previousDeltaAngle = angle;
            return true;
        }

        @Override
        public void pinchStop() {

        }
    }
}