package com.netrunnergame.graphics;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import java.util.List;

/**
 * The type Main menu stage.
 *
 * @author Kirill Smirnov
 * @version 1.0
 * @see Stage
 */

public class MenuStage extends Stage {
    /**
     * size of a space in percent
     */
    private static final float SPACE_PCT = 0.05f;
    /**
     * width of a button in percent
     */
    private static final float BUTTON_WIDTH_PCT = 0.5f;
    /**
     * height of a button in percent
     */
    private static final float BUTTON_HEIGHT_PCT = 0.08f;
    /**
     * size of a pad in percent
     */
    private static final float PAD_PCT = 0.07f;

    /**
     * Instantiates a new Main menu stage.
     *
     * @param head    the head title of the menu
     * @param buttons the list of button's setting
     * @param skin    the skin
     * @see ButtonSettings
     */
    public MenuStage(String head, List<ButtonSettings> buttons, Skin skin) {
        super(new ExtendViewport(800, 480));

        Table mainTable = new Table();

        mainTable.setPosition(0,0);
        mainTable.setHeight(getViewport().getWorldHeight());
        mainTable.setWidth(getViewport().getWorldWidth());
        mainTable.center();
        mainTable.setSkin(skin);
        addActor(mainTable);

        Value butWidth = Value.percentWidth(BUTTON_WIDTH_PCT, mainTable);
        Value butHeight = Value.percentWidth(BUTTON_HEIGHT_PCT, mainTable);
        Value space = Value.percentHeight(SPACE_PCT, mainTable);
        Value padY = Value.percentHeight(PAD_PCT, mainTable);
        Value zero = Value.zero;

        mainTable.add(head, "head")
                .expand()
                .pad(padY, zero, zero, zero)
                .space(space)
                .fill().getActor().setAlignment(Align.center);

        for (ButtonSettings bs : buttons) {
            mainTable.row();
            mainTable.add(createButton(bs.text, bs.listener, skin))
                    //.fillY()
                    //.expandY()
                    .width(butWidth)
                    .height(butHeight)
                    .space(space)
                    .uniform();
        }
        mainTable.getCells().peek().pad(zero, zero, padY, zero);
        //setDebugAll(true);
    }

    /**
     * create a text button
     *
     * @param text     the text which will be wrote on the button
     * @param listener if is null then the button is disabled
     * @param skin     the skin
     * @return button
     * @see ClickListener
     * @see Button
     */
    private Button createButton(String text, ClickListener listener, Skin skin) {
        TextButton button = new TextButton(text, skin);
        if (listener != null) {
            button.addListener(listener);
        } else {
            button.setDisabled(true);
        }
        return button;
    }

    /**
     * The type of button settings.
     *
     * @author Kirill Smirnov
     * @version 1.0
     */
    public static class ButtonSettings {
        /**
         * The text
         */
        String text;
        /**
         * The Listener
         */
        ClickListener listener;

        /**
         * Instantiates a new button's settings.
         *
         * @param text     the text
         * @param listener the listener
         * @see ClickListener
         */
        public ButtonSettings(String text, ClickListener listener) {
            this.text = text;
            this.listener = listener;
        }
    }

}
