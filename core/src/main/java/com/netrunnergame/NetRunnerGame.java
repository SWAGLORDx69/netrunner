package com.netrunnergame;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.google.common.collect.ImmutableMap;
import com.netrunnergame.graphics.MenuScreen;
import com.netrunnergame.utils.LevelJson;
import com.netrunnergame.utils.LevelJsonLoader;

public class NetRunnerGame extends Game {
    public final AssetManager assets = new AssetManager();
    private ImmutableMap<String, BitmapFont> fonts;
    private ImmutableMap<String, Color> colors;
    private String currentLevelFile;

    private void makeColors() {
        ImmutableMap.Builder<String, Color> builder = ImmutableMap.builder();
        builder.put("button", new Color(0xD40012CC));
        builder.put("button touched", new Color(0xD43333CC));
        builder.put("text background", new Color(0xFFFFFFCC));
        builder.put("program menu background", new Color(0xFFFFFFCC));
        builder.put("head background", new Color(0xA10F12CC));
        builder.put("files background", new Color(0xC70012CC));
        builder.put("program char background", new Color(0xC70012CC));
        builder.put("text head", new Color(0xFFFFFFFF));
        builder.put("text", new Color(0x000000FF));
        builder.put("node player", new Color(0xBB2520FF));
        builder.put("node neutral", new Color(0x8E8D8FFF));
        builder.put("node candidate", new Color(0xE6E4E7FF));
        builder.put("background", new Color(0x141400FF));
        builder.put("game menu background", new Color(0xFFFFFFCC));
        builder.put("game menu button", new Color(0xAD4040FF));
        builder.put("game menu button checked", new Color(0x8E8D8FFF));
        builder.put("shadow", new Color(0x00000096));
        colors = builder.build();

    }

    public Color getColor(String name) {
        return colors.get(name);
    }

    private void makeFonts() {
        ImmutableMap.Builder<String, BitmapFont> builder = ImmutableMap.<String, BitmapFont>builder();
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        BitmapFont font;

        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(
                Gdx.files.internal("data/fonts/a_LCDNova.ttf"));
        //       Gdx.files.internal("data/fonts/8080.ttf"));
        //        Gdx.files.internal("data/fonts/Advanced_LED_Board-7.ttf"));

        parameter.size = 30;
        parameter.magFilter = Texture.TextureFilter.Linear;
        parameter.minFilter = Texture.TextureFilter.Linear;
        font = generator.generateFont(parameter);
        builder.put("head", font);

        parameter.size = 40;
        font = generator.generateFont(parameter);
        builder.put("info", font);

        parameter.size = 27;
        font = generator.generateFont(parameter);
        builder.put("button", font);

        parameter.size = 28;
        font = generator.generateFont(parameter);
        builder.put("menu button", font);

        generator.dispose();

        generator = new FreeTypeFontGenerator(
                Gdx.files.internal("data/fonts/Consolas.ttf"));

        parameter.size = 24;
        font = generator.generateFont(parameter);
        builder.put("text", font);
        builder.put("hint", font);

        parameter.size = 27;
        font = generator.generateFont(parameter);
        builder.put("message", font);

        generator.dispose();

        generator = new FreeTypeFontGenerator(
                Gdx.files.internal("data/fonts/Consolas_Italic.ttf"));

        parameter.size = 14;
        font = generator.generateFont(parameter);
        builder.put("log", font);

        parameter.size = 20;
        font = generator.generateFont(parameter);
        builder.put("copyright", font);

        generator.dispose();
        fonts = builder.build();
        //assert (fonts.size() == 5);
    }

    public BitmapFont getFont(String name) {
        return fonts.get(name);
    }

    @Override
    public void create() {
        Gdx.app.setLogLevel(Gdx.app.LOG_DEBUG);
        makeFonts();
        makeColors();
        assets.load("data/sprites/loading.txt", TextureAtlas.class);
        assets.finishLoadingAsset("data/sprites/loading.txt");
        assets.setLoader(LevelJson.class, new LevelJsonLoader(new InternalFileHandleResolver()));
        //this.setScreen(new LevelScene(this, new LevelLoader(this, "data/levels/test.json").buildLevel()));
        this.setScreen(new MenuScreen(this));
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void dispose() {
        super.dispose();
    }

    public String getCurrentLevelFile() {
        return currentLevelFile;
    }

    public void setCurrentLevelFile(String currentLevelFile) {
        this.currentLevelFile = currentLevelFile;
    }

}
