programiconnotavail.png
size: 512, 512
format: RGBA8888
filter: Linear,Linear
repeat: none
Acceleration Program
  rotate: false
  xy: 0, 0
  size: 128, 128
  orig: 128, 128
  offset: 0, 0
  index: -1
Active Defense
  rotate: false
  xy: 128, 0
  size: 128, 128
  orig: 128, 128
  offset: 0, 0
  index: -1
Advanced Attack
  rotate: false
  xy: 256, 0
  size: 128, 128
  orig: 128, 128
  offset: 0, 0
  index: -1
Attack Program
  rotate: false
  xy: 384, 0
  size: 128, 128
  orig: 128, 128
  offset: 0, 0
  index: -1
Data Mining
  rotate: false
  xy: 0, 128
  size: 128, 128
  orig: 128, 128
  offset: 0, 0
  index: -1
Defensive Program
  rotate: false
  xy: 128, 128
  size: 128, 128
  orig: 128, 128
  offset: 0, 0
  index: -1
Distributed Computing
  rotate: false
  xy: 256, 128
  size: 128, 128
  orig: 128, 128
  offset: 0, 0
  index: -1
extra1
  rotate: false
  xy: 384, 128
  size: 128, 128
  orig: 128, 128
  offset: 0, 0
  index: -1
extra2
  rotate: false
  xy: 0, 256
  size: 128, 128
  orig: 128, 128
  offset: 0, 0
  index: -1
Firewall Program
  rotate: false
  xy: 128, 256
  size: 128, 128
  orig: 128, 128
  offset: 0, 0
  index: -1
Hound Program
  rotate: false
  xy: 256, 256
  size: 128, 128
  orig: 128, 128
  offset: 0, 0
  index: -1
Mask Program
  rotate: false
  xy: 384, 256
  size: 128, 128
  orig: 128, 128
  offset: 0, 0
  index: -1
Spy Program
  rotate: false
  xy: 0, 384
  size: 128, 128
  orig: 128, 128
  offset: 0, 0
  index: -1
