To work with this project you need the following software:

 - Android SDK
 - Framework for Android API 21(Download from Android SDK Manager)

Setting up your work space:

 - Download repository
 - Import project into your favourite IDE(or use the console)
 - Make an Android project working under Android API 21
 - Set an environmental variable ANDROID_HOME with path to your SDK

Building and debbuging application:

 - Plug in your android device(USB debugging mode must be switched on, set it on your device in advance) or run an android emulator
 - Install with command "gradlew android:installDebug"
 - Run with command "gradlew android:run" or from your device itself

